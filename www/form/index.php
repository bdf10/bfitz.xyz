<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead() ?>

<?php
// Check if the form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Retrieve form data
    $name = htmlspecialchars($_POST['name']);
    $gender = $_POST['gender'];
    $response = htmlspecialchars($_POST['response']);

    // Process or display the form data
    echo "<h2>My Form</h2>";
    echo "Name: " . $name . "<br>";
    echo "Gender: " . $gender . "<br>";
    echo "Response: " . $response . "<br>";
}
?>

<!-- The HTML form -->
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
    <label for="name">Name:</label><br>
    <input type="text" id="name" name="name"><br>

    <label for="gender">Gender:</label><br>
    <select id="gender" name="gender">
        <option value="Male">Male</option>
        <option value="Female">Female</option>
    </select><br>

    <label for="response">Your Response:</label><br>
    <textarea id="response" name="response"></textarea><br>

    <input type="submit" value="Submit">
</form>
