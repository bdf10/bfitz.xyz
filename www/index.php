<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead('Home') ?>

<p> I am a lecturer in the <a href="http://math.duke.edu/">mathematics
    department</a> at <a href="http://duke.edu">Duke University</a>. I completed
    a PhD in mathematics in 2017 under the supervision of
    <a href="https://math.duke.edu/people/paul-stephen-aspinwall">Paul
        Aspinwall</a>. My interests lie mainly in algebraic geometry, category
    theory, and homological algebra.</p>

<p> This website is built upon free and open source software. You can contribute
    to the construction of this site through its
    <a href="https://gitlab.oit.duke.edu/bdf10/bfitz.xyz">GitLab
        repository</a>.</p>
