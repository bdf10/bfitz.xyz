<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead() ?>

<h2>Schedule</h2>

<p>Quizzes and problem sets are distributed through gradescope.</p>

<?php
$fdoc = mktime(hour: 6, day: 29, month: 8, year: 2022);
$schedule = [
    [
        'debut' => strtotime("-4 week", $fdoc),
        'col1' => 'Week 0',
        'lecname' => 'Welcome!',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/welcome/welcome.pdf',
        'icons' => [
            'youtube' => 'https://youtu.be/XltceBe1gGQ',
            'ios' => 'https://apps.apple.com/us/app/scanner-pro-pdf-scanner-app/id333710667',
            'android' => 'https://play.google.com/store/apps/details?id=com.microsoft.office.officelens',
        ],
    ],
    [],
    [
        'debut' => strtotime("-4 week", $fdoc),
        'col1' => 'Week 1',
        'lecname' => 'Matrices and Vectors',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/matvec/matvec.pdf',
        'icons' => [
            'youtube' => 'https://youtu.be/3uNbT8X7zGo',
            'sage' => './matvec-sage.php',
        ],
    ],
    [
        'lecname' => 'Adjectives',
        'comment' => '<strong style="color:red"> ⟵ complete at home! (w/quiz but no pset)</strong>',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/adjectives/adjectives.pdf',
        'icons' => [
            'youtube' => 'https://youtu.be/4O541F3C89I',
            'sage' => './adjectives-sage.php',
        ],
    ],
    [
        'lecname' => 'Matrix-Vector Products',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/lincomb/lincomb.pdf',
        'icons' => [
            'youtube' => 'https://youtu.be/-095IM1GU5U',
            'sage' => './lincomb-sage.php',
        ],
    ],
    [
        'lecname' => 'Digraphs',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/digraphs/digraphs.pdf',
        'icons' => [
            'youtube' => 'https://youtu.be/rWbcj17JuWA',
            'sage' => './digraphs-sage.php',
        ],
    ],
    [],
    [
        'debut' => strtotime("+1 week", $fdoc),
        'col1' => 'Week 2',
        'lecname' => 'Vector Geometry',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/geometry/geometry.pdf',
        'icons' => [
            'youtube' => 'https://youtu.be/HAgVUqbnBWM',
            'sage' => './geometry-sage.php',
        ],
    ],
    [
        'lecname' => 'Matrix Multiplication',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/matmult/matmult.pdf',
        'icons' => [
            'youtube' => 'https://youtu.be/4_3z7DE4580',
            'sage' => './matmult-sage.php',
        ],
    ],
    [
        'lecname' => 'Row Echelon Forms',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/rref/rref.pdf',
        'icons' => [
            'youtube' => 'https://youtu.be/SouZy61TGIY',
            'sage' => './rref-sage.php',
        ],
    ],
    [],
    [
        'debut' => strtotime("+2 week", $fdoc),
        'col1' => 'Week 3',
        'lecname' => 'Linear Systems',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/axb/axb.pdf',
        'icons' => [
            'youtube' => 'https://youtu.be/yIUmtIwh37Y',
            'sage' => './axb-sage.php',
            'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/axb/axb-ex.pdf',
        ],
    ],
    [
        'lecname' => 'Gauss-Jordan Elimination',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/gj/gj.pdf',
        'icons' => [
            'youtube' => 'https://youtu.be/LUQHT6OniTY',
            'sage' => './gj-sage.php',
            'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/gj/gj-examples.pdf',
        ],
    ],
    [
        'lecname' => 'Nonsingular Matrices',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/nonsing/nonsing.pdf',
        'icons' => [
            'youtube' => 'https://youtu.be/GCc2_G8TK0s',
            'sage' => './nonsing-sage.php',
        ],
    ],
    [],
    [
        'debut' => strtotime("+3 week", $fdoc),
        'col1' => 'Week 4',
        'lecname' => 'EA=R Factorizations',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/ear/ear.pdf',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCBTvNlWm5TBy_662VwCjrd-',
            'sage' => './ear-sage.php',
        ],
    ],
    [
        'lecname' => 'PA=LU Factorizations',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/palu/palu.pdf',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCC8fkdt06abb3PoPsGFZGE_',
            'sage' => './palu-sage.php',
        ],
    ],
    [
        'lecname' => 'Eigenvalues',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/evals/evals.pdf',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDRFYw5usU7O0CNxmxbMkKz',
            'sage' => './evals-sage.php',
        ],
    ],
    [],
    [
        'debut' => strtotime("+3 week", $fdoc),
        'col1' => date("D d-M", mktime(hour: 0, day: 3, month: 10, year: 2022)),
        'lecname' => 'Exam I',
        'comment' => '<strong style="color:red"> ⟵ covers weeks 1-4 (rescheduled 3-Oct!)</strong>',
        'leclink' => './218f22-exam01.pdf',
        'icons' => [
            'info' => './exam01.php',
            'key' => './218f22-exam01-solutions.pdf'
        ],
    ],
    [],
    [
        'debut' => strtotime("+4 week", $fdoc),
        'col1' => 'Week 5',
        'lecname' => 'Null Spaces',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/null/null.pdf',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDtTAi5KpGXH-eeUgRipRlU',
            'sage' => './null-sage.php',
        ],
    ],
    [
        'lecname' => 'Column Spaces',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/col/col.pdf',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCCEYLykkkV9fnt9cL3wemzI',
            'sage' => './col-sage.php',
        ],
    ],
    [],
    [
        'debut' => strtotime("+4 week", $fdoc),
        'col1' => 'Week 6',
        'lecname' => 'The Four Fundamental Subspaces',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/fundsub/fundsub.pdf',
        'comment' => '<strong style="color:red"> ⟵ meet on zoom 30-Sept!</strong>',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCBLO0R2e0Ss2J9eDYp_VO_-',
            'sage' => './fundsub-sage.php',
            'BF' => 'https://duke.zoom.us/j/92474098117?pwd=RE5rSVp3SC9sMmlWUDVORzBHUzVPQT09',
        ],
    ],
    [
        'lecname' => 'Linear Independence',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/linind/linind.pdf',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDq3mdRoSQZkeZxTNKJO4vO',
            'sage' => './linind-sage.php',
        ],
    ],
    [
        'lecname' => 'Bases',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/bases/bases.pdf',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCBPCxI_yBV9yQ56aqLg7Tbg',
            'sage' => './bases-sage.php',
        ],
    ],
    [],
    [
        'debut' => strtotime("+6 week", $fdoc),
        'col1' => 'Week 7',
        'lecname' => 'Dimension',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/dim/dim.pdf',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDv25ifWyybsRNPI4uS0Phj',
            'sage' => './dim-sage.php',
        ],
    ],
    [
        'lecname' => 'Orthogonality',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/orthog/orthog.pdf',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCD7Lhh9l271DtnbiSQIDsXk',
            'sage' => './orthog-sage.php',
        ],
    ],
    [],
    [
        'debut' => strtotime("+7 week", $fdoc),
        'col1' => 'Week 8',
        'lecname' => 'Projections',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/proj/proj.pdf',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCAgtrr5g8d6oR9U8fEphFgI',
            'sage' => './proj-sage.php',
        ],
    ],
    [
        'lecname' => 'Least Squares',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/xhat/xhat.pdf',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCCbbYdcEFhOnazvDCBkU3Mr',
            'sage' => './xhat-sage.php',
        ],
    ],
    [
        'lecname' => 'A=QR Factorizations',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/aqr/aqr.pdf',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCD6Gpj_xfFSJ0FeJgvNAeZ9',
            'sage' => './aqr-sage.php',
        ],
    ],
    [],
    [
        'debut' => strtotime("+7 week", $fdoc),
        'col1' => date("D d-M", mktime(hour: 0, day: 28, month: 10, year: 2022)),
        'lecname' => 'Exam II',
        'comment' => '<strong style="color:red"> ⟵ covers weeks 5-8</strong>',
        'leclink' => './218f22-exam02.pdf',
        'icons' => [
            'info' => './exam02.php',
            'key' => './218f22-exam02-solutions.pdf'
        ],
    ],
    [],
    [
        'debut' => strtotime("+8 week", $fdoc),
        'col1' => 'Week 9',
        'lecname' => 'The Gram-Schmidt Algorithm',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/gs/gs.pdf',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCCgVnzmc7GpajCxyYyqhRFa',
            'sage' => './gs-sage.php',
        ],
    ],
    [
        'lecname' => 'Determinants I',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/det1/det1.pdf',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDNySC8M3lT8qhXYIgEq1de',
            'sage' => './det1-sage.php',
        ],
    ],
    [], /* SPRING BREAK */
    [
        'debut' => strtotime("+9 week", $fdoc),
        'col1' => 'Week 10',
        'lecname' => 'Determinants II',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/det2/det2.pdf',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCCYDXbKfr21FC2rfDcs5ylV',
            'sage' => './det2-sage.php',
        ],
    ],
    [
        'lecname' => 'Complex Numbers',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/clx/clx.pdf',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCASNHStI6lJFrnsrFHyJ0oF',
            'sage' => './clx-sage.php',
        ],
    ],
    [
        'lecname' => 'Polynomial Algebra',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/poly/poly.pdf',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCC2F9MY2i0IYSbB8_mIqWrW',
            'sage' => './poly-sage.php',
        ],
    ],
    [],
    [
        'debut' => strtotime("+10 week", $fdoc),
        'col1' => 'Week 11',
        'lecname' => 'The Characteristic Polynomial',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/chi/chi.pdf',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDsS8d6cKcDXeEOwXZ-DSXf',
            'sage' => './chi-sage.php',
        ],
    ],
    [
        'lecname' => 'Diagonalization',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/diag/diag.pdf',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCCJ3Ym-gfpi7jS66_grk2Bb',
            'sage' => './diag-sage.php',
        ],
    ],
    [
        'lecname' => 'Matrix Exponentials',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/exp/exp.pdf',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDIB1xn7WUZNEaefgxM0Jby',
            'sage' => './exp-sage.php',
            'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/exp/exp-ex.pdf',
        ],
    ],
    [],
    [
        'debut' => strtotime("+11 week", $fdoc),
        'col1' => 'Week 12',
        'lecname' => 'The Spectral Theorem',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/spectral/spectral.pdf',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCC0g6fs8EAsE4RMyJ_L-Vmc',
            'sage' => './spectral-sage.php',
        ],
    ],
    [
        'lecname' => 'Definiteness',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/posdef/posdef.pdf',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCAY-1mnoguKeJF-xe7vUP_e',
            'sage' => './posdef-sage.php',
            'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/posdef/posdef-ex.pdf',
        ],
    ],
    [],
    [
        'debut' => strtotime("+12 week", $fdoc),
        'col1' => 'Week 13',
        'lecname' => 'Partial Derivatives',
        'comment' => '<strong style="color:red"> ⟵ (w/quiz but no pset)</strong>',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/partial/partial.pdf',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCAHo2FLv4X5VFejOThbjJuf',
            'sage' => './partial-sage.php',
        ],
    ],
    [],
    [
        'debut' => strtotime("+12 week", $fdoc),
        'col1' => date("D d-M", mktime(hour: 0, day: 2, month: 12, year: 2022)),
        'lecname' => 'Exam III',
        'comment' => '<strong style="color:red"> ⟵ covers weeks 9-13</strong>',
        'leclink' => './218f22-exam03.pdf',
        'icons' => [
            'info' => './exam03.php',
            'key' => './218f22-exam03-solutions.pdf',
        ],
    ],
    [],
    [
        'debut' => strtotime("+13 week", $fdoc),
        'col1' => 'Week 14',
        'lecname' => 'Singular Value Decomposition',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/svd/svd.pdf',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCBGtVOACD7lW9ejEMY8Tq8B',
            'sage' => './svd-sage.php',
            'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/svd/svd-ex.pdf',
        ],
    ],
    [],
    [
        'debut' => strtotime("+14 week", $fdoc),
        'col1' => 'Week 15',
        'lecname' => 'Linear Approximations',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/jacobian/jacobian.pdf',
        'comment' => '<strong style="color:red"> ⟵ pset not collected!</strong>',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCAncXe-cm8_Da5MtglnsLoQ',
            'sage' => './jacobian-sage.php',
            'memo' => './jacobian-hw.pdf',
        ],
    ],
    [
        'lecname' => 'Cholesky Factorizations',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/cholesky/cholesky.pdf',
        'comment' => '<strong style="color:red"> ⟵ pset not collected!</strong>',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCApkxXB-XIDhEDso4nDcO_f',
            'sage' => './cholesky-sage.php',
            'memo' => './cholesky-hw.pdf',
        ],
    ],
    [
        'lecname' => 'The Hessian',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/hessian/hessian.pdf',
        'comment' => '<strong style="color:red"> ⟵ pset not collected!</strong>',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDvM1pjbv0O5FPs2RAVMHPm',
            'sage' => './hessian-sage.php',
            'memo' => './hessian-hw.pdf',
        ],
    ],
    [],
    [
        'debut' => strtotime("+14 week", $fdoc),
        'col1' => date("D d-M", mktime(hour: 0, day: 18, month: 12, year: 2022)),
        'lecname' => 'Final Exam',
        'leclink' => './final.php',
        'comment' => '<strong style="color:red"> ⟵ click for info!</strong>',
        /* 'icons' => [
         *     'info' => './final.php',
         * ], */
    ],
];
if (basename($_SERVER["SCRIPT_FILENAME"], '.php') == 'index') {
    mktbl($schedule);
} else {
    mktbl($schedule, rolling: false);
}
?>
