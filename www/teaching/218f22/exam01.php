<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead() ?>

<h2>Exam I</h2>

<p> Exam I is scheduled to take place in class on <code>Mon 3-Oct (rescheduled)</code> and will cover all
    topics from <code>Weeks 1-4</code>.</p>

<p> The exam will be four single-sided pages and consist of mostly free-response
    problems with a few fill in the blank and multiple choice questions. You
    should be prepared to solve problems that you have not seen before.</p>

<p> The exam will be "closed" in the sense that no outside aid (like books or
    calculators) will be allowed. The only thing you need to bring is a writing
    utensil.</p>

<p> To prepare for the exam, you should be able to:</p>

<ul>
    <li>Correctly articulate all definitions and theorems from lecture.</li>
    <li>Clearly and coherently solve all examples from lecture.</li>
    <li>Write correct and legible solutions to all problems from the problem
        sets and <a href="./218f22-e01--study.pdf">discussion worksheets</a>.</li>
    <li>Clearly explain how to solve each comprehension quiz problem.</li>
</ul>

<p> If you feel confident that you can accomplish everything described above and
    would like some additional practice, here are some <code>optional</code> problems
    from Gilbert Strang's textbook. Solutions to these problems will not be
    provided, but I will happily discuss them in office hours.</p>

<ul>
    <li>Section 1.1 Exercises 1-4, 6, 31</li>
    <li>Section 1.2 Exercises 4, 6(a), 8, 12-14, 31</li>
    <li>Section 1.3 Exercises 3, 4, 6, 8, 10 (ignore the term "independent" in all these problems)</li>
    <li>Section 2.1 Exercises 9-12, 13(a), 18, 19, 32</li>
    <li>Section 2.2 Exercises 5, 9, 12, 19</li>
    <li>Section 2.3 Exercises 1-3, 6, 23, 25</li>
    <li>Section 2.4 Exercises 1, 2, 15, 17(a, b)</li>
    <li>Section 2.5 Exercises 6-9, 12, 18, 27</li>
    <li>Section 2.6 Exercises 8</li>
    <li>Section 2.7 Exercises 12, 16, 17(a), 19, 22</li>
    <li>Section 3.3 Exercises 18-21, 31, 33</li>
</ul>

<p> My Fall 2021 and Spring 2022 Exam I and solutions are posted below. These could be useful study tools, but don't expect this semester's exam to look anything like the previous semesters.</p>

<ul>
    <li><a href="./218f21-exam01.pdf">Fall 2021</a> (<a href="./218f21-exam01-solutions.pdf">solutions</a>)</li>
    <li><a href="./218s22-exam01.pdf">Spring 2022</a> (<a href="./218s22-exam01-solutions.pdf">solutions</a>)</li>
</ul>
