<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Diagonalization</h2>
<p> The following code will attempt to diagonalize an input matrix <code>A</code>.
<div class="compute">
<script type="text/x-sage">
A = matrix([[41, 42, -28, -4, -100], [-37, -37, 28, 1, 102], [17, 14, 47, 65, 162], [-13, -14, 0, -6, -2], [-1, 0, -14, -17, -49]])

D, X = A.jordan_form(transformation=True)
print(f'A =\n{A}\n')
if not D.is_diagonal(): raise TypeError('Matrix is not diagonalizable!')
print(f'X =\n{X}\n')
print(f'D =\n{D}\n')
print(f'X^-1 =\n{X.inverse()}')
</script>
</div>
