<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Vector Geometry</h2>

<p>We can plot a vector with the syntax <code>v.plot()</code>.

<div class="compute">
<script type="text/x-sage">
v = vector([1, 2])

v.plot()
</script>
</div>

<p>By default, our vector emanates from the origin. We can specify a different
   tail with the syntax <code>v.plot(start=(new tail))</code>.

<div class="compute">
<script type="text/x-sage">
v = vector([1, 2])

v.plot(start=(-3, 1), color='green')
</script>
</div>

<p>We can also plot multiple vectors together by adding plots.

<div class="compute">
<script type="text/x-sage">
v = vector([1, 2])
w = vector([3, -4])

v.plot() + w.plot(color='green') + (v-w).plot(start=w, color='red')
</script>
</div>

<p>The length and normalization of a vector are given by the
  syntax <code>v.norm()</code> and <code>v.normalized()</code>.

<div class="compute">
<script type="text/x-sage">
v = vector([1, -2, -3, 4])

print(f'v = {v}\n')
print(f'|v| = {v.norm()}')
print(f'v^ = {v.normalized()}\n')
</script>
</div>

<p>The inner product of two vectors is given by <code>v * w</code>.

<div class="compute">
<script type="text/x-sage">
v = vector([1, -2, -3, 4])
w = vector([-9, 2, -8, 7])

print(f'v = {v}')
print(f'w = {w}\n')
print(f'<v, w> = {v * w}\n')
</script>
</div>
