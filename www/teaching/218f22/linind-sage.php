<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Linear Independence</h2>
<p>To determine if a list of vectors is linearly independent, we can put them
   into the columns of a matrix and check that the nullity is zero. We can
   accomplish this with the syntax <code>matrix.column([list of
   vectors]).right_nullity() == 0</code>.
<div class="compute">
<script type="text/x-sage">
vectors = [(-7, 4, 4, -5), (34, -15, -19, 25), (-200, 91, 112, -141), (454, -204, -254, 322)]

A = matrix.column(vectors)
print(f'Vectors are the columns of A=\n{A}\n')
print(f'rref(A)=\n{A.rref()}\n')
print(f'Is the list linearly independent? {A.right_nullity() == 0}')
</script>
</div>
