<?php
return [
    'h1' => 'Math 218: Matrices and Vectors',
    'icons' => [
        ['./', 'schedule', 'list.svg'],
        ['./cal.php', 'calendar', 'calendar.svg'],
        ['https://duke.zoom.us/j/92474098117?pwd=RE5rSVp3SC9sMmlWUDVORzBHUzVPQT09', 'Brian\'s Zoom (passcode: linear)', 'BF.png'],
        ['https://duke.zoom.us/j/96653764077', 'Yixin\'s Zoom', 'YT.png'],
        ['https://duke.zoom.us/j/97308350791?pwd=eUJ1QTl5N3d5bGxTQ25Ddnpia2dUdz09', 'Hwai-Ray\'s Zoom', 'HT.png'],
        ['https://duke.zoom.us/j/3588900846', 'Yuqing\'s Zoom', 'YD.png'],
        ['https://duke.zoom.us/j/96510764502', 'Yupei\'s Zoom', 'YH.png'],
        ['https://duke.zoom.us/j/93806074652?pwd=b1hRTWdUSlBrdklUL3E5ZWtnVDNDUT09', 'Yupeng\'s Zoom', 'YL.png'],
        ['https://duke.zoom.us/j/4831704939', 'Bowen\'s Zoom', 'BL.png'],
        ['https://gradescope.com', 'gradescope', 'gradescope.png'],
        ['https://gitlab.oit.duke.edu/bdf10/course-policies/-/raw/master/218f22/218f22-policies.pdf', 'policies', 'scroll.svg'],
    ],
    'msg' => "<p>Welcome! You have found the homepage of the Fall 2022 manifestation of Math 218.</p>\n<code>PLEASE FILL OUT YOUR COURSE EVALUATIONS</code>"
];
?>
