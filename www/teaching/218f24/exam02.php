<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead() ?>

<h2>Exam II</h2>

<p> Exam II is scheduled to take place in class on <code>Fri 25-October</code> and will cover all
    topics from <code>Weeks 5-8</code>.</p>

<p> The exam will be four single-sided pages and consist of a mixture of
    free-response, multiple choice, and fill in the blank problems. You should
    be prepared to solve problems that you have not seen before.</p>

<p> The exam will be "closed" in the sense that no outside aid (like books or
    calculators) will be allowed. The only thing you need to bring is a writing
    utensil.</p>

<p> To prepare for the exam, you should be able to:</p>

<ul>
    <li>Correctly articulate all definitions and theorems from lecture.</li>
    <li>Clearly and coherently solve all examples from lecture.</li>
    <li>Write correct and legible solutions to all problems from the problem
        sets and discussion worksheets.</li>
    <li>Clearly explain how to solve each comprehension quiz problem.</li>
</ul>

<p> If you feel confident that you can accomplish everything described above and
    would like some additional practice, you can look at some of the <a href="./exams.php">Exam
    II's from previous semesters</a>. These could be useful study tools, but don't
    expect this semester's exam to look anything like the previous semesters.</p>
