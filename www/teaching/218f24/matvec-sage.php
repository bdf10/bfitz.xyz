<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Matrices and Vectors</h2>
<p>Sage is an open source computer algebra system written python.</p>

<p>Matrices are defined as lists of rows via the syntax <code>A = matrix([(1, 2,
    3), (4, 5, 6)])</code>.</p>

<?php
sagecell(<<<'EOF'
A = matrix([(1, 2, 3), (4, 5, 6)])

print(f'A=\n{A}')
EOF)
?>

<p>Matrices can be summed <code>A + B</code>, scaled <code>c * A</code>, and
    transposed <code>A.T</code>.</p>

<?php
sagecell(<<<'EOF'
A = matrix([(1, 2, 3), (4, 5, 6)])
B = matrix([(3, -7, 4), (5, 9, 6)])

print(f'A=\n{A}\n')
print(f'B=\n{B}\n')

print(f'A+B=\n{A+B}\n')
print(f'2*A=\n{2*A}\n')
print(f'A^T=\n{A.T}')
EOF)
?>

<p>The trace of an <code>nxn</code> matrix <code>A</code> can be calculated with <code>A.trace()</code>.</p>

<?php
sagecell(<<<'EOF'
A = matrix([(1, 2, 3), (4, 5, 6), (7, 8, 9)])

print(f'A=\n{A}\n')

print(f'trace(A)={A.trace()}')
EOF)
?>

<p>Vectors are defined with the syntax <code>vector([coordinate list])</code>.</p>

<?php
sagecell(<<<'EOF'
v = vector([1, 2, 3, 4])

print(f'v = {v}')
EOF)
?>

<p>Vectors can be scaled <code>c*v</code> and summed <code>v+w</code>.</p>

<?php
sagecell(<<<'EOF'
v = vector([1, 2, 3, 4])
w = vector([0, -3, 5, 1])

print(f'v = {v}')
print(f'w = {w}\n')

print(f'6*v = {6*v}')
print(f'v+w = {v+w}')
EOF)
?>
