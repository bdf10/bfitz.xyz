<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead() ?>

<h2>Exam Repository</h2>

<!-- https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/ -->

<table>
    <tr>
        <th style="text-align: left">Course</th>
        <th style="text-align: left">Semester</th>
        <th style="text-align: left">Exam</th>
        <th style="text-align: left">Solutions</th>
    </tr>

    <!-- Math 218 -->
    <!-- Fall 2021 -->
    <tr>
        <td>Math 218</td>
        <td>Fall 2021</td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/f21/exam01/218f21-exam01.pdf">Exam I</a></td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/f21/exam01/218f21-exam01--solutions.pdf"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
        <td>Math 218</td>
        <td>Fall 2021</td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/f21/exam02/218f21-exam02.pdf">Exam II</a></td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/f21/exam02/218f21-exam02--solutions.pdf"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
        <td>Math 218</td>
        <td>Fall 2021</td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/f21/exam03/218f21-exam03.pdf">Exam III</a></td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/f21/exam03/218f21-exam03--solutions.pdf"><img src="/pix/icons/key.svg"></a></td>
    </tr>
    <tr class="blank_row"><td></td></tr>
    
    <!-- Spring 2022 -->
    <tr>
        <td>Math 218</td>
        <td>Spring 2022</td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/s22/exam01/218s22-exam01.pdf">Exam I</a></td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/s22/exam01/218s22-exam01-solutions.pdf"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
        <td>Math 218</td>
        <td>Spring 2022</td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/s22/exam02/218s22-exam02.pdf">Exam II</a></td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/s22/exam02/218s22-exam02-solutions.pdf"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
        <td>Math 218</td>
        <td>Spring 2022</td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/s22/exam03/218s22-exam03.pdf">Exam III</a></td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/s22/exam03/218s22-exam03-solutions.pdf"><img src="/pix/icons/key.svg"></a></td>
    </tr>
    <tr class="blank_row"><td></td></tr>

    <!-- Fall 2022 -->
    <tr>
        <td>Math 218</td>
        <td>Fall 2022</td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/f22/exam01/218f22-exam01.pdf">Exam I</a></td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/f22/exam01/218f22-exam01-solutions.pdf"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
        <td>Math 218</td>
        <td>Fall 2022</td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/f22/exam02/218f22-exam02.pdf">Exam II</a></td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/f22/exam02/218f22-exam02-solutions.pdf"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
        <td>Math 218</td>
        <td>Fall 2022</td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/f22/exam03/218f22-exam03.pdf">Exam III</a></td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/f22/exam03/218f22-exam03-solutions.pdf"><img src="/pix/icons/key.svg"></a></td>
    </tr>
    <tr class="blank_row"><td></td></tr>

    <!-- Spring 2023 -->
    <tr>
        <td>Math 218</td>
        <td>Spring 2023</td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/s23/exam01/218s23-exam01.pdf">Exam I</a></td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/s23/exam01/218s23-exam01-solutions.pdf"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
        <td>Math 218</td>
        <td>Spring 2023</td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/s23/exam02/218s23-exam02.pdf">Exam II</a></td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/s23/exam02/218s23-exam02-solutions.pdf"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
        <td>Math 218</td>
        <td>Spring 2023</td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/s23/exam03/218s23-exam03.pdf">Exam III</a></td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/s23/exam03/218s23-exam03-solutions.pdf"><img src="/pix/icons/key.svg"></a></td>
    </tr>
    <tr class="blank_row"><td></td></tr>

    <!-- Fall 2023 -->
    <tr>
        <td>Math 218</td>
        <td>Fall 2023</td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/f23/exam01/218f23-exam01.pdf">Exam I</a></td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/f23/exam01/218f23-exam01-solutions.pdf"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
        <td>Math 218</td>
        <td>Fall 2023</td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/f23/exam02/218f23-exam02.pdf">Exam II</a></td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/f23/exam02/218f23-exam02-solutions.pdf"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
        <td>Math 218</td>
        <td>Fall 2023</td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/f23/exam03/218f23-exam03.pdf">Exam III</a></td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/f23/exam03/218f23-exam03-solutions.pdf"><img src="/pix/icons/key.svg"></a></td>
    </tr>
    <tr class="blank_row"><td></td></tr>

    <!-- Spring 2024 -->
    <tr>
        <td>Math 218</td>
        <td>Spring 2024</td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/s24/exam01/218s24-exam01.pdf">Exam I</a></td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/s24/exam01/218s24-exam01-solutions.pdf"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
        <td>Math 218</td>
        <td>Spring 2024</td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/s24/exam02/218s24-exam02.pdf">Exam II</a></td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/s24/exam02/218s24-exam02-solutions.pdf"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
        <td>Math 218</td>
        <td>Spring 2024</td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/s24/exam03/218s24-exam03.pdf">Exam III</a></td>
        <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/s24/exam03/218s24-exam03-solutions.pdf"><img src="/pix/icons/key.svg"></a></td>
    </tr>

</table>
