<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Secret Santa!</h2>
<p>Let's solve the secret santa problem using digraphs!</p>
<?php
sagecell(<<<'EOF'
# input your list of friends in the list `friends`
friends = ['Gauss', 'Jordan', 'Euler', 'Gram', 'Schmidt']

# for replicable randomness, input a `seed_value` and set `random_seed` to this
# value
# seed_value = 2023
# set_random_seed(seed_value)

# uncomment to introduce randomness (will not replicate without setting
# `seed_value`)
# shuffle(friends)

# keep track of who is not allowed to gift to whom using the list `nogift`
# type `nogift=()` if there are no constraints on gift-giving
nogift = [('Gram', 'Schmidt'), ('Schmidt', 'Gram'), ('Gauss', 'Jordan')]

# the default `nogift above indicates that
# + 'Gram' cannot give to 'Schmidt'
# + 'Schmidt' cannot give to 'Gram'
# + 'Gauss' cannot give to 'Jordan'

def santa_graph(friends, nogift=()):
    g = digraphs.Complete(len(friends)).relabel(friends, inplace=False)
    g.delete_edges(nogift)
    return g

def secret_santa_cycle(friends, nogift=()):
    return santa_graph(friends=friends, nogift=nogift).hamiltonian_cycle()

santa_graph(friends, nogift).plot(vertex_size=2500, vertex_colors='#F8B229', graph_border=True).show(title='Possibilities Digraph')
print('')
secret_santa_cycle(friends, nogift).plot(vertex_size=2500, vertex_colors='#F8B229', graph_border=True, layout='spring').show(title='Secret Santa Solution')
EOF)
?>
