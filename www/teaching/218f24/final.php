<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead() ?>

<h2>Final Exam</h2>

<p> The Final Exam is scheduled to take place on <code>Saturday 14-December</code> from
    <code>2:00 PM to 5:00 PM</code>.</p>

<p> You will either take the final exam in <code>White Lecture Hall</code>, <code>East Duke 204D</code>, <code>East Duke 204B</code>, or <code>East Duke 209</code>,
    depending on which discussion section you are enrolled in. Please verify your discussion number on DukeHub.</p>

<ul>
    <li><code>White Lecture Hall</code></li>
    <ul>
        <li>MATH 218D-2 01D-DIS (5127)</li>
        <li>MATH 218D-2 02D-DIS (5128)</li>
        <li>MATH 218D-2 03D-DIS (5129)</li>
        <li>MATH 218D-2 04D-DIS (5130)</li>
        <li>MATH 218D-2 05D-DIS (5131)</li>
    </ul>
    <li><code>East Duke 204D</code></li>
    <ul>
        <li>MATH 218D-2 06D-DIS (5132)</li>
    </ul>
    <li><code>East Duke 204B</code></li>
    <ul>
        <li>MATH 218D-2 07D-DIS (5133)</li>
        <li>MATH 218D-2 08D-DIS (5134)</li>
    </ul>
    <li><code>East Duke 209</code></li>
    <ul>
        <li>MATH 218D-2 09D-DIS (5135)</li>
        <li>MATH 218D-2 10D-DIS (5136)</li>
    </ul>
</ul>

<p> The exam will consist of mostly free-response problems with a few fill in
    the blank and multiple choice questions. You should be prepared to solve
    problems that you have not seen before.</p>

<p> The exam is <code>cumulative</code>, so you should be prepared for every
    topic covered throughout the semester to be represented on the exam.</p>

<p> The exam will be "closed" in the sense that no outside aid (like books or
    calculators) will be allowed. The only thing you need to bring is a writing
    utensil.</p>

<p> The schedule of office hours leading up to the final will be:</p>
<ul>
    <li><code>Brian's Office Hours</code></li>
    <ul>
        <li>M-F 8:30am-10:30am (zoom)</li>
    </ul>
    <li><code>Bobby's Office Hours</code></li>
    <ul>
        <li>M 9am-12pm (Gross Hall 359)</li>
    </ul>
    <li><code>Jason's Office Hours</code></li>
    <ul>
        <li>Tu 10am-1pm (Physics 274J)</li>
    </ul>
    <li><code>Fernando's Office Hours</code></li>
    <ul>
        <li>Tu 1:30pm-2:30pm (Gross Hall 352)</li>
        <li>W 12pm-2pm (Gross Hall 352)</li>
    </ul>
    <li><code>Rohit's Office Hours</code></li>
    <ul>
        <li>M 12pm-1pm (Gross Hall 351)</li>
        <li>Tu 1pm-3pm (<a href="https://duke.zoom.us/j/95507053573?pwd=fBK0Eiwh283HVna4jfV2ltODD5ZSGw.1">Zoom</a> use this link, not the one Brian uses)</li>
    </ul>
    <li><code>Zijun's Office Hours</code></li>
    <ul>
        <li>M 12pm-3pm (Physics 274J)</li>
    </ul>
    <li><code>Help Room</code></li>
    <ul>
        <li>MTu 7pm-10pm (Physics 274J)</li>
    </ul>
</ul>

<p> These office hours are neatly advertised on the course calendar.</p>

<p> To prepare for the exam, you should be able to:</p>

<ul>
    <li>Correctly articulate all definitions and theorems from lecture.</li>
    <li>Clearly and coherently solve all examples from lecture.</li>
    <li>Write correct and legible solutions to all problems from the midterm
        exams, problem sets, and discussion worksheets.</li>
    <li>Clearly explain how to solve each comprehension quiz problem.</li>
</ul>
