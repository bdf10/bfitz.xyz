<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Singular Value Decomposition</h2>
<p>The following code will calculate a spectral
   factorization <code>A^*A=VDV^*</code> of the Gramian of any
   matrix <code>A</code>, provided that the eigenvalues of <code>A^*A</code> are
   nice enough. This is the hardest step in calculating a singular value
   decomposition.
<div class="compute">
<script type="text/x-sage">
A = matrix([(1, 4, 0), (-4, 3, 1), (0, 1, -2), (0, 1, 3)])


H = A.H*A
V = matrix.column(flatten(list((map(lambda t: matrix(map(lambda v: v.normalized(), matrix(t[1]).gram_schmidt()[0])).rows(), H.eigenvectors_right())))))
D = V.H*A.H*A*V
print(f'A = \n{A}\n')
print(f'V = \n{V}\n')
print(f'D = \n{D}')
</script>
</div>
