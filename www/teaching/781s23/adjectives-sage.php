<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Adjectives</h2>
<p>Properties can be calculated with the
    syntax <code>A.property_name()</code>. A full list of properties can be
    found in the <a href="https://doc.sagemath.org/html/en/constructions/linear_algebra.html">official
    documentation</a>.</p>

<?php
sagecell(<<<'EOF'
A = matrix([(1, 2, 3), (4, 5, 6), (7, 8, 9)])

print(f'number of rows of A: {A.nrows()}\n')
print(f'number of cols of A: {A.ncols()}\n')
print(f'diagonal of A:\n{A.diagonal()}\n')
print(f'is A symmetric?\n{A.is_symmetric()}')
EOF)
?>

<p>Sage also has useful matrix constructors built in,
    like <code>zero_matrix</code>, <code>ones_matrix</code>, <code>identity_matrix</code>,
    and <code>diagonal_matrix</code>.</p>

<?php
sagecell(<<<'EOF'
Z = zero_matrix(3, 4)
A = ones_matrix(6, 3)
I = identity_matrix(9)
D = diagonal_matrix([-7, 9, 5])

print(f'3x4 zero matrix:\n{Z}\n')
print(f'6x3 ones matrix:\n{A}\n')
print(f'9x9 identity matrix:\n{I}\n')
print(f'D=\n{D}')
EOF)
?>
