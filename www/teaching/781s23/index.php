<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead() ?>

<h2>Schedule</h2>

<p>Quizzes and problem sets are distributed through gradescope.</p>

<?php
$fdoc = mktime(hour: 6, day: 9, month: 1, year: 2023);
include($_SERVER['DOCUMENT_ROOT'].'/scripts/topics.php');
$schedule = [
    ['debut' => strtotime("-4 week", $fdoc),'col1' => 'Week 0'] +
    $welcome_781,
    [],
    ['debut' => strtotime("-4 week", $fdoc),'col1' => 'Week 1'] +
    $matvec,
    $adjectives + ['comment' => '<strong style="color:red"> ⟵ complete at home!</strong>'],
    [],
    ['debut' => strtotime("+1 week 1 days", $fdoc),'col1' => 'Week 2'] +
    $lincomb,
    $digraphs,
    [],
    ['debut' => strtotime("+2 week", $fdoc),'col1' => 'Week 3'] +
    $geometry,
    $matmult,
    [],
    ['debut' => strtotime("+3 week", $fdoc),'col1' => 'Week 4'] +
    $rref,
    $axb,
    [],
    ['debut' => strtotime("+4 week", $fdoc),'col1' => 'Week 5'] +
    $gj,
    $nonsing,
    [],
    ['debut' => strtotime("+5 week", $fdoc),'col1' => 'Week 6'] +
    $ear,
    $palu,
    $evals,
    [],
    ['debut' => strtotime("+6 week", $fdoc),'col1' => 'Week 7'] +
    $null,
    $col,
    $fundsub,
    [],
    ['debut' => strtotime("+7 week", $fdoc),'col1' => 'Week 8'] +
    $linind,
    $bases,
    $dim,
    [],
    ['debut' => strtotime("+8 week", $fdoc),'col1' => 'Week 9'] +
    $orthog,
    $proj,
    $xhat,
    [],
    ['debut' => strtotime("+9 week", $fdoc),'col1' => 'Week 10'] +
    $aqr,
    $gs,
    [],
    ['debut' => strtotime("+11 week", $fdoc),'col1' => 'Week 11'] +
    $det1,
    $det2,
    $clx,
    [],
    ['debut' => strtotime("+12 week", $fdoc),'col1' => 'Week 12'] +
    $poly,
    $chi,
    $diag,
    [],
    ['debut' => strtotime("+13 week", $fdoc),'col1' => 'Week 13'] +
    $spectral,
    $posdef,
    [],
    ['debut' => strtotime("+14 week", $fdoc),'col1' => 'Week 14'] +
    $svd,
];
if (basename($_SERVER["SCRIPT_FILENAME"], '.php') == 'index') {
    mktbl($schedule);
} else {
    mktbl($schedule, rolling: false);
}
?>
