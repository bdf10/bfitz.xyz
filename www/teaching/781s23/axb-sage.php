<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Linear Systems</h2>
<p>We may define augmented matrices with the syntax <code>A.augment(b,
   subdivide=True)</code>.
<div class="compute">
<script type="text/x-sage">
A = matrix([(1, -3, 4), (2, 8, 7), (-5, 2, 1), (0, 4, 7)])
b = vector([6, -9, 2, 1])

print(A.augment(b, subdivide=True))
</script>
</div>

<p>The code below will apply the statement of the Rouche-Capelli Theorem to a
   system given in row echelon form.
<div class="compute">
<script type="text/x-sage">
A = matrix([[2, 7, -11, 6, 9], [0, 3, 14, 2, 5], [0, 0, 0, -4, 6], [0, 0, 0, 0, 0]])
b = vector([3, -2, 4, 0])


def in_row_echelon_form(A):
    l = list(map(lambda r: r.is_zero(), A))
    if l != sorted(l):
	    return False
    for j, r in zip(A.pivots(), A):
	    if j != next((i for i, a in enumerate(r) if a), None):
		    return False
    return True

def dependent_variables(M):
    return r'{' + ', '.join([f'x_{i+1}' for i in M.pivots() if i != M.ncols()]) + r'}'

def free_variables(M):
    return r'{' + ', '.join([f'x_{i+1}' for i in range(M.ncols()-1) if i not in M.pivots()]) + r'}'

def number_of_solutions(M):
    if M.ncols() in M.pivots():
	    return 'zero'
    if M.right_nullity()-1 > 0:
	    return 'infinitely many'
    return 'exactly one'

M = A.augment(b, subdivide=True)
print(M)
if not in_row_echelon_form(M):
    raise TypeError('System is not in row echelon form!')

print(f'\nIs the system consistent? {not M.ncols()-1 in M.pivots()}')
print(f'Dependent variables: {dependent_variables(M)}')
print(f'Free variables: {free_variables(M)}')
print(f'Number of solutions: {number_of_solutions(M)}')
</script>
</div>
