<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead() ?>

<h2>Exam III</h2>

<p> Exam III is scheduled to take place in class on <code>Fri 21-April</code> and will cover all
    topics from <code>Weeks 10-13</code>.</p>

<p> The exam will be four single-sided pages and consist of mostly free-response
    problems with a few fill in the blank and multiple choice questions. You
    should be prepared to solve problems that you have not seen before.</p>

<p> The exam will be "closed" in the sense that no outside aid (like books or
    calculators) will be allowed. The only thing you need to bring is a writing
    utensil.</p>

<p> To prepare for the exam, you should be able to:</p>

<ul>
    <li>Correctly articulate all definitions and theorems from lecture.</li>
    <li>Clearly and coherently solve all examples from lecture.</li>
    <li>Write correct and legible solutions to all problems from the problem
        sets and <a href="./218f22-e03-study.pdf">discussion worksheets</a>.</li>
    <li>Clearly explain how to solve each comprehension quiz problem.</li>
</ul>

<p> If you feel confident that you can accomplish everything described above and
    would like some additional practice, here are some <code>optional</code> problems
    from Gilbert Strang's textbook. Solutions to these problems will not be
    provided, but I will happily discuss them in office hours.</p>

<ul>
    <li>Section 4.4 Exercises 3, 5, 7, 21, 22</li>
    <li>Section 5.1 Exercises 1-4, 10, 14, 16, 18, 29</li>
    <li>Section 5.2 Exercises 3, 11, 12, 20</li>
    <li>Section 5.3 Exercises 1, 5, 6-9</li>
    <li>Section 6.2 Exercises 1, 2, 4, 6, 7, 13, 14, 17-19, 27</li>
    <li>Section 6.3 Exercises 4-6, 19, 21, 24</li>
    <li>Section 6.4 Exercises 1, 5-8, 9(a), 9(c), 15</li>
    <li>Section 6.5 Exercises 4, 7, 8, 16, 20</li>
    <li>Section 6.5 Exercises 2, 3, 5, 7, 25, 26, 30, 31</li>
    <li>Section 9.1 Exercises 1, 9, 10, 11</li>
    <li>Section 9.2 Exercises 1-3, 5, 12, 14</li>
</ul>

<p> My Fall 2021, Spring 2022, and Fall 2022 Exam III and solutions are posted below. These could be useful study tools, but don't expect this semester's exam to look anything like the previous semesters.</p>

<ul>
    <li><a href="./218f21-exam03.pdf">Fall 2021</a> (<a href="./218f21-exam03--solutions.pdf">solutions</a>)</li>
    <li><a href="./218s22-exam03.pdf">Spring 2022</a> (<a href="./218s22-exam03-solutions.pdf">solutions</a>)</li>
    <li><a href="./218f22-exam03.pdf">Fall 2022</a> (<a href="./218f22-exam03-solutions.pdf">solutions</a>)</li>
</ul>
