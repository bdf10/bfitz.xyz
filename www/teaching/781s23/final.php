<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead() ?>

<h2>Final Exam</h2>

<code>PLEASE FILL OUT YOUR COURSE EVALUATIONS</code>

<p> The Final Exam is scheduled to take place on <code>Sunday 18-December</code> from
    <code>7:00 PM to 10:00 PM</code>.</p>

<p> You will either take the final exam in <code>Gross Hall 107</code> or <code>Gross Hall 103</code>,
    depending on which discussion section you are enrolled in. Please verify your discussion number on DukeHub.</p>

<ul>
    <li><code>Gross Hall 107</code></li>
    <ul>
        <li>Math 218D-2-03D (Yuqing Dai)</li>
        <li>Math 218D-2-04D (Yuqing Dai)</li>
        <li>Math 218D-2-05D (Bowen Li)</li>
        <li>Math 218D-2-07D (Yupeng Li)</li>
        <li>Math 218D-2-08D (Yupeng Li)</li>
        <li>Math 218D-2-09D (Bowen Li)</li>
    </ul>
    <li><code>Gross Hall 103</code></li>
    <ul>
        <li>MATH 218D-2.01D (Yixin Tan)</li>
        <li>MATH 218D-2.02D (Hwai-Ray Tung)</li>
    </ul>
</ul>

<p> The exam will consist of mostly free-response problems with a few fill in
    the blank and multiple choice questions. You should be prepared to solve
    problems that you have not seen before.</p>

<p> The exam is <code>cumulative</code>, so you should be prepared for every
    topic covered throughout the semester to be represented on the exam.</p>

<p> The exam will be "closed" in the sense that no outside aid (like books or
    calculators) will be allowed. The only thing you need to bring is a writing
    utensil.</p>

<p> The schedule of office hours leading up to the final will be:</p>
<ul>
    <li>Brian's Office Hours</li>
    <ul>
        <li>Monday 12-December 11am-1pm (Zoom: <a href="https://duke.zoom.us/j/92474098117?pwd=RE5rSVp3SC9sMmlWUDVORzBHUzVPQT09"><img src="/pix/icons/BF.png" style="height: 1.00em !important"></a>)</li>
        <li>Wednesday 14-December 9am-11am (Zoom: <a href="https://duke.zoom.us/j/92474098117?pwd=RE5rSVp3SC9sMmlWUDVORzBHUzVPQT09"><img src="/pix/icons/BF.png" style="height: 1.00em !important"></a>)</li>
        <li>Thursday 15-December 9am-11am (Zoom: <a href="https://duke.zoom.us/j/92474098117?pwd=RE5rSVp3SC9sMmlWUDVORzBHUzVPQT09"><img src="/pix/icons/BF.png" style="height: 1.00em !important"></a>)</li>
    </ul>
    <li>Yixin's Office Hours</li>
    <ul>
        <li>Monday 12-December 9am-11am (Zoom: <a href="https://duke.zoom.us/j/96653764077" title="Yixin's Zoom (passcode: linear)"><img src="/pix/icons/YT.png" style="height: 1.00em !important"></a>)</li>
        <li>Tuesday 13-December 9am-11am (Zoom: <a href="https://duke.zoom.us/j/96653764077" title="Yixin's Zoom (passcode: linear)"><img src="/pix/icons/YT.png" style="height: 1.00em !important"></a>)</li>
    </ul>
    <li>Ray's Office Hours</li>
    <ul>
        <li>Monday 12-December 2pm-4pm (Physics 274J)</li>
    </ul>
    <li>Bowen's Office Hours</li>
    <ul>
        <li>Monday 12-December 7pm-10pm (Gross 324)</li>
        <li>Wednesday 14-December 7pm-10pm (Gross 324)</li>
    </ul>
    <li>Yupeng's Office Hours</li>
    <ul>
        <li>Wednesday 14-December 11am-12:30pm (Gross 351)</li>
        <li>Friday 16-December 11am-12:30pm (Gross 351)</li>
    </ul>
</ul>

<p> To prepare for the exam, you should be able to:</p>

<ul>
    <li>Correctly articulate all definitions and theorems from lecture.</li>
    <li>Clearly and coherently solve all examples from lecture.</li>
    <li>Write correct and legible solutions to all problems from the midterm
        exams, <a href="./218f22-hw.pdf">problem sets</a>, and <a href="./218f22-ds.pdf">discussion worksheets</a>.</li>
    <li>Clearly explain how to solve each comprehension quiz problem.</li>
</ul>

<p> If you feel confident that you can accomplish everything described above and
    would like some additional practice, you can review the extra material
    suggested for studying for <a href="./exam01.php">Exam I</a>,
    <a href="./exam02.php">Exam II</a>, and <a href="./exam03.php">Exam III</a>.
    Additionally, here are some <code>optional</code> problems
    from Gilbert Strang's textbook. Solutions to these problems will not be
    provided, but I will happily discuss them in office hours.</p>

<ul>
    <li>Section 7.2 Exercises 2, 4, 5, 7, 15, 16, 17</li>
</ul>
