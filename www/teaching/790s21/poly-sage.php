<!doctype html>
<?php $TITLE='Math 218: Matrices and Vector Spaces'; ?>

<title><?php echo $TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="/css/style.css">

<?php include($_SERVER['DOCUMENT_ROOT'].'/menu.php') ?>
<?php include('./menu.php') ?>

<script src="https://sagecell.sagemath.org/static/embedded_sagecell.js"></script>
<script>
 // Make the div with id 'mycell' a Sage cell
 sagecell.makeSagecell({inputLocation:  '#mycell',
			template:       sagecell.templates.minimal,
			evalButtonText: 'Activate'});
 // Make *any* div with class 'compute' a Sage cell
 sagecell.makeSagecell({inputLocation: 'div.compute',
			evalButtonText: 'Evaluate'});
</script>

<h2>Polynomial Algebra</h2>
<p>If the roots of a polynomial are nice enough, then we can use sage to find
   them with the syntax <code>f.roots()</code>. This will give the roots of the
   polynomial along with their multiplicities.
<div class="compute">
<script type="text/x-sage">
var('t')
f = t**5 + 15*t**4 + 98*t**3 + 398*t**2 + 897*t + 1183

print(f'f(t) = {f}\n')
print(f'the roots of f(t) are:\n{f.roots()}')
</script>
</div>

<p>However, if the roots are ugly, then sage will simply give up.
<div class="compute">
<script type="text/x-sage">
var('t')
f = t**5 - 15*t**4 + 98*t**3 + 398*t**2 + 897*t + 1183

print(f'f(t) = {f}')
print(f'the roots of f(t) are:\n{f.roots()}')
</script>
</div>
