<h1>
  <?php echo $TITLE; ?>
  <a href="./" title="schedule"><img src="/pix/icons/list.svg"></a>
  <a href="./cal.php" title="calendar"><img src="/pix/icons/calendar.svg"></a>
  <a href="https://duke.zoom.us/j/97513959039?pwd=M0dUUTBWTGd2VWszY0J1bVlqMmlRUT09" title="zoom"><img src="/pix/icons/zoom.svg"></a>
  <a href="https://gradescope.com" title="gradescope"><img src="/pix/icons/gradescope.png"></a>
  <a href="https://math.duke.edu" title="policies"><img src="/pix/icons/scroll.svg"></a>
</h1>

<p> Welcome! You have found the homepage of the Spring 2021 manifestation of
    Math 790-92.
