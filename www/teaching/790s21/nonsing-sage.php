<!doctype html>
<?php $TITLE='Math 218: Matrices and Vector Spaces'; ?>

<title><?php echo $TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="/css/style.css">

<?php include($_SERVER['DOCUMENT_ROOT'].'/menu.php') ?>
<?php include('./menu.php') ?>

<script src="https://sagecell.sagemath.org/static/embedded_sagecell.js"></script>
<script>
 // Make the div with id 'mycell' a Sage cell
 sagecell.makeSagecell({inputLocation:  '#mycell',
			template:       sagecell.templates.minimal,
			evalButtonText: 'Activate'});
 // Make *any* div with class 'compute' a Sage cell
 sagecell.makeSagecell({inputLocation: 'div.compute',
			evalButtonText: 'Evaluate'});
</script>

<h2>Nonsingular Matrices</h2>
<p>Our algorithm for calculating matrix inverses called for augmentation by
   identity matrices. We can accomplish this with the
   syntax <code>A.augment(identity_matrix(A.nrows()), subdivide=True)</code>.
<div class="compute">
<script type="text/x-sage">
A = matrix([(1, 2, -1), (4, 1, -1), (1, 1, -1)])

print(f'[A|I]=\n{A.augment(identity_matrix(A.nrows()), subdivide=True)}\n')
print(f'rref[A|I]=\n{A.augment(identity_matrix(A.nrows()), subdivide=True).rref()}')
</script>
</div>

<p>Of course, sage is capable of calculating inverses directly. The appropriate
  syntax is <code>A.inverse()</code>.
<div class="compute">
<script type="text/x-sage">
A = matrix([(1, 2, -1), (4, 1, -1), (1, 1, -1)])

print(f'A=\n{A}\n')
print(f'A^-1=\n{A.inverse()}')
</script>
</div>
