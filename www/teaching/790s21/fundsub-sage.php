<!doctype html>
<?php $TITLE='Math 218: Matrices and Vector Spaces'; ?>

<title><?php echo $TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="/css/style.css">

<?php include($_SERVER['DOCUMENT_ROOT'].'/menu.php') ?>
<?php include('./menu.php') ?>

<script src="https://sagecell.sagemath.org/static/embedded_sagecell.js"></script>
<script>
 // Make the div with id 'mycell' a Sage cell
 sagecell.makeSagecell({inputLocation:  '#mycell',
			template:       sagecell.templates.minimal,
			evalButtonText: 'Activate'});
 // Make *any* div with class 'compute' a Sage cell
 sagecell.makeSagecell({inputLocation: 'div.compute',
			evalButtonText: 'Evaluate'});
</script>

<h2>The Four Fundamental Subspaces</h2>
<p>To verify if <code>v</code> is in the null space of a matrix <code>A</code>,
   we can use the syntax <code>A*v == zero_vector(A.nrows())</code>.
<div class="compute">
<script type="text/x-sage">
A = matrix([(0, -1, -1, 0), (1, -1, -2, -12), (-1, -1, -1, -3)])
v = vector([3, -15, 15, -1])

print(f'A = \n{A}\n')
print(f'v = {v}\n')
print(f'A*v = {A*v}\n')
print(f'v in Null(A)? {A*v == zero_vector(A.nrows())}')
</script>
</div>

<p>To verify if <code>v</code> is in the left null space of a
   matrix <code>A</code>, we can use the syntax <code>A.T*v ==
   zero_vector(A.ncols())</code>.
<div class="compute">
<script type="text/x-sage">
A = matrix([(0, -1, -1, 0), (1, -1, -2, -12), (-1, -1, -1, -3)])
v = vector([2, -6, 5])

print(f'A = \n{A}\n')
print(f'v = {v}\n')
print(f'A^T*v = {A.T*v}\n')
print(f'v in Null(A^T)? {A.T*v == zero_vector(A.ncols())}')
</script>
</div>

<p>To verify if <code>v</code> is in the column space of a matrix <code>A</code>,
   we can use the syntax <code>A.rank() == A.augment(v).rank()</code>.
<div class="compute">
<script type="text/x-sage">
A = matrix([(0, -1, -1, 0), (1, -1, -2, -12), (-1, -1, -1, -3)])
v = vector([4, 2, 5])

print(f'[A | v] =\n{A.augment(v, subdivide=True)}\n')
print(f'rref[A | v] =\n{A.augment(v, subdivide=True).rref()}\n')
print(f'v in Col(A)? {A.rank() == A.augment(v).rank()}')
</script>
</div>

<p>To verify if <code>v</code> is in the row space of a matrix <code>A</code>,
   we can use the syntax <code>A.T.rank() == A.T.augment(v).rank()</code>.
<div class="compute">
<script type="text/x-sage">
A = matrix([(0, -1, -1, 0), (1, -1, -2, -12), (-1, -1, -1, -3)])
v = vector([-45, -12, 17, -160])

print(f'[A^T | v] =\n{A.T.augment(v, subdivide=True)}\n')
print(f'rref[A^T | v] =\n{A.T.augment(v, subdivide=True).rref()}\n')
print(f'v in Col(A^T)? {A.T.rank() == A.T.augment(v).rank()}')
</script>
</div>
