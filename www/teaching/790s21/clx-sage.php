<!doctype html>
<?php $TITLE='Math 218: Matrices and Vector Spaces'; ?>

<title><?php echo $TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="/css/style.css">

<?php include($_SERVER['DOCUMENT_ROOT'].'/menu.php') ?>
<?php include('./menu.php') ?>

<script src="https://sagecell.sagemath.org/static/embedded_sagecell.js"></script>
<script>
 // Make the div with id 'mycell' a Sage cell
 sagecell.makeSagecell({inputLocation:  '#mycell',
			template:       sagecell.templates.minimal,
			evalButtonText: 'Activate'});
 // Make *any* div with class 'compute' a Sage cell
 sagecell.makeSagecell({inputLocation: 'div.compute',
			evalButtonText: 'Evaluate'});
</script>

<h2>Complex Numbers</h2>
<p>In sage, the variables <code>i</code> and <code>I</code> can be used
   interchangably as the "imaginary unit".
<div class="compute">
<script type="text/x-sage">
i**2
</script>
</div>

<p>The commands <code>z.real()</code> and <code>z.imag()</code> calculate the
   real and imaginary parts of <code>z</code>. The
   commands <code>z.conjugate()</code> and <code>z.abs()</code> calculate the
   conjugate and absolute value of <code>z</code>. We can calculate the
   recriprocal of <code>z</code> with <code>1/z</code>.
<div class="compute">
<script type="text/x-sage">
z = 7-15*i

print(f'z = {z}')
print(f'Re(z) = {z.real()}')
print(f'Im(z) = {z.imag()}')
print(f'z^- = {z.conjugate()}')
print(f'|z| = {z.abs()}')
print(f'1/z = {1/z}')
</script>
</div>

<p>Of course, we can add and multiply complex numbers with the
   syntax <code>z1+z2</code> and <code>z1*z2</code>.
<div class="compute">
<script type="text/x-sage">
z1 = 7-15*i
z2 = 13-4*i

print(f'z1 = {z1}')
print(f'z2 = {z2}')
print(f'z1+z2 = {z1+z2}')
print(f'z1*z2 = {z1*z2}')
</script>
</div>

<p>We can use the expected syntax to define a complex matrix <code>A</code> and
   a complex vector <code>v</code>. The syntax <code>A*v</code> calculates the
   associated matrix-vector product. The conjugate-transpose of <code>A</code>
   is given by <code>A.H</code>.
<div class="compute">
<script type="text/x-sage">
A = matrix([(3+2*i, 5+7*i, 11-2*i), (4, 0, -8+11*i)])
v = vector([-i, 11+2*i, 19-i])

print(f'A =\n{A}\n')
print(f'A^* =\n{A.H}\n')
print(f'v = {v}\n')
print(f'A*v = {A*v}')
</script>
</div>

<p>The inner product of two complex vectors is given
   by <code>v1.hermitian_inner_product(v2)</code>.
<div class="compute">
<script type="text/x-sage">
v1 = vector([-i, 11+2*i, 19-i])
v2 = vector([-9, 6-4*i, 5*i])

print(f'v1 = {v1}')
print(f'v2 = {v2}')
print(f'<v1, v2> = {v1.hermitian_inner_product(v2)}')
</script>
</div>
