<!doctype html>
<?php $TITLE='Math 218: Matrices and Vector Spaces'; ?>

<title><?php echo $TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="/css/style.css">

<?php include($_SERVER['DOCUMENT_ROOT'].'/menu.php') ?>
<?php include('./menu.php') ?>

<script src="https://sagecell.sagemath.org/static/embedded_sagecell.js"></script>
<script>
 // Make the div with id 'mycell' a Sage cell
 sagecell.makeSagecell({inputLocation:  '#mycell',
			template:       sagecell.templates.minimal,
			evalButtonText: 'Activate'});
 // Make *any* div with class 'compute' a Sage cell
 sagecell.makeSagecell({inputLocation: 'div.compute',
			evalButtonText: 'Evaluate'});
</script>

<h2>Determinants III</h2>
<p>The code below will visualize the parallelogram formed by two
   vectors <code>v</code> and <code>w</code>.
<div class="compute">
<script type="text/x-sage">
v = vector([3, -2])
w = vector([2, 5])

A = matrix.column([v, w])
print(f'P is the parallelogram spanned by {v} and {w}.')
print(f'area(P) = {abs(A.det())}')
polygon([0*v, v, v+w, w], color='purple')+plot(v, color='blue')+plot(w, color='red')+plot(v+w, color='green')
</script>
</div>
