<!doctype html>
<?php $TITLE='Math 218: Matrices and Vector Spaces'; ?>

<title><?php echo $TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="/css/style.css">

<?php include($_SERVER['DOCUMENT_ROOT'].'/menu.php') ?>
<?php include('./menu.php') ?>

<script src="https://sagecell.sagemath.org/static/embedded_sagecell.js"></script>
<script>
 // Make the div with id 'mycell' a Sage cell
 sagecell.makeSagecell({inputLocation:  '#mycell',
			template:       sagecell.templates.minimal,
			evalButtonText: 'Activate'});
 // Make *any* div with class 'compute' a Sage cell
 sagecell.makeSagecell({inputLocation: 'div.compute',
			evalButtonText: 'Evaluate'});
</script>

<h2>Row Echelon Forms</h2>
<p>The command <code>A == A.rref()</code> will determine if a given matrix is in
   reduced row echelon form.
<div class="compute">
<script type="text/x-sage">
A = matrix([(1, -4, 0, 0, 1), (0, 0, 1, 0, 2), (0, 0, 0, 1, 7), (0, 0, 0, 0, 0)])

print(f'A = \n{A}\n')
print(f'{A == A.rref()}')
</script>
</div>

<p>The commands <code>A.rank()</code> and <code>A.right_nullity()</code>
   calculate the rank and nullity.

<div class="compute">
<script type="text/x-sage">
A = matrix([(1, -4, 0, 0, 1), (0, 0, 1, 0, 2), (0, 0, 0, 1, 7), (0, 0, 0, 0, 0)])

print(f'A = \n{A}\n')
print(f'rank(A) = {A.rank()}')
print(f'nullity(A) = {A.right_nullity()}')
</script>
</div>

<p>The code below will verify whether or not a matrix is in row echelon form.

<div class="compute">
<script type="text/x-sage">
A = matrix([[2, 7, -11, 6, 9], [0, 3, 14, 2, 5], [0, 0, 0, -4, 6], [0, 0, 0, 0, 0]])

def in_row_echelon_form(A):
    l = list(map(lambda r: r.is_zero(), A))
    if l != sorted(l):
	    return False
    for j, r in zip(A.pivots(), A):
	    if j != next((i for i, a in enumerate(r) if a), None):
		    return False
    return True

print(f'A=\n{A}\n')
print(f'Is A in row echelon form? {in_row_echelon_form(A)}')
</script>
</div>
