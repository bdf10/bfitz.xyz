<!doctype html>
<?php $TITLE='Math 218: Matrices and Vector Spaces'; ?>

<title><?php echo $TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="/css/style.css">

<?php include($_SERVER['DOCUMENT_ROOT'].'/menu.php') ?>
<?php include('./menu.php') ?>

<script src="https://sagecell.sagemath.org/static/embedded_sagecell.js"></script>
<script>
 // Make the div with id 'mycell' a Sage cell
 sagecell.makeSagecell({inputLocation:  '#mycell',
			template:       sagecell.templates.minimal,
			evalButtonText: 'Activate'});
 // Make *any* div with class 'compute' a Sage cell
 sagecell.makeSagecell({inputLocation: 'div.compute',
			evalButtonText: 'Evaluate'});
</script>

<h2>Linear Independence</h2>
<p>To determine if a list of vectors is linearly independent, we can put them
   into the columns of a matrix and check that the nullity is zero. We can
   accomplish this with the syntax <code>matrix.column([list of
   vectors]).right_nullity() == 0</code>.
<div class="compute">
<script type="text/x-sage">
vectors = [(-7, 4, 4, -5), (34, -15, -19, 25), (-200, 91, 112, -141), (454, -204, -254, 322)]

A = matrix.column(vectors)
print(f'Vectors are the columns of A=\n{A}\n')
print(f'rref(A)=\n{A.rref()}\n')
print(f'Is the list linearly independent? {A.right_nullity() == 0}')
</script>
</div>
