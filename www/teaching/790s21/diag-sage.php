<!doctype html>
<?php $TITLE='Math 218: Matrices and Vector Spaces'; ?>

<title><?php echo $TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="/css/style.css">

<?php include($_SERVER['DOCUMENT_ROOT'].'/menu.php') ?>
<?php include('./menu.php') ?>

<script src="https://sagecell.sagemath.org/static/embedded_sagecell.js"></script>
<script>
 // Make the div with id 'mycell' a Sage cell
 sagecell.makeSagecell({inputLocation:  '#mycell',
			template:       sagecell.templates.minimal,
			evalButtonText: 'Activate'});
 // Make *any* div with class 'compute' a Sage cell
 sagecell.makeSagecell({inputLocation: 'div.compute',
			evalButtonText: 'Evaluate'});
</script>

<h2>Diagonalization</h2>
<p> The following code will attempt to diagonalize an input matrix <code>A</code>.
<div class="compute">
<script type="text/x-sage">
A = matrix([[41, 42, -28, -4, -100], [-37, -37, 28, 1, 102], [17, 14, 47, 65, 162], [-13, -14, 0, -6, -2], [-1, 0, -14, -17, -49]])

D, X = A.jordan_form(transformation=True)
print(f'A =\n{A}\n')
if not D.is_diagonal(): raise TypeError('Matrix is not diagonalizable!')
print(f'X =\n{X}\n')
print(f'D =\n{D}\n')
print(f'X^-1 =\n{X.inverse()}')
</script>
</div>
