<!doctype html>
<?php $TITLE='Math 218: Matrices and Vector Spaces'; ?>

<title><?php echo $TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="/css/style.css">

<?php include($_SERVER['DOCUMENT_ROOT'].'/menu.php') ?>
<?php include('./menu.php') ?>

<script src="https://sagecell.sagemath.org/static/embedded_sagecell.js"></script>
<script>
 // Make the div with id 'mycell' a Sage cell
 sagecell.makeSagecell({inputLocation:  '#mycell',
			template:       sagecell.templates.minimal,
			evalButtonText: 'Activate'});
 // Make *any* div with class 'compute' a Sage cell
 sagecell.makeSagecell({inputLocation: 'div.compute',
			evalButtonText: 'Evaluate'});
</script>

<h2>Orthogonality</h2>
<p>The following code with produce a basis of the orthogonal complement of a
   vector space.
<div class="compute">
<script type="text/x-sage">
basis_of_V = [(-2, 6, -1, 0), (-3, 9, -2, -1)]

A = matrix.column(basis_of_V)

print(f'V is spanned by the columns of:\n{A}\n')
print(f'V^perp is spanned by the columns of:\n{A.left_kernel().basis_matrix().T}')
</script>
</div>
