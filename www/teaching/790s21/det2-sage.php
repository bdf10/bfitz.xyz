<!doctype html>
<?php $TITLE='Math 218: Matrices and Vector Spaces'; ?>

<title><?php echo $TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="/css/style.css">

<?php include($_SERVER['DOCUMENT_ROOT'].'/menu.php') ?>
<?php include('./menu.php') ?>

<script src="https://sagecell.sagemath.org/static/embedded_sagecell.js"></script>
<script>
 // Make the div with id 'mycell' a Sage cell
 sagecell.makeSagecell({inputLocation:  '#mycell',
			template:       sagecell.templates.minimal,
			evalButtonText: 'Activate'});
 // Make *any* div with class 'compute' a Sage cell
 sagecell.makeSagecell({inputLocation: 'div.compute',
			evalButtonText: 'Evaluate'});
</script>

<h2>Determinants II</h2>
<p>We can calculate the adjugate of a matrix with the
   syntax <code>A.adjugate()</code>. The cofactor matrix is then given
   by <code>A.adjugate().T</code>.
<div class="compute">
<script type="text/x-sage">
A = matrix([(3, -7, 9, 4), (12, 14, -20, 5), (22, -22, 14, 22), (-11, 22, 15, 5)])

print(f'A=\n{A}\n')
print(f'C=\n{A.adjugate().T}\n')
print(f'adj(A)=\n{A.adjugate()}\n')
</script>
</div>
