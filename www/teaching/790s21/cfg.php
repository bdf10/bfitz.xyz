<?php
return [
    'h1' => 'Math 790-92: Topics in Foundational Mathematics',
    'icons' => [
        ['./', 'schedule', 'list.svg'],
        ['./cal.php', 'calendar', 'calendar.svg'],
        ['https://duke.zoom.us', 'zoom', 'zoom.svg'],
        ['https://gradescope.com', 'gradescope', 'gradescope.png'],
    ],
    'msg' => '<p>Welcome! You have found the homepage of the Spring 2021 manifestation of Math 790-92.</p>'
];
?>
