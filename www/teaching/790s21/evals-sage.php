<!doctype html>
<?php $TITLE='Math 218: Matrices and Vector Spaces'; ?>

<title><?php echo $TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="/css/style.css">

<?php include($_SERVER['DOCUMENT_ROOT'].'/menu.php') ?>
<?php include('./menu.php') ?>

<script src="https://sagecell.sagemath.org/static/embedded_sagecell.js"></script>
<script>
 // Make the div with id 'mycell' a Sage cell
 sagecell.makeSagecell({inputLocation:  '#mycell',
			template:       sagecell.templates.minimal,
			evalButtonText: 'Activate'});
 // Make *any* div with class 'compute' a Sage cell
 sagecell.makeSagecell({inputLocation: 'div.compute',
			evalButtonText: 'Evaluate'});
</script>

<h2>Eigenvalues</h2>
<p>We can check if <code>l</code> is an eigenvalue of <code>A</code> by
   studying <code>(l*identity_matrix(A.nrows())-A).rref()</code>.
<div class="compute">
<script type="text/x-sage">
A = matrix([(-41, 200, -150), (-15, 69, -45), (-5, 20, -6)])
l = 9

print(f'A=\n{A}\n')
print(f'rref({l}I-A)=\n{(l*identity_matrix(A.nrows())-A).rref()}')
</script>
</div>

<p>More efficiently, we can directly check if <code>l</code> is an eigenvalue
   of <code>A</code>
   with <code>(l*identity_matrix(A.nrows())-A).is_singular()</code>.
<div class="compute">
<script type="text/x-sage">
A = matrix([(-41, 200, -150), (-15, 69, -45), (-5, 20, -6)])
l = 9

print(f'A=\n{A}\n')
print(f'Is l={l} an eigenvalue? {(l*identity_matrix(A.nrows())-A).is_singular()}')
</script>
</div>

<p>The command <code>(l*identity_matrix(A.nrows())-A).right_nullity()</code>
   calculates the geometric multiplicity of an eigenvalue.
<div class="compute">
<script type="text/x-sage">
A = matrix([(-41, 200, -150), (-15, 69, -45), (-5, 20, -6)])
l = 9

print(f'A=\n{A}\n')
print(f'gm_A({l})={(l*identity_matrix(A.nrows())-A).right_nullity()}')
</script>
</div>
