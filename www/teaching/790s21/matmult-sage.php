<!doctype html>
<?php $TITLE='Math 218: Matrices and Vector Spaces'; ?>

<title><?php echo $TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="/css/style.css">

<?php include($_SERVER['DOCUMENT_ROOT'].'/menu.php') ?>
<?php include('./menu.php') ?>

<script src="https://sagecell.sagemath.org/static/embedded_sagecell.js"></script>
<script>
 // Make the div with id 'mycell' a Sage cell
 sagecell.makeSagecell({inputLocation:  '#mycell',
			template:       sagecell.templates.minimal,
			evalButtonText: 'Activate'});
 // Make *any* div with class 'compute' a Sage cell
 sagecell.makeSagecell({inputLocation: 'div.compute',
			evalButtonText: 'Evaluate'});
</script>

<h2>Matrix Multiplication</h2>
<p>The syntax for matrix multiplication is <code>A * B</code>.
<div class="compute">
<script type="text/x-sage">
A = matrix([(1, -2, 3, 8), (4, -9, 15, 3)])
B = matrix([(1, 3, 2), (-9, 8, 7), (-4, 1, 0), (0, 2, 12)])

print(f'A=\n{A}\n')
print(f'B=\n{B}\n')
print(f'AB=\n{A*B}\n')
</script>
</div>
