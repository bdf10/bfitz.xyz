<!doctype html>
<?php $TITLE='Math 218: Matrices and Vector Spaces'; ?>

<title><?php echo $TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="/css/style.css">

<?php include($_SERVER['DOCUMENT_ROOT'].'/menu.php') ?>
<?php include('./menu.php') ?>

<script src="https://sagecell.sagemath.org/static/embedded_sagecell.js"></script>
<script>
 // Make the div with id 'mycell' a Sage cell
 sagecell.makeSagecell({inputLocation:  '#mycell',
			template:       sagecell.templates.minimal,
			evalButtonText: 'Activate'});
 // Make *any* div with class 'compute' a Sage cell
 sagecell.makeSagecell({inputLocation: 'div.compute',
			evalButtonText: 'Evaluate'});
</script>

<h2>Definiteness</h2>
<p>Sage is capable of constructing the quadratic form defined by any
   real-symmetric matrix <code>S</code>.
<div class="compute">
<script type="text/x-sage">
S = matrix([(-6, -5, 1), (-5, -10, -3), (1, -3, 2)])

q = QuadraticForm(2*S).polynomial(names=[f'x{i}' for i in range(1, S.nrows()+1)])
print(f'S =\n{S}\n')
print(f'q(x) = <x, Sx> = {q}')
</script>
</div>

<p>The following code will calculate the real-symmetric matrix representing a
   defined quadratic form.
<div class="compute">
<script type="text/x-sage">
var('x1 x2 x3')
q = 3*x1**2 - 7*x1*x2 + 11*x2**2 - 4*x1*x3 + 2*x2*x3 - 19*x3**2

S = q.hessian() / 2
print(f'q(x) = <x, Sx> = {q}\n')
print(f'S =\n{S}')
</script>
</div>
