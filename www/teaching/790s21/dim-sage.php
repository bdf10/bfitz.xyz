<!doctype html>
<?php $TITLE='Math 218: Matrices and Vector Spaces'; ?>

<title><?php echo $TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="/css/style.css">

<?php include($_SERVER['DOCUMENT_ROOT'].'/menu.php') ?>
<?php include('./menu.php') ?>

<script src="https://sagecell.sagemath.org/static/embedded_sagecell.js"></script>
<script>
  // Make the div with id 'mycell' a Sage cell
  sagecell.makeSagecell({inputLocation:  '#mycell',
  template:       sagecell.templates.minimal,
  evalButtonText: 'Activate'});
  // Make *any* div with class 'compute' a Sage cell
  sagecell.makeSagecell({inputLocation: 'div.compute',
  evalButtonText: 'Evaluate'});
</script>

<h2>Dimension</h2>
<p>There is now even more wisdom in the following image.

<div style="text-align: center">
<img src="dim-sage.png" alt="dim" class="center" width="100%">
</div>

<p>The following code will take a matrix <code>A</code> and calculate all of the
   bases and give the dimensions of its four fundamental subspaces referenced
   above.

<div class="compute">
<script type="text/x-sage">
A = matrix([(1, -2, 3, 8), (4, -9, 15, 3)])

print(f'A=\n{A}\n')
print(f'rref(A)=\n{A.rref()}\n')
print(f'rref(A^T)=\n{A.T.rref()}\n')

print(f'Col(A)')
print(f'------')
print(f'pivot columns of A: {[v for i, v in enumerate(A.T) if i in A.pivots()]}')
print(f'nonzero rows of rref(A^T): {list(filter(bool, A.T.rref()))}')
print(f'dim Col(A): {A.rank()}\n')

print(f'Col(A^T)')
print(f'--------')
print(f'pivot columns of A^T: {[v for i, v in enumerate(A) if i in A.T.pivots()]}')
print(f'nonzero rows of rref(A): {list(filter(bool, A.rref()))}')
print(f'dim Col(A^T): {A.rank()}\n')

print(f'Null(A)')
print(f'-------')
print(f"pivot solutions to Av=0: {list(A.change_ring(QQ).right_kernel(basis='pivot').basis())}")
print(f'dim Null(A): {A.right_nullity()}\n')

print(f'Null(A^T)')
print(f'---------')
print(f"pivot solutions to A^Tv=0: {list(A.T.change_ring(QQ).right_kernel(basis='pivot').basis())}")
print(f'dim Null(A^T): {A.left_nullity()}')
</script>
</div>
