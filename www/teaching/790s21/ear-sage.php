<!doctype html>
<?php $TITLE='Math 218: Matrices and Vector Spaces'; ?>

<title><?php echo $TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="/css/style.css">

<?php include($_SERVER['DOCUMENT_ROOT'].'/menu.php') ?>
<?php include('./menu.php') ?>

<script src="https://sagecell.sagemath.org/static/embedded_sagecell.js"></script>
<script>
 // Make the div with id 'mycell' a Sage cell
 sagecell.makeSagecell({inputLocation:  '#mycell',
			template:       sagecell.templates.minimal,
			evalButtonText: 'Activate'});
 // Make *any* div with class 'compute' a Sage cell
 sagecell.makeSagecell({inputLocation: 'div.compute',
			evalButtonText: 'Evaluate'});
</script>

<h2>EA=R Factorizations</h2>
<p>Sage has a built-in syntax for defining elementary matrices.


<p>To define the elementary matrix of size n corresponding to the
   row-swap <code>ri <-> rj</code>, we use the syntax <code>elementary_matrix(n,
   row1=i-1, row2=j-1)</code>. Note that we need to subtract one from the row
   indices because in python indices start at zero.

<p>In this example, we define the 10x10 elementary matrix corresponding to
   swapping row 4 with row 8.
<div class="compute">
<script type="text/x-sage">
E = elementary_matrix(10, row1=3, row2=7)

print(E)
</script>
</div>

<p>To define the elementary matrix of size n corresponding to the row
   scaling <code>c*ri -> ri</code>, we use the syntax <code>elementary_matrix(n,
   row1=i-1, scale=c)</code>. Again, it is important that we subtract one from
   the row index because python counts from zero, not one.

<p>In this example, we define the 6x6 elementary matrix corresponding to scaline
   row 5 by -33.

<div class="compute">
<script type="text/x-sage">
E = elementary_matrix(6, row1=4, scale=-33)

print(E)
</script>
</div>

<p>To define the elementary matrix of size n corresponding to the row
   addition <code>ri + c*rj -> ri</code>, we use the
   syntax <code>elementary_matrix(n, row1=i-1, row2=j-1, scale=c)</code>. Again,
   we need to subtract one from each row index because counting in python starts
   at zero, not one.

<p>In this example, we define the 4x4 elementary matrix corresponding to scaline
   <code>r3 - 8*r1 -> r3</code>.

<div class="compute">
<script type="text/x-sage">
E = elementary_matrix(4, row1=2, row2=0, scale=-8)

print(E)
</script>
</div>
