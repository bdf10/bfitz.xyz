<!doctype html>
<?php $TITLE='Math 218: Matrices and Vector Spaces'; ?>

<title><?php echo $TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="/css/style.css">

<?php include($_SERVER['DOCUMENT_ROOT'].'/menu.php') ?>
<?php include('./menu.php') ?>

<script src="https://sagecell.sagemath.org/static/embedded_sagecell.js"></script>
<script>
 // Make the div with id 'mycell' a Sage cell
 sagecell.makeSagecell({inputLocation:  '#mycell',
			template:       sagecell.templates.minimal,
			evalButtonText: 'Activate'});
 // Make *any* div with class 'compute' a Sage cell
 sagecell.makeSagecell({inputLocation: 'div.compute',
			evalButtonText: 'Evaluate'});
</script>

<h2>A=QR Factorizations</h2>
<p>We can check if a list of vectors is orthonormal but putting them into the
   columns of a matrix <code>Q</code> and checking if <code>Q.T*Q ==
   identity_matrix(Q.ncols())</code>
<div class="compute">
<script type="text/x-sage">
list_of_vectors = [(-2/3, 2/3, 1/3, 0), (-4/15, 1/5, -14/15, -2/15), (-1/5, -4/15, 2/15, -14/15)]

Q = matrix.column(list_of_vectors)

print(f'Vectors are the columns of:\n{Q}\n')
print(f'Q^T * Q =\n{Q.T*Q}\n')
print(f'Are the vectors orthonormal? {Q.T*Q == identity_matrix(Q.ncols())}')
</script>
</div>

<p>When <code>A=QR</code>, projection onto the column space of <code>A</code> is
   simplified with <code>P=Q*Q.T</code>.
<div class="compute">
<script type="text/x-sage">
Q = matrix([(-2/3, -4/15, -1/5), (2/3, 1/5, -4/15), (1/3, -14/15, 2/15), (0, -2/15, -14/15)])
R = matrix([(3, -3, -2), (0, 15, -7), (0, 0, 1)])

if Q.T*Q != identity_matrix(Q.ncols()): raise TypeError('Q must have orthonormal columns')
if any(map(lambda t: t[0] > t[1], R.dict())): raise TypeError('R must be upper-triangular')
A = Q*R
print(f'A=\n{A}\n')
print(f'Q =\n{Q}\n')
print(f'R =\n{R}\n')
print(f'P_Col(A) = Q*Q.T =\n{Q*Q.T}')
</script>
</div>

<p>When <code>A=QR</code>, the least-squares problem <code>[A.T*A |
   A.T*b]</code> reduces to <code>[R | Q.T*b]</code>.
<div class="compute">
<script type="text/x-sage">
Q = matrix([(-2/3, -4/15, -1/5), (2/3, 1/5, -4/15), (1/3, -14/15, 2/15), (0, -2/15, -14/15)])
R = matrix([(3, -3, -2), (0, 15, -7), (0, 0, 1)])
b = vector([3, 1, 7, -1])

if Q.T*Q != identity_matrix(Q.ncols()): raise TypeError('Q must have orthonormal columns')
if any(map(lambda t: t[0] > t[1], R.dict())): raise TypeError('R must be upper-triangular')
A = Q*R
print(f'A=\n{A}\n')
print(f'Q =\n{Q}\n')
print(f'R =\n{R}\n')
print(f'Original least squares system.\n------------------------------\n[A.T*A | A.T*b]=\n{(A.T*A).augment(A.T*b, subdivide=True)}\n')
print(f'Reduced least squares system.\n-----------------------------\n[R | Q.T*b]=\n{R.augment(Q.T*b, subdivide=True)}\n')
print(f'rref[R | Q.T*b]=\n{R.augment(Q.T*b, subdivide=True).rref()}')
</script>
</div>
