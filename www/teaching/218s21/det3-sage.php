<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Determinants III</h2>
<p>The code below will visualize the parallelogram formed by two
   vectors <code>v</code> and <code>w</code>.
<div class="compute">
<script type="text/x-sage">
v = vector([3, -2])
w = vector([2, 5])

A = matrix.column([v, w])
print(f'P is the parallelogram spanned by {v} and {w}.')
print(f'area(P) = {abs(A.det())}')
polygon([0*v, v, v+w, w], color='purple')+plot(v, color='blue')+plot(w, color='red')+plot(v+w, color='green')
</script>
</div>
