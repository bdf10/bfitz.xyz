<?php
return [
    'h1' => 'Math 218: Matrices and Vector Spaces',
    'icons' => [
        ['./', 'schedule', 'list.svg'],
        ['./cal.php', 'calendar', 'calendar.svg'],
        ['https://duke.zoom.us', 'zoom', 'zoom.svg'],
        ['https://gradescope.com', 'gradescope', 'gradescope.png'],
        ['https://gitlab.oit.duke.edu/bdf10/course-policies/-/raw/master/218s21/218s21-policies.pdf?inline=true', 'policies', 'scroll.svg'],
    ],
    'msg' => '<p>Welcome! You have found the homepage of the Spring 2021 manifestation of Math 218.</p>'
];
?>
