<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>PA=LU Factorizations</h2>
<p>In sage, the syntax <code>P, L, U = A.LU(pivot='nonzero')</code> gives the
   matrix equation <code>A=PLU</code> where <code>P</code> is a permutation
   matrix, <code>L</code> is lower-triangular, and <code>U</code> is
   upper-triangular. We can redefine <code>P</code> as its
   transpose <code>P=P.T</code> to obtain <code>PA=LU</code>.

<p>In class, we require <code>U</code> be in row echelon form, not just upper
   triangular. So, we may need to do a few more reductions ourselves.

<div class="compute">
<script type="text/x-sage">
A = matrix([(2, -7, 8, -9, -17), (-6, 21, -24, 27, 59), (10, -24, 5, -29, -34), (12, 2, -92, 15, 87)])

P, L, U = A.LU(pivot='nonzero')
P = P.T

print(f'A=\n{A}\n')
print(f'P=\n{P}\n')
print(f'L=\n{L}\n')
print(f'U=\n{U}')
</script>
</div>
