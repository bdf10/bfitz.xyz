<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Matrices and Vectors</h2>
<p>Sage is an open source computer algebra system written python.</p>

<p>Matrices are defined as lists of rows via the syntax <code>A = matrix([(1, 2,
    3), (4, 5, 6)])</code>.</p>

<?php
sagecell(<<<'EOF'
A = matrix([(1, 2, 3), (4, 5, 6)])

print(f'A=\n{A}')
EOF)
?>

<p>Matrices can be summed <code>A + B</code>, scaled <code>c * A</code>, and
    transposed <code>A.T</code>.</p>

<?php
sagecell(<<<'EOF'
A = matrix([(1, 2, 3), (4, 5, 6)])
B = matrix([(3, -7, 4), (5, 9, 6)])

print(f'A+B=\n{A+B}\n')
print(f'2*A=\n{2*A}\n')
print(f'A^T\n{A.T}')
EOF)
?>

<p>Properties can be calculated with the
    syntax <code>A.property_name()</code>. A full list of properties can be
    found in the <a href="https://doc.sagemath.org/html/en/constructions/linear_algebra.html">official
    documentation</a>.</p>

<?php
sagecell(<<<'EOF'
A = matrix([(1, 2, 3), (4, 5, 6), (7, 8, 9)])

print(f'number of rows of A: {A.nrows()}\n')
print(f'number of cols of A: {A.ncols()}\n')
print(f'diagonal of A:\n{A.diagonal()}\n')
print(f'trace(A)={A.trace()}\n')
print(f'is A symmetric?\n{A.is_symmetric()}\n')
EOF)
?>

<p>Sage also has useful matrix constructors built in,
    like <code>zero_matrix</code>, <code>ones_matrix</code>, <code>identity_matrix</code>,
    and <code>diagonal_matrix</code>.</p>

<div class="compute">
<script type="text/x-sage">
Z = zero_matrix(3, 4)
A = ones_matrix(6, 3)
I = identity_matrix(9)
D = diagonal_matrix([-7, 9, 5])

print(f'3x4 zero matrix:\n{Z}\n')
print(f'6x3 ones matrix:\n{A}\n')
print(f'9x9 identity matrix:\n{I}\n')
print(f'D=\n{D}')
</script>
</div>

<p>Vectors are defined with the syntax <code>vector([coordinate list])</code>.</p>

<div class="compute">
<script type="text/x-sage">
v = vector([1, 2, 3, 4])

print(f'v = {v}')
</script>
</div>

<p>We can calculate linear combinations by summing <code>v1 + v2</code> and
    scaling <code>c * v</code> vectors.</p>

<div class="compute">
<script type="text/x-sage">
v1 = vector([-1, 1, 2, 2])
v2 = vector([2, -2, 0, -1])
v3 = vector([1, 1, 1, -1])

c1 = 9
c2 = -3
c3 = 6

print(f'c1*v1 + c2*v2 + c3*v3 = {c1*v1+c2*v2+c3*v3}')
</script>
</div>

<p>Of course, the previous linear combination can be more elegantly calculated
    with a matrix-vector product <code>A * v</code>.</p>

<div class="compute">
<script type="text/x-sage">
A = matrix([(-1, 2, 1), (1, -2, 1), (2, 0, 1), (2, -1, -1)])
v = vector([9, -3, 6])

print(f'A * v = {A * v}')
</script>
</div>
