<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead() ?>

<p> This is a modified version of the standard Math 212 schedule designed for
    the pilot program for first-year engineering students in Spring 2020. These
    students successfully completed Math 218 "Matrices and Vector Spaces" in
    Fall 2019 and thus had acheived a degree of mathematical maturity not held
    by a typical Math 212 student.

    <p> Vector fields are introduced on the first day of class and woven into the
        schedule rather than introduced concurrently with the vector calculus
        theorems at the end of the course. The language of matrices is used
        throughout the course. For example, we approach the topic of unconstrained
        optimization by discussing the definiteness of the quadratic form
        corresponding to the Hessian matrix of the objective function. This language
        is traditionally unavailable to students who have not had a course in linear
        algebra that covers the definiteness of Hermitian matrices.

        <p> We used Susan Colley's "Vector Calculus", which is beautifully written and
            embraces the language of matrices throughout the whole text.

            <h2>Lessons</h2>

            <p>The source code for the lecture slides below is publicly hosted on my
                <a href="https://gitlab.oit.duke.edu/bdf10/212-lectures/">GitLab page</a>.

                <table>
                    <tr>
                        <th style="text-align: left">Topic</th>
                        <th style="text-align: left">Materials</th>
                    </tr>

                    <tr>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/vector-fields/vector-fields.pdf?inline=true">Vector Fields</a> <- click to access the lecture!</td>
			<td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/vector-fields/vector-fields-hw.pdf?inline=true"><img src="/pix/icons/memo.svg"></a></td>
                    </tr>

                    <tr>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/cross-products/cross-products.pdf?inline=true">Orientations and Cross Products</a></td>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/cross-products/cross-products-hw.pdf?inline=true"><img src="/pix/icons/memo.svg"></a></td>
                    </tr>

                    <tr>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/lines-planes/lines-planes.pdf?inline=true">Lines and Planes</a></td>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/lines-planes/lines-planes-hw.pdf?inline=true"><img src="/pix/icons/memo.svg"></a></td>
                    </tr>

                    <tr>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/quadric/quadric.pdf?inline=true">Quadric Surfaces</a></td>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/quadric/quadric-hw.pdf?inline=true"><img src="/pix/icons/memo.svg"></a></td>
                    </tr>

                    <tr>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/coords/coords.pdf?inline=true">Coordinate Systems</a></td>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/coords/coords-hw.pdf?inline=true"><img src="/pix/icons/memo.svg"></a></td>
                    </tr>

                    <tr>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/jacobian/jacobian.pdf?inline=true">The Derivative</a></td>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/jacobian/jacobian-hw.pdf?inline=true"><img src="/pix/icons/memo.svg"></a></td>
                    </tr>

                    <tr>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/grad/grad.pdf?inline=true">The Gradient</a></td>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/grad/grad-hw.pdf?inline=true"><img src="/pix/icons/memo.svg"></a></td>
                    </tr>

                    <tr>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/chain-rule/chain-rule.pdf?inline=true">The Chain Rule</a></td>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/chain-rule/chain-rule-hw.pdf?inline=true"><img src="/pix/icons/memo.svg"></a></td>
                    </tr>

                    <tr>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/curves/curves.pdf?inline=true">Parameterized Curves</a></td>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/curves/curves-hw.pdf?inline=true"><img src="/pix/icons/memo.svg"></a></td>
                    </tr>

                    <tr>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/hessian/hessian.pdf?inline=true">The Hessian</a></td>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/hessian/hessian-hw.pdf?inline=true"><img src="/pix/icons/memo.svg"></a></td>
                    </tr>

                    <tr>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/line-integrals/line-integrals.pdf?inline=true">Line Integrals</a></td>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/line-integrals/line-integrals-hw.pdf?inline=true"><img src="/pix/icons/memo.svg"></a></td>
                    </tr>

                    <tr>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/grad-curl-div/grad-curl-div.pdf?inline=true">grad, curl, and div</a></td>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/grad-curl-div/grad-curl-div-hw.pdf?inline=true"><img src="/pix/icons/memo.svg"></a></td>
                    </tr>

                    <tr>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/double-integrals/double-integrals.pdf?inline=true">Double Integrals</a></td>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/double-integrals/double-integrals-hw.pdf?inline=true"><img src="/pix/icons/memo.svg"></a></td>
                    </tr>

                    <tr>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/triple-integrals/triple-integrals.pdf?inline=true">Triple Integrals</a></td>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/triple-integrals/triple-integrals-hw.pdf?inline=true"><img src="/pix/icons/memo.svg"></a></td>
                    </tr>

                    <tr>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/applications/applications.pdf?inline=true">Applications of Integration</a></td>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/applications/applications-hw.pdf?inline=true"><img src="/pix/icons/memo.svg"></a></td>
                    </tr>

                    <tr>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/change-variables/change-variables.pdf?inline=true">Change of Variables</a></td>
                        <td>
	                    <a href="https://youtube.com/playlist?list=PLwK75AcBxZCCDguoHA-ojvKq_Ph8IqX0j"><img src="/pix/icons/youtube.png"></a>
	                    <a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/change-variables/change-variables-hw.pdf?inline=true"><img src="/pix/icons/memo.svg"></a>
                        </td>
                    </tr>

                    <tr>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/surfaces/surfaces.pdf?inline=true">Parameterized Surfaces</a></td>
                        <td>
	                    <a href="https://youtube.com/playlist?list=PLwK75AcBxZCDW_yCFg7U4cyQGavrijQqH"><img src="/pix/icons/youtube.png"></a>
	                    <a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/surfaces/surfaces-hw.pdf?inline=true"><img src="/pix/icons/memo.svg"></a>
                        </td>
                    </tr>

                    <tr>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/surface-integrals/surface-integrals.pdf?inline=true">Surface Integrals</a></td>
                        <td>
	                    <a href="https://youtube.com/playlist?list=PLwK75AcBxZCCrWftxhPbi_xdxR1JjThRJ"><img src="/pix/icons/youtube.png"></a>
	                    <a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/surface-integrals/surface-integrals-hw.pdf?inline=true"><img src="/pix/icons/memo.svg"></a>
                        </td>
                    </tr>

                    <tr>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/green/green.pdf?inline=true">Green's Theorem</a></td>
                        <td>
	                    <a href="https://youtube.com/playlist?list=PLwK75AcBxZCDnjk-FBxryjRv72vbYUvWJ"><img src="/pix/icons/youtube.png"></a>
	                    <a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/green/green-hw.pdf?inline=true"><img src="/pix/icons/memo.svg"></a>
                        </td>
                    </tr>

                    <tr>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/stokes/stokes.pdf?inline=true">Stokes' Theorem</a></td>
                        <td>
	                    <a href="https://youtube.com/playlist?list=PLwK75AcBxZCDeS2tWChN2SaIdsdTdcsQ2"><img src="/pix/icons/youtube.png"></a>
	                    <a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/stokes/stokes-hw.pdf?inline=true"><img src="/pix/icons/memo.svg"></a>
                        </td>
                    </tr>

                    <tr>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/gauss/gauss.pdf?inline=true">Gauss' Divergence Theorem</a></td>
                        <td>
	                    <a href="https://youtube.com/playlist?list=PLwK75AcBxZCCqkNocDpyruM7W68rbqm4j"><img src="/pix/icons/youtube.png"></a>
	                    <a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/gauss/gauss-hw.pdf?inline=true"><img src="/pix/icons/memo.svg"></a>
                        </td>
                    </tr>

                    <tr>
                        <td><a href="https://gitlab.oit.duke.edu/teaching/212-lectures/-/raw/master/big-picture/big-picture.pdf?inline=true">The Big Picture of Vector Calculus</a></td>
                        <td><a href="https://youtu.be/e7R1gbV_0W8"><img src="/pix/icons/youtube.png"></a></td>
                    </tr>

                </table>
