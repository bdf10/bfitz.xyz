<?php
return [
    'h1' => 'Math 780: Calculus and Probability',
    'icons' => [
        ['./', 'schedule', 'list.svg'],
        ['./cal.php', 'calendar', 'calendar.svg'],
        ['https://duke.zoom.us/j/97291535986?pwd=2z95TQgNbXtzjeLXwwLjeFvbFoY3Wp.1', 'Brian\'s Zoom', 'B.png'],
        ['https://duke.zoom.us/j/5468489121', 'Ching-Lung\'s Zoom', 'C.png'],
        ['https://gradescope.com', 'gradescope', 'gradescope.png'],
        ['https://gitlab.oit.duke.edu/bdf10/course-policies/-/raw/master/780f24/780f24-policies.pdf', 'policies', 'scroll.svg'],
    ],
    'msg' => '<p>Welcome! You have found the homepage of the Fall 2024 manifestation of Math 780.</p>'
];
?>
