<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead() ?>

<h2>Schedule</h2>

<p>Quizzes are distributed through gradescope.</p>

<?php
$fdoc = mktime(hour: 6, day: 26, month: 8, year: 2024);
$schedule = [
    [
        'debut' => strtotime("-16 week", $fdoc),
        'col1' => 'Week 0',
        'lecname' => 'Welcome!',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/780-lessons/-/raw/main/topics/welcome/terms/f24/780f24-welcome.pdf',
        'icons' => [
            /* 'youtube' => 'https://youtu.be/woooFxSCwWo', */
            'book' => 'https://assets.openstax.org/oscms-prodcms/media/documents/Calculus_Volume_1_-_WEB_68M1Z5W.pdf',
        ],
    ],
    [],
    [
        'debut' => strtotime("-16 week", $fdoc),
        'col1' => 'Week 1',
        'lecname' => 'Lines',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/780-lessons/-/raw/main/topics/lines/lines.pdf',
        'icons' => [
            'book' => 'https://assets.openstax.org/oscms-prodcms/media/documents/Calculus_Volume_1_-_WEB_68M1Z5W.pdf#page=44',
        ],
    ],
    [
        'debut' => strtotime("-16 week", $fdoc),
        'lecname' => 'Trig',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/780-lessons/-/raw/main/topics/trig/trig.pdf',
        'icons' => [
            'book' => 'https://assets.openstax.org/oscms-prodcms/media/documents/Calculus_Volume_1_-_WEB_68M1Z5W.pdf#page=70',
            'sage' => './trig-sage.php',
            'circle' => 'https://gitlab.oit.duke.edu/bdf10/780-lessons/-/raw/main/topics/trig/unit-circle.pdf',
        ],
    ],
    [],
    [
        'debut' => strtotime("+1 week", $fdoc),
        'col1' => 'Week 2',
        'lecname' => 'Exponential and Power Functions',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/780-lessons/-/raw/main/topics/pow/pow.pdf',
        'icons' => [
            'book' => 'https://assets.openstax.org/oscms-prodcms/media/documents/CalculusVolume1-OP.pdf#page=104',
        ],
    ],
    [
        'debut' => strtotime("+1 week", $fdoc),
        'lecname' => 'Inverse Functions',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/780-lessons/-/raw/main/topics/inverse/inverse.pdf',
        'icons' => [
            'book' => 'https://assets.openstax.org/oscms-prodcms/media/documents/CalculusVolume1-OP.pdf#page=86',
        ],
    ],
    [],
    [
        'col1' => 'Week 3',
        'debut' => strtotime("+2 week", $fdoc),
        'lecname' => 'Limits and Continuity',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/780-lessons/-/raw/main/topics/limits/limits.pdf',
        'icons' => [
            'book' => 'https://assets.openstax.org/oscms-prodcms/media/documents/CalculusVolume1-OP.pdf#page=143',
        ],
    ],
    [
        'debut' => strtotime("+2 week", $fdoc),
        'lecname' => 'The Derivative',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/780-lessons/-/raw/main/topics/fprime/fprime.pdf',
        'icons' => [
            'book' => 'https://assets.openstax.org/oscms-prodcms/media/documents/CalculusVolume1-OP.pdf#page=222',
        ],
    ],
    [],
    [
        'col1' => 'Week 4',
        'debut' => strtotime("+3 week", $fdoc),
        'lecname' => 'The Chain Rule',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/780-lessons/-/raw/main/topics/chain/chain.pdf',
        'icons' => [
            'book' => 'https://assets.openstax.org/oscms-prodcms/media/documents/CalculusVolume1-OP.pdf#page=295',
        ],
    ],
    [
        'debut' => strtotime("+3 week", $fdoc),
        'lecname' => 'Product, Quotient, and Trig Rules',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/780-lessons/-/raw/main/topics/prod/prod.pdf',
        'icons' => [
            'book' => 'https://assets.openstax.org/oscms-prodcms/media/documents/CalculusVolume1-OP.pdf#page=255',
        ],
    ],
    [],    
    [
        'col1' => 'Week 5',
        'debut' => strtotime("+4 week", $fdoc),
        'lecname' => 'Approximations and Concavity',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/780-lessons/-/raw/main/topics/approx/approx.pdf',
        'icons' => [
            'book' => 'https://assets.openstax.org/oscms-prodcms/media/documents/CalculusVolume1-OP.pdf#page=362',
        ],
    ],
    [
        'debut' => strtotime("+4 week", $fdoc),
        'lecname' => 'Implicit Differentiation <strong style="color:red"> ⟵ covered in week 6</strong>',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/780-lessons/-/raw/main/topics/implicit/implicit.pdf',
        'icons' => [
            'book' => 'https://assets.openstax.org/oscms-prodcms/media/documents/CalculusVolume1-OP.pdf#page=317',
        ],
    ],
    [],    
    [
        'col1' => 'Week 6',
        'debut' => strtotime("+5 week", $fdoc),
        'lecname' => 'Related Rates',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/780-lessons/-/raw/main/topics/rrates/rrates.pdf',
        'icons' => [
            'book' => 'https://assets.openstax.org/oscms-prodcms/media/documents/CalculusVolume1-OP.pdf#page=350',
        ],
    ],
    [
        'debut' => strtotime("+5 week", $fdoc),
        'lecname' => 'Optimization',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/780-lessons/-/raw/main/topics/critical/critical.pdf',
        'icons' => [
            'book' => 'https://assets.openstax.org/oscms-prodcms/media/documents/CalculusVolume1-OP.pdf#page=374',
        ],
    ],
    [],
    [
        'col1' => 'Week 7-8',
        'debut' => strtotime("+6 week", $fdoc),
        'lecname' => 'L\'Hopital\'s Rule',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/780-lessons/-/raw/main/topics/hospital/hospital.pdf',
        'icons' => [
            'book' => 'https://assets.openstax.org/oscms-prodcms/media/documents/CalculusVolume1-OP.pdf#page=462',
        ],
    ],
    [
        'debut' => strtotime("+6 week", $fdoc),
        'lecname' => 'The Definite Integral',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/780-lessons/-/raw/main/topics/riemann/riemann.pdf',
        'icons' => [
            'book' => 'https://assets.openstax.org/oscms-prodcms/media/documents/CalculusVolume1-OP.pdf#page=537',
        ],
    ],
    [],
    [
        'debut' => strtotime("+8 week", $fdoc),
        'col1' => 'Week 9',
        'lecname' => 'The Fundamental Theorem of Calculus',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/780-lessons/-/raw/main/topics/ftc/ftc.pdf',
        'icons' => [
            'book' => 'https://assets.openstax.org/oscms-prodcms/media/documents/CalculusVolume1-OP.pdf#page=557',
        ],
    ],
    [
        'debut' => strtotime("+8 week", $fdoc),
        'lecname' => 'u-Substitution',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/780-lessons/-/raw/main/topics/usub/usub.pdf',
        'icons' => [
            'book' => 'https://assets.openstax.org/oscms-prodcms/media/documents/CalculusVolume1-OP.pdf#page=592',
        ],
    ],
    [],
    [
        'col1' => 'Week 10',
        'debut' => strtotime("+9 week", $fdoc),
        'lecname' => 'Integration by Parts',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/780-lessons/-/raw/main/topics/ibp/ibp.pdf',
        'icons' => [
            'book' => 'https://assets.openstax.org/oscms-prodcms/media/documents/CalculusVolume2-OP.pdf#page=270',
        ],
    ],
    [
        'debut' => strtotime("+9 week", $fdoc),
        'lecname' => 'Improper Integrals',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/780-lessons/-/raw/main/topics/improper/improper.pdf',
        'icons' => [
            'book' => 'https://assets.openstax.org/oscms-prodcms/media/documents/CalculusVolume2-OP.pdf#page=338',
        ],
    ],
    [],
    [
        'col1' => 'Week 11',
        'debut' => strtotime("+10 week", $fdoc),
        'lecname' => 'Probability Distributions',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/780-lessons/-/raw/main/topics/pdf/pdf.pdf',
        'icons' => [
            'book' => 'https://math.dartmouth.edu/~prob/prob/prob.pdf#page=49',
        ],
    ],
    [
        'debut' => strtotime("+10 week", $fdoc),
        'lecname' => 'Expected Value',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/780-lessons/-/raw/main/topics/ev/ev.pdf',
        'icons' => [
            'book' => 'https://math.dartmouth.edu/~prob/prob/prob.pdf#page=233',
        ],
    ],
    [],
    [
        'col1' => 'Week 12',
        'debut' => strtotime("+11 week", $fdoc),
        'lecname' => 'Cumulative Distribution Functions',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/780-lessons/-/raw/main/topics/cdf/cdf.pdf',
        'icons' => [
            'book' => 'https://math.dartmouth.edu/~prob/prob/prob.pdf#page=69',
        ],
    ],
    [],
    [
        'debut' => strtotime("+12 week", $fdoc),
        'col1' => 'Week 13-14',
        'lecname' => 'Variance',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/780-lessons/-/raw/main/topics/sd/sd.pdf',
        'icons' => [
            'book' => 'https://math.dartmouth.edu/~prob/prob/prob.pdf#page=276',
        ],
    ],
    [
        'debut' => strtotime("+12 week", $fdoc),
        'lecname' => 'Named Distributions',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/780-lessons/-/raw/main/topics/names/names.pdf',
        'icons' => [
            'book' => 'https://math.dartmouth.edu/~prob/prob/prob.pdf#page=276',
        ],
    ],
    /* [
     *     'debut' => strtotime("+13 week", $fdoc),
     *     'lecname' => 'Double Integrals',
     *     'leclink' => 'https://gitlab.oit.duke.edu/bdf10/780-lessons/-/raw/main/topics/iint/iint.pdf',
     *     'icons' => [
     *         'book' => 'https://assets.openstax.org/oscms-prodcms/media/documents/CalculusVolume3-OP.pdf#page=486',
     *     ],
     * ], */
];
if (basename($_SERVER["SCRIPT_FILENAME"], '.php') == 'index') {
    mktbl($schedule);
} else {
    mktbl($schedule, rolling: false);
}
?>
