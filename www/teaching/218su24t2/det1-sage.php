<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Determinants I</h2>
<p>We can calculate the determinant of a matrix with the
   syntax <code>A.det()</code>.
<div class="compute">
<script type="text/x-sage">
A = random_matrix(ZZ, 10)

print(f'A=\n{A}\n')
print(f'det(A)={A.det()}')
</script>
</div>
