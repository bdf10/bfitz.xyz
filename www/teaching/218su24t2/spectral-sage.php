<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>The Spectral Theorem</h2>
<p>The following code will calculate a spectral
   factorization <code>H=UDU^*</code> of a Hermitian matrix <code>H</code>,
   assuming that the eigenvalues of <code>H</code> are nice enough.
<div class="compute">
<script type="text/x-sage">
H = matrix([(4, -2, 1), (-2, 4, 1), (1, 1, 1)])

if not H.is_hermitian(): raise TypeError('H is not Hermitian')
U = matrix.column(flatten(list((map(lambda t: matrix(map(lambda v: v.normalized(), matrix(t[1]).gram_schmidt()[0])).rows(), H.eigenvectors_right())))))
D = U.H*H*U

print(f'H = \n{H}\n')
print(f'U = \n{U}\n')
print(f'D = \n{D}')
</script>
</div>
