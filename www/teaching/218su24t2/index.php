<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead() ?>

<h2>Schedule</h2>

<p>Quizzes are distributed through gradescope.</p>

<?php
$fdoc = mktime(hour: 6, day: 1, month: 7, year: 2024);
include($_SERVER['DOCUMENT_ROOT'].'/scripts/topics.php');
$schedule = [
    ['debut' => strtotime("-4 week", $fdoc),'col1' => 'Week 0'] +
    $welcome_218_su,
    [],
    ['debut' => strtotime("-4 week", $fdoc),'col1' => 'Week 1'] +
    $matvec_su,
    $adjectives_su + ['comment' => '<strong style="color:red"> ⟵ complete at home!</strong>'],
    $lincomb_su,
    $digraphs_su,
    $geometry_su,
    $matmult_su,
    [],
    ['debut' => strtotime("+1 week", $fdoc),'col1' => 'Week 2'] +
    $rref_su,
    $axb_su,
    $gj_su,
    $nonsing_su,
    $ear_su,
    $palu_su,
    $evals_su,
    [],
    [
        'debut' => strtotime("+1 week", $fdoc),
        'col1' => date("D d-M", mktime(hour: 0, day: 18, month: 7, year: 2024)),
        'lecname' => 'Exam I',
        'comment' => '<strong style="color:red"> ⟵ covers weeks 1-2</strong>',
        'leclink' => './218su24t2-exam01.pdf',
        /* 'leclink' => './exam01.php', */
        'icons' => [
            'info' => './exam01.php',
            'key' => './218su24t2-exam01-solutions.pdf'
        ],
    ],
    [],
    ['debut' => strtotime("+2 week", $fdoc),'col1' => 'Week 3'] +
    $null_su,
    $col_su,
    $fundsub_su,
    $linind_su,
    $bases_su,
    $dim_su + ['comment' => '<strong style="color:red"> ⟵ covered in week 4</strong>'],
    [],
    ['debut' => strtotime("+3 week", $fdoc),'col1' => 'Week 4'] +
    $orthog_su,
    $proj_su,
    $xhat_su,
    $aqr_su,
    $gs_su,
    $det1_su + ['comment' => '<strong style="color:red"> ⟵ covered in week 5</strong>'],
    [],
    [
        'debut' => strtotime("+3 week", $fdoc),
        'col1' => date("D d-M", mktime(hour: 0, day: 1, month: 8, year: 2024)),
        'lecname' => 'Exam II',
        'comment' => '<strong style="color:red"> ⟵ covers weeks 3-4</strong>',
        'leclink' => './218su24t2-exam02.pdf',
        /* 'leclink' => './exam02.php', */
        'icons' => [
            'info' => './exam02.php',
            'key' => './218su24t2-exam02-solutions.pdf'
        ],
    ],
    [],
    ['debut' => strtotime("+4 week", $fdoc),'col1' => 'Week 5'] +
    $det2_su,
    $clx_su,
    $poly_su,
    $chi_su,
    [],
    ['debut' => strtotime("+5 week", $fdoc),'col1' => 'Week 6'] +
    $diag_su,
    $exp_su,
    $spectral_su,
    $posdef_su,
    $svd_su,
    [],
    [
        'debut' => strtotime("+5 week", $fdoc),
        'col1' => date("D d-M", mktime(hour: 0, day: 10, month: 8, year: 2024)),
        'lecname' => 'Final Exam',
        'leclink' => './final.php',
        'comment' => '<strong style="color:red"> ⟵ click for info!</strong>',
    ],
];
if (basename($_SERVER["SCRIPT_FILENAME"], '.php') == 'index') {
    mktbl($schedule);
} else {
    mktbl($schedule, rolling: false);
}
?>
