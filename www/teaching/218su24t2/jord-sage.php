<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Jordan Canonical Form</h2>
<p>Do some math! Try defining <code>A = matrix([(1, 2, 3), (4, 5, 6)])</code>.
<div class="compute">
    <script type="text/x-sage">
     A = matrix([(1, -2, 3, 8), (4, -9, 15, 3)])
    </script>
</div>
