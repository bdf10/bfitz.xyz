<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Partial Derivatives</h2>

<p>The syntax <code>f.diff(x)</code> will produce the partial derivative of a
scalar field <code>f</code> with respect to the variable <code>x</code></p>

<?php
sagecell(<<<'EOF'
var('x y z')
f = x**2*y+y**2*cos(x*y*z)

print(f'The partial derivatives of\n')
print(f'f = {f}\n')
print(f'with respect to {f.variables()} are\n')
print(f'{f.gradient().column()}')
EOF)
?>
