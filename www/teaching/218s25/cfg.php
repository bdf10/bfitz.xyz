<?php
return [
    'h1' => 'Math 218: Matrices and Vectors',
    'icons' => [
        ['./', 'schedule', 'list.svg'],
        ['./cal.php', 'calendar', 'calendar.svg'],
        ['mailto:bfitzpat@math.duke.edu', 'email', 'email.svg'],
        ['https://duke.zoom.us/j/94116122584?pwd=H4OgRdwn0xNy5vnu25XpbYabvCCt2H.1', 'Brian\'s Zoom', 'zoom.svg'],
        ['https://gradescope.com', 'gradescope', 'gradescope.png'],
        ['https://gitlab.oit.duke.edu/bdf10/course-policies/-/raw/master/218s25/218s25-policies.pdf', 'policies', 'scroll.svg'],
        ['./exams.php', 'exams', 'folder.svg'],
    ],
    'msg' => "<p>Welcome! You have found the homepage of the Spring 2025 manifestation of Math 218.</p>"
];
?>
