<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead() ?>

<h2>Schedule</h2>

<p>Quizzes and problem sets are distributed through gradescope.</p>

<?php
$fdoc = mktime(hour: 6, day: 6, month: 1, year: 2025);
include($_SERVER['DOCUMENT_ROOT'].'/scripts/topics.php');
$schedule = [
    ['debut' => strtotime("-4 week", $fdoc),'col1' => 'Week 0'] +
    $welcome_218,
    [],
    ['debut' => strtotime("-4 week", $fdoc),'col1' => 'Week 1'] +
    $matvec,
    $adjectives + ['comment' => '<strong style="color:red"> ⟵ complete at home! (w/quiz but no pset)</strong>'],
    $lincomb,
    [],
    ['debut' => strtotime("+1 week", $fdoc),'col1' => 'Week 2'] +
    $digraphs,
    $geometry,
    $matmult + ['comment' => '<strong style="color:red"> ⟵ deadline extended to Tu 28-Jan!</strong>'],
    [],
    ['debut' => strtotime("+2 week", $fdoc),'col1' => 'Week 3'] +
    $rref,
    $axb + ['comment' => '<strong style="color:red"> ⟵ deadline extended to Tu 4-Feb!</strong>'],
    [],
    ['debut' => strtotime("+3 week", $fdoc),'col1' => 'Week 4'] +
    $gj,
    $nonsing,
    [],
    [
        'debut' => strtotime("+3 week", $fdoc),
        'col1' => date("D d-M", mktime(hour: 0, day: 7, month: 2, year: 2025)),
        'lecname' => 'Exam I',
        'comment' => '<strong style="color:red"> ⟵ covers weeks 1-4</strong>',
        /* 'leclink' => './exam01.php', */
        'leclink' => './218s25-exam01.pdf',
        'icons' => [
            'info' => './exam01.php',
            'key' => './218s25-exam01-solutions.pdf'
        ],
    ],
    [],
    ['debut' => strtotime("+4 week", $fdoc),'col1' => 'Week 5'] +
    $ear,
    $palu,
    [],
    ['debut' => strtotime("+5 week", $fdoc),'col1' => 'Week 6'] +
    $evals,
    $null,
    $col,
    [],
    ['debut' => strtotime("+6 week", $fdoc),'col1' => 'Week 7'] +
    $fundsub,
    $linind,
    $bases,
    [],
    ['debut' => strtotime("+7 week", $fdoc),'col1' => 'Week 8'] +
    $dim,
    $orthog,
    $proj,
    [],
    [
        'debut' => strtotime("+7 week", $fdoc),
        'col1' => date("D d-M", mktime(hour: 0, day: 7, month: 3, year: 2025)),
        'lecname' => 'Exam II',
        'comment' => '<strong style="color:red"> ⟵ covers weeks 5-8</strong>',
        'leclink' => './exam02.php',
        /* 'leclink' => './218s25-exam02.pdf', */
        'icons' => [
            'info' => './exam02.php',
            /* 'key' => './218s25-exam02-solutions.pdf' */
        ],
    ],
    [],
    ['debut' => strtotime("+8 week", $fdoc),'col1' => 'Week 9'] +
    $xhat,
    $aqr,
    [],
    ['debut' => strtotime("+8 week", $fdoc),'col1' => 'Week 10'] +    
    $sb25,
    [],
    ['debut' => strtotime("+10 week", $fdoc),'col1' => 'Week 11'] +
    $gs,
    $det1,
    $det2,
    [],
    ['debut' => strtotime("+11 week", $fdoc),'col1' => 'Week 12'] +
    $clx,
    $poly,
    $chi,
    [],
    ['debut' => strtotime("+12 week", $fdoc),'col1' => 'Week 13'] +
    $diag,
    $exp,
    $spectral,
    [],
    ['debut' => strtotime("+13 week", $fdoc),'col1' => 'Week 14'] +
    $posdef,
    $svd,
    [],
    [
        'debut' => strtotime("+14 week", $fdoc),
        'col1' => date("D d-M", mktime(hour: 0, day: 18, month: 4, year: 2025)),
        'lecname' => 'Exam III',
        'comment' => '<strong style="color:red"> ⟵ covers weeks 9-14</strong>',
        /* 'leclink' => './218s25-exam03.pdf', */
        'leclink' => './exam03.php',
        'icons' => [
            'info' => './exam03.php',
            /* 'key' => './218s25-exam03-solutions.pdf', */
        ],
    ],
    [],
    ['debut' => strtotime("+14 week", $fdoc),'col1' => 'Week 15'] +
    $partial,
    $jacobian,
    [],    
    ['debut' => strtotime("+15 week", $fdoc),'col1' => 'Week 16'] +
    $cholesky,
    $hessian,
    [],
    [
        'debut' => strtotime("+15 week", $fdoc),
        'col1' => date("D d-M", mktime(hour: 0, day: 29, month: 4, year: 2025)),
        'lecname' => 'Final Exam',
        'leclink' => './final.php',
        'comment' => '<strong style="color:red"> ⟵ click for info! (covers everything)</strong>',
    ],
];
if (basename($_SERVER["SCRIPT_FILENAME"], '.php') == 'index') {
    mktbl($schedule);
} else {
    mktbl($schedule, rolling: false);
}
?>
