<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead() ?>

<h2>Final Exam</h2>

<p> The Final Exam is scheduled to take place on <code>Friday 10-December</code> from
    <code>2:00 PM to 5:00 PM</code>.</p>

<p> You will either take the final exam in <code>Biological Sciences 111</code> or <code>French Science 2231</code>,
    depending on which discussion section you are enrolled in. Please verify your discussion number on DukeHub.</p>

<ul>
    <li><code>Biological Sciences 111</code></li>
    <ul>
        <li>Math 218D-2-01D</li>
        <li>Math 218D-2-02D</li>
        <li>Math 218D-2-03D</li>
        <li>Math 218D-2-04D</li>
        <li>Math 218D-2-05D</li>
        <li>Math 218D-2-06D</li>
        <li>Math 218D-2-09D</li>
    </ul>
    <li><code>French Science 2231</code></li>
    <ul>
        <li>Math 218D-2-07D</li>
        <li>Math 218D-2-08D</li>
        <li>Math 218D-2-10D</li>
    </ul>
</ul>

<p> The exam will consist of mostly free-response problems with a few fill in
    the blank and multiple choice questions. You should be prepared to solve
    problems that you have not seen before.</p>

<p> The exam is <code>cumulative</code>, so you should be prepared for every
    topic covered throughout the semester to be represented on the exam.</p>

<p> The exam will be "closed" in the sense that no outside aid (like books or
    calculators) will be allowed. The only thing you need to bring is a writing
    utensil.</p>

<p> To prepare for the exam, you should be able to:</p>

<ul>
    <li>Correctly articulate all definitions and theorems from lecture.</li>
    <li>Clearly and coherently solve all examples from lecture.</li>
    <li>Write correct and legible solutions to all problems from the midterm
        exams, problem sets, and <a href="./218f21-final--study.pdf">discussion worksheets</a>.</li>
    <li>Clearly explain how to solve each comprehension quiz problem.</li>
</ul>

<p> If you feel confident that you can accomplish everything described above and
    would like some additional practice, you can review the extra material
    suggested for studying for <a href="./exam01.php">Exam I</a>,
    <a href="./exam02.php">Exam II</a>, and <a href="./exam03.php">Exam III</a>.
    Additionally, here are some <code>optional</code> problems
    from Gilbert Strang's textbook. Solutions to these problems will not be
    provided, but I will happily discuss them in office hours.</p>

<ul>
    <li>Section 6.5 Exercises 2, 3, 5, 7, 25, 26, 30, 31</li>
    <li>Section 7.2 Exercises 2, 4, 5, 7, 15, 16, 17</li>
</ul>
