<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>The Hessian</h2>
<p>The syntax <code>f.hessian()</code> calls Hessian derivative of a function <code>f</code>.</p>

<?php
sagecell(<<<'EOF'
var('x y z')
f = x*y*z**3+exp(x*y)

print(f'f={f}')
print(f'Hf=\n{f.hessian()}')
EOF)
?>
