<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Cholesky Factorizations</h2>
<p>The following code will calculate <code>S=LDL^T</code> if this factorization exists.</p>
<?php
sagecell(<<<'EOF'
S = matrix([(7, -21, 14, -42), (-21, 61, -52, 140), (14, -52, -17, 6), (-42, 140, 6, 235)])

if not S.is_hermitian():
    raise TypeError('Matrix is not Hermitian!')
P, L, U = S.LU(pivot='nonzero')
if P != identity_matrix(S.nrows()):
    raise TypeError('Matrix is indefinite with no LU factorization.')
D = diagonal_matrix(U.diagonal())
if L*D*L.T != S:
    raise TypeError('LDL^T != S. Something went wrong!')


print(f'S=\n{S}\n')
print(f'L=\n{L}\n')
print(f'D=\n{D}')
EOF)
?>

<p>The following code will calculate a Cholesky factorization <code>S=R^TR</code> if <code>S</code> is positive semidefinite.</p>
<?php
sagecell(<<<'EOF'
S = matrix(ZZ, [[9, -6, 3, 12], [-6, 29, -2, -3], [3, -2, 1, 4], [12, -3, 4, 123]])

if not S.is_hermitian():
    raise TypeError('Matrix is not Hermitian!')
P, L, U = S.LU(pivot='nonzero')
if P != identity_matrix(S.nrows()):
    raise TypeError('Matrix is indefinite with no LU factorization.')
R = diagonal_matrix(map(sqrt, U.diagonal()))*L.T

print(f'S=\n{S}\n')
print(f'R=\n{R}')
EOF)
?>
