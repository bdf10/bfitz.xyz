<?php
return [
    'h1' => 'Math 218: Matrices and Vector Spaces',
    'icons' => [
        ['./', 'schedule', 'list.svg'],
        ['./cal.php', 'calendar', 'calendar.svg'],
        ['https://gradescope.com', 'gradescope', 'gradescope.png'],
        ['https://gitlab.oit.duke.edu/bdf10/course-policies/-/raw/master/218f21/218f21-policies.pdf?inline=true', 'policies', 'scroll.svg'],
    ],
    'msg' => '<p>Welcome! You have found the homepage of the Fall 2021 manifestation of Math 218.</p>'
];
?>
