<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>The Gram-Schmidt Algorithm</h2>
<p>We can calculate the projection of a vector <code>v</code> in the direction
   of <code>v1</code> with the syntax <code>(v1*v)/(v1*v1) * v1</code>.
<div class="compute">
<script type="text/x-sage">
v1 = vector([3, -5, 8, 4])
v = vector([17, 9, -12, 5])

print(f'v1={v1}\n')
print(f'v={v}\n')
print(f'proj_v1(v)={(v1*v)/(v1*v1)*v1}')
</script>
</div>

<p>We can calculate the component of a vector <code>v</code> in the direction
   of <code>v1</code> with the syntax <code>(v1*v)/v1.norm()</code>.
<div class="compute">
<script type="text/x-sage">
v1 = vector([3, -5, 8, 4])
v = vector([17, 9, -12, 5])

print(f'v1={v1}\n')
print(f'v={v}\n')
print(f'comp_v1(v)={(v1*v)/v1.norm()}')
</script>
</div>

<p>The Gram-Schmidt algorithm is built into sage!
<div class="compute">
<script type="text/x-sage">
basis_of_V = [(0, -1, 1, -1), (1, -2, 2, -2), (-10, 14, -13, 9)]

W = matrix(basis_of_V).gram_schmidt()[0].T
print(f'V = Span{basis_of_V}\n')
print(f'Mutually orthogonal basis of V: {W.columns()}\n')
print(f'Orthonormal basis of V: {list(map(lambda v: v.normalized(), W.T))}')
</script>
</div>

<p>We can then use the Gram-Schmidt algorithm to calculate <code>A=QR</code>.
<div class="compute">
<script type="text/x-sage">
A = matrix([(0, 1, -10), (-1, -2, 14), (1, 2, -13), (-1, -2, 9)])

W = A.T.gram_schmidt()[0]
Q = matrix.column(map(lambda v: v.normalized(), W))
print(f'A=\n{A}\n')
print(f'Q=\n{Q}\n')
print(f'R=\n{Q.T*A}')
</script>
</div>
