<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Gauss-Jordan Elimination</h2>
<p>The syntax <code>A.rref()</code> produces the reduced row echelon form of a
   matrix.
<div class="compute">
<script type="text/x-sage">
A = matrix([(7, 28, -18, 43), (-4, -16, 13, -30), (-2, -8, 5, -12)])

print(f'A = \n{A}\n')
print(f'rref(A) = \n{A.rref()}')
</script>
</div>

<p>We use the same syntax to produce the reduced row echelon form of an
   augmented matrix.
<div class="compute">
<script type="text/x-sage">
A = matrix([(3, -14, 40, -20), (2, -9, 26, -13), (-3, 11, -34, 17)])
b = vector([34, 22, -27])

system = A.augment(b, subdivide=True)

print(f'[A | b] = \n{system}\n')
print(f'rref[A | b] = \n{system.rref()}')
</script>
</div>

<p>Rank and nullity can be calculated with the syntax <code>A.rank()</code>
   and <code>A.right_nullity()</code>.
<div class="compute">
<script type="text/x-sage">
A = matrix([[13, -26, 38, -32, -186, -134, 384, -313, -211], [-3, 6, -5, 6, 36, 23, -71, 55, 37], [-2, 4, -3, 5, 29, 18, -57, 43, 27], [-1, 2, -5, 4, 23, 18, -49, 40, 26], [-4, 8, -12, 10, 58, 42, -120, 98, 66], [-3, 6, -6, 2, 20, 16, -40, 33, 31]])

print(f'A = \n{A}\n')
print(f'rank(A) = {A.rank()}')
print(f'nullity(A) = {A.right_nullity()}')
</script>
</div>
