<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Orthogonality</h2>
<p>The following code with produce a basis of the orthogonal complement of a
   vector space.
<div class="compute">
<script type="text/x-sage">
basis_of_V = [(-2, 6, -1, 0), (-3, 9, -2, -1)]

A = matrix.column(basis_of_V)

print(f'V is spanned by the columns of:\n{A}\n')
print(f'V^perp is spanned by the columns of:\n{A.left_kernel().basis_matrix().T}')
</script>
</div>
