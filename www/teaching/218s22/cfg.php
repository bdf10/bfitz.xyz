<?php
return [
    'h1' => 'Math 218: Matrices and Vector Spaces',
    'icons' => [
        ['./', 'schedule', 'list.svg'],
        ['./cal.php', 'calendar', 'calendar.svg'],
        ['https://duke.zoom.us/j/96471292021?pwd=ckEvNlAwVlFCZk1VRzk3S2NRVjBKZz09', 'Brian Zoom (passcode: linear)', 'b.svg'],
        ['https://duke.zoom.us/j/99336718283?pwd=ZU8rdzR5UkhCdHFSWXozRHloYUt5QT09', 'Ruby Zoom (passcode: linear)', 'r.svg'],
        ['https://duke.zoom.us/j/93806074652?pwd=b1hRTWdUSlBrdklUL3E5ZWtnVDNDUT09', 'Yupeng Zoom (passcode: linear)', 'y.svg'],
        ['https://duke.zoom.us/j/2769042707?pwd=SEpOQitqTW9rWnhSczV3S0R6eTRzZz09', 'Mo Zoom (passcode: 923644)', 'm.svg'],
        ['https://gradescope.com', 'gradescope', 'gradescope.png'],
        ['https://gitlab.oit.duke.edu/bdf10/course-policies/-/raw/master/218s22/218s22-policies.pdf?inline=true', 'policies', 'scroll.svg'],
    ],
    'msg' => '<p>Welcome! You have found the homepage of the Spring 2022 manifestation of Math 218.</p>'
];
?>
