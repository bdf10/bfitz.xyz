<!doctype html>
<?php $TITLE='Math 218: Matrices and Vector Spaces'; ?>

<title><?php echo $TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="/css/style.css">

<?php include($_SERVER['DOCUMENT_ROOT'].'/menu.php') ?>
<?php include('./menu.php') ?>

<script src="https://sagecell.sagemath.org/static/embedded_sagecell.js"></script>
<script>
 // Make the div with id 'mycell' a Sage cell
 sagecell.makeSagecell({inputLocation:  '#mycell',
			template:       sagecell.templates.minimal,
			evalButtonText: 'Activate'});
 // Make *any* div with class 'compute' a Sage cell
 sagecell.makeSagecell({inputLocation: 'div.compute',
			evalButtonText: 'Evaluate'});
</script>

<h2>titlegoeshere</h2>
<p>Do some math! Try defining <code>A = matrix([(1, 2, 3), (4, 5, 6)])</code>.
<div class="compute">
    <script type="text/x-sage">
     A = matrix([(1, -2, 3, 8), (4, -9, 15, 3)])
    </script>
</div>
