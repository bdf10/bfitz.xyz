<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead() ?>

<h2>Final Exam</h2>

<p> The Final Exam is scheduled to take place on <code>Saturday 30-April</code> from
    <code>2:00 PM to 5:00 PM</code>.</p>

<p> The location of the final exam is <code>LSRC B101</code></p>

<p> The exam will consist of mostly free-response problems with a few fill in
    the blank and multiple choice questions. You should be prepared to solve
    problems that you have not seen before.</p>

<p> The exam is <code>cumulative</code>, so you should be prepared for every
    topic covered throughout the semester to be represented on the exam.</p>

<p> The exam will be "closed" in the sense that no outside aid (like books or
    calculators) will be allowed. The only thing you need to bring is a writing
    utensil.</p>

<p> The schedule of office hours leading up to the final will be:</p>
<ul>
    <li>Brian's Office Hours</li>
    <ul>
        <li>Monday 25-April 11am-1pm (Zoom: <a href="https://duke.zoom.us/j/96471292021?pwd=ckEvNlAwVlFCZk1VRzk3S2NRVjBKZz09" title="Brian Zoom (passcode: linear)"><img src="/pix/icons/b.svg" style="height: 1.00em !important"></a>)</li>
        <li>Tuesday 26-April 9am-10am and 1:15pm-2:15pm (Zoom: <a href="https://duke.zoom.us/j/96471292021?pwd=ckEvNlAwVlFCZk1VRzk3S2NRVjBKZz09" title="Brian Zoom (passcode: linear)"><img src="/pix/icons/b.svg" style="height: 1.00em !important"></a>)</li>
        <li>Wednesday 27-April 11am-1pm (Zoom: <a href="https://duke.zoom.us/j/96471292021?pwd=ckEvNlAwVlFCZk1VRzk3S2NRVjBKZz09" title="Brian Zoom (passcode: linear)"><img src="/pix/icons/b.svg" style="height: 1.00em !important"></a>)</li>
    </ul>
    <li>Ruby's Office Hours</li>
    <ul>
        <li>Monday 25-April 1pm-3pm (Zoom: <a href="https://duke.zoom.us/j/99336718283?pwd=ZU8rdzR5UkhCdHFSWXozRHloYUt5QT09" title="Ruby Zoom (passcode: linear)"><img src="/pix/icons/r.svg" style="height: 1.00em !important"></a>)</li>
        <li>Wednesday 27-April 1pm-3pm (Zoom: <a href="https://duke.zoom.us/j/99336718283?pwd=ZU8rdzR5UkhCdHFSWXozRHloYUt5QT09" title="Ruby Zoom (passcode: linear)"><img src="/pix/icons/r.svg" style="height: 1.00em !important"></a>)</li>
    </ul>
    <li>Mo's Office Hours</li>
    <ul>
        <li>Tuesday 26-April 2:15pm-4:15pm (Zoom: <a href="https://duke.zoom.us/j/2769042707?pwd=SEpOQitqTW9rWnhSczV3S0R6eTRzZz09" title="Mo Zoom (passcode: 923644)"><img src="/pix/icons/m.svg" style="height: 1.00em !important"></a>)</li>
        <li>Wednesday 27-April 3pm-5pm (Zoom: <a href="https://duke.zoom.us/j/2769042707?pwd=SEpOQitqTW9rWnhSczV3S0R6eTRzZz09" title="Mo Zoom (passcode: 923644)"><img src="/pix/icons/m.svg" style="height: 1.00em !important"></a>)</li>
    </ul>
    <li>Yupeng's Office Hours</li>
    <ul>
        <li>Thursday 28-April 2pm-4pm (In-Person: Physics 274J)</li>
        <li>Friday 29-April 2pm-4pm (In-Person: Physics 274J)</li>
    </ul>
</ul>

<p> To prepare for the exam, you should be able to:</p>

<ul>
    <li>Correctly articulate all definitions and theorems from lecture.</li>
    <li>Clearly and coherently solve all examples from lecture.</li>
    <li>Write correct and legible solutions to all problems from the midterm
        exams, <a href="./218s22-hws.pdf">problem sets</a>, and <a href="./218s22-final--study.pdf">discussion worksheets</a>.</li>
    <li>Clearly explain how to solve each comprehension quiz problem.</li>
</ul>

<p> If you feel confident that you can accomplish everything described above and
    would like some additional practice, you can review the extra material
    suggested for studying for <a href="./exam01.php">Exam I</a>,
    <a href="./exam02.php">Exam II</a>, and <a href="./exam03.php">Exam III</a>.
    Additionally, here are some <code>optional</code> problems
    from Gilbert Strang's textbook. Solutions to these problems will not be
    provided, but I will happily discuss them in office hours.</p>

<ul>
    <li>Section 6.4 Exercises 1, 5-8, 9(a), 9(c), 15</li>
    <li>Section 6.5 Exercises 4, 7, 8, 16, 20</li>
    <li>Section 6.5 Exercises 2, 3, 5, 7, 25, 26, 30, 31</li>
    <li>Section 7.2 Exercises 2, 4, 5, 7, 15, 16, 17</li>
</ul>
