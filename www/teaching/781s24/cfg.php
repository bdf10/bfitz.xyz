<?php
return [
    'h1' => 'Math 781: Matrices and Data',
    'icons' => [
        ['./', 'schedule', 'list.svg'],
        ['./cal.php', 'calendar', 'calendar.svg'],
        ['https://gradescope.com', 'gradescope', 'gradescope.png'],
        ['https://gitlab.oit.duke.edu/bdf10/course-policies/-/raw/master/781s24/781s24-policies.pdf', 'policies', 'scroll.svg'],
    ],
    'msg' => "<p>Welcome! You have found the homepage of the Spring 2024 manifestation of Math 781.</p>"
];
?>
