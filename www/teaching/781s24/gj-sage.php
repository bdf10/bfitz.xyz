<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Gauss-Jordan Elimination</h2>
<p>The syntax <code>A.rref()</code> produces the reduced row echelon form of a
   matrix.
<div class="compute">
<script type="text/x-sage">
A = matrix([(7, 28, -18, 43), (-4, -16, 13, -30), (-2, -8, 5, -12)])

print(f'A = \n{A}\n')
print(f'rref(A) = \n{A.rref()}')
</script>
</div>

<p>We use the same syntax to produce the reduced row echelon form of an
   augmented matrix.
<div class="compute">
<script type="text/x-sage">
A = matrix([(3, -14, 40, -20), (2, -9, 26, -13), (-3, 11, -34, 17)])
b = vector([34, 22, -27])

system = A.augment(b, subdivide=True)

print(f'[A | b] = \n{system}\n')
print(f'rref[A | b] = \n{system.rref()}')
</script>
</div>

<p>Rank and nullity can be calculated with the syntax <code>A.rank()</code>
   and <code>A.right_nullity()</code>.
<div class="compute">
<script type="text/x-sage">
A = matrix([[13, -26, 38, -32, -186, -134, 384, -313, -211], [-3, 6, -5, 6, 36, 23, -71, 55, 37], [-2, 4, -3, 5, 29, 18, -57, 43, 27], [-1, 2, -5, 4, 23, 18, -49, 40, 26], [-4, 8, -12, 10, 58, 42, -120, 98, 66], [-3, 6, -6, 2, 20, 16, -40, 33, 31]])

print(f'A = \n{A}\n')
print(f'rank(A) = {A.rank()}')
print(f'nullity(A) = {A.right_nullity()}')
</script>
</div>

<p>The following code will show the steps in the Gauss-Jordan algorithm used to reduce a matrix <code>A</code> to <code>rref(A)</code></p>

<?php
sagecell(<<<'EOF'
A = matrix([(-4, 20, -12), (-1, 5, -3), (5, -25, 16)])

elem = elementary_matrix


def row_swap(i, i0, n):
    st = f'r{i+1} <-> r{i0+1}'
    E = elem(n, row1=i, row2=i0)
    return st, E


def row_scale(i, c, n):
    st = f'{c} * r{i+1} -> r{i+1}'
    E = elem(n, row1=i, scale=c)
    return st, E


def row_add(d, p, n):
    def pretty_int(x):
        if x == 1:
            return '+'
        if x == -1:
            return '-'
        if x < 0:
            return f'- {abs(x)} *'
        return f'+ {x} *'
    st = '\n'.join([f'r{i+1} {pretty_int(-m)} r{p+1} -> r{i+1}' for i, m in sorted(d.items()) if i != p])
    E = prod([elem(n, row1=i, row2=p, scale=-m) for i, m in d.items() if i != p])
    return st, E


def print_step(st, A):
    print('\n'.join([st, A.__repr__()]))
    print('\n')


def rref(A):
    i = j = 0
    m, n = A.dimensions()
    print(A)
    print('\n')
    while i <= m-1 and j <= n-1:
        if A[i, j] == 0:
            try:
                i0 = min(filter(lambda x: x > i, A.T[j].dict()))
                st, E = row_swap(i, i0, m)
                A = E*A
                print_step(st, A)
            except ValueError:
                j += 1
        elif A[i, j] != 1:
            st, E = row_scale(i, 1/A[i, j], m)
            A = E*A
            print_step(st, A)
        else:
            st, E = row_add(A.T[j].dict(), i, m)
            if E != 1:
                A = E*A
                print_step(st, A)
            i += 1
            j += 1


rref(A)
EOF)
?>
