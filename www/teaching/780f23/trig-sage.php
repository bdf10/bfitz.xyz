<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Trig</h2>

<p>In class, we discussed how manipulating <code>sin</code> and <code>cos</code> accounts
    for different types of oscillation.</p>
<div style="text-align: center">
    <img src="sin-1.png" alt="sin" class="center" width="66%">
</div>

<p>The following code will plot <code>sin(x)</code> (blue) and <code>A*sin(B*(x+C))+D</code> (green)
    after the user defines <code>A</code>, <code>B</code>, <code>C</code>, and <code>D</code></p>


<?php
sagecell(<<<'EOF'
A = 1/2
B = 2
C = 1/3
D = 1/4

plot([sin(x), A*sin(B*(x+C))+D], x, -4*pi, 4*pi)
EOF)
?>
