<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Least Squares</h2>
<p>The following code analyzes the least squares problem associated to a given
   system.
<div class="compute">
<script type="text/x-sage">
A = matrix([(1, 0, -1), (0, 1, 1), (1, 1, 2), (0, -1, -2)])
b = vector([2, 0, 3, -4])

print(f'[A|b]=\n{A.augment(b, subdivide=True)}\n')
print(f'rref[A|b]=\n{A.augment(b, subdivide=True).rref()}\n')
print(f'[A^TA|A^Tb]=\n{(A.T*A).augment(A.T*b, subdivide=True)}\n')
print(f'rref[A^TA|A^Tb]=\n{(A.T*A).augment(A.T*b, subdivide=True).rref()}\n')
print(f'x_hat={A.pseudoinverse()*b}\n')
print(f'E={(b-A*A.pseudoinverse()*b).norm()**2}')
</script>
</div>

<p>The following code will calculate the least squares line of best fit to a
  given list of data points.
<div class="compute">
<script type="text/x-sage">
pts = [(1, 6), (2, 5), (3, 7), (4, 10)]

x_hat = matrix([(len(pts), sum(matrix(pts).T[0])), (sum(matrix(pts).T[0]), matrix(pts).T[0]*matrix(pts).T[0])]).pseudoinverse()*vector([sum(matrix(pts).T[1]), matrix(pts).T[0]*matrix(pts).T[1]])
var('t')
f_hat = vector([1, t])*x_hat
print(f'Points are the rows of:\n{matrix(pts)}\n')
print(f'Least squares line of best fit is:\nf_hat(t)={f_hat}\n')
ptplot = plot(point(pts, size=100))
fplot = plot(f_hat, t, ptplot.xmin()-1, ptplot.xmax()+1)
ptplot+fplot
</script>
</div>
