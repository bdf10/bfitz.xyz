<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead() ?>

<h2>Schedule</h2>

<p>Quizzes and problem sets are distributed through gradescope.</p>

<?php
$fdoc = mktime(hour: 6, day: 9, month: 1, year: 2023);
include($_SERVER['DOCUMENT_ROOT'].'/scripts/topics.php');
$schedule = [
    ['debut' => strtotime("-4 week", $fdoc),'col1' => 'Week 0'] +
    $welcome_218,
    [],
    ['debut' => strtotime("-4 week", $fdoc),'col1' => 'Week 1'] +
    $matvec,
    $adjectives + ['comment' => '<strong style="color:red"> ⟵ complete at home! (w/quiz but no pset)</strong>'],
    $lincomb,
    [],
    ['debut' => strtotime("+1 week 1 days", $fdoc),'col1' => 'Week 2'] +
    $digraphs,
    $geometry,
    [],
    ['debut' => strtotime("+2 week", $fdoc),'col1' => 'Week 3'] +
    $matmult,
    $rref,
    $axb,
    [],
    ['debut' => strtotime("+3 week", $fdoc),'col1' => 'Week 4'] +
    $gj,
    $nonsing,
    $ear,
    [],
    ['debut' => strtotime("+4 week", $fdoc),'col1' => 'Week 5'] +
    $palu,
    $evals,
    $null,
    [],
    [
        'debut' => strtotime("+4 week", $fdoc),
        'col1' => date("D d-M", mktime(hour: 0, day: 17, month: 2, year: 2023)),
        'lecname' => 'Exam I',
        'comment' => '<strong style="color:red"> ⟵ covers weeks 1-5</strong>',
        'leclink' => './218s23-exam01.pdf',
        'icons' => [
            'info' => './exam01.php',
            'key' => './218s23-exam01-solutions.pdf'
        ],
    ],
    [],
    ['debut' => strtotime("+5 week", $fdoc),'col1' => 'Week 6'] +
    $col,
    $fundsub,
    [],
    ['debut' => strtotime("+6 week", $fdoc),'col1' => 'Week 7'] +
    $linind,
    $bases,
    $dim,
    [],
    ['debut' => strtotime("+7 week", $fdoc),'col1' => 'Week 8'] +
    $orthog,
    $proj,
    $xhat,
    [],
    ['debut' => strtotime("+8 week", $fdoc),'col1' => 'Week 9'] +
    $aqr,
    $gs,
    $det1,
    [],
    [
        'debut' => strtotime("+7 week", $fdoc),
        'col1' => date("D d-M", mktime(hour: 0, day: 24, month: 3, year: 2023)),
        'lecname' => 'Exam II',
        'comment' => '<strong style="color:red"> ⟵ covers weeks 6-9</strong>',
        'leclink' => './218s23-exam02.pdf',
        'icons' => [
            'info' => './exam02.php',
            'key' => './218s23-exam02-solutions.pdf'
        ],
    ],
    [],
    ['debut' => strtotime("+9 week", $fdoc),'col1' => 'Week 10'] +
    $det2,
    $clx,
    [],
    ['debut' => strtotime("+11 week", $fdoc),'col1' => 'Week 11'] +
    $poly,
    $chi,
    $diag,
    [],
    ['debut' => strtotime("+12 week", $fdoc),'col1' => 'Week 12'] +
    $exp,
    $spectral,
    $posdef + ['comment' => '<strong style="color:red"> ⟵ assignment deadline pushed to Tu 18-April</strong>'],
    [],
    ['debut' => strtotime("+13 week", $fdoc),'col1' => 'Week 13'] +
    $svd,
    [],
    [
        'debut' => strtotime("+13 week", $fdoc),
        'col1' => date("D d-M", mktime(hour: 0, day: 21, month: 4, year: 2023)),
        'lecname' => 'Exam III',
        'comment' => '<strong style="color:red"> ⟵ covers weeks 10-13</strong>',
        'leclink' => './218s23-exam03.pdf',
        'icons' => [
            'info' => './exam03.php',
            'key' => './218s23-exam03-solutions.pdf',
        ],
    ],
    [],
    ['debut' => strtotime("+14 week", $fdoc),'col1' => 'Week 14'] +
    $partial,
    $jacobian,
    [],
    ['debut' => strtotime("+15 week", $fdoc),'col1' => 'Week 15'] +
    $cholesky,
    $hessian,
    [],
    [
        'debut' => strtotime("+15 week", $fdoc),
        'col1' => date("D d-M", mktime(hour: 0, day: 2, month: 5, year: 2023)),
        'lecname' => 'Final Exam',
        'leclink' => './final.php',
        'comment' => '<strong style="color:red"> ⟵ click for info!</strong>',
    ],
];
if (basename($_SERVER["SCRIPT_FILENAME"], '.php') == 'index') {
    mktbl($schedule);
} else {
    mktbl($schedule, rolling: false);
}
?>
