<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Matrix-Vector Products</h2>

<p>We can calculate linear combinations by summing scaled vectors</p>

<?php
sagecell(<<<'EOF'
v1 = vector([-1, 1, 2, 2])
v2 = vector([2, -2, 0, -1])
v3 = vector([1, 1, 1, -1])

c1 = 9
c2 = -3
c3 = 6

print(f'c1*v1 + c2*v2 + c3*v3 = {c1*v1+c2*v2+c3*v3}')
EOF)
?>

<p>Of course, the previous linear combination can be more elegantly calculated
    with a matrix-vector product <code>A * v</code>.</p>

<?php
sagecell(<<<'EOF'
A = matrix([(-1, 2, 1), (1, -2, 1), (2, 0, 1), (2, -1, -1)])
v = vector([9, -3, 6])

print(f'A=\n{A}\n')
print(f'v=\n{v}\n')
print(f'A * v = {A * v}')
EOF)
?>
