<?php
return [
    'h1' => 'Math 218: Matrices and Vectors',
    'icons' => [
        ['./', 'schedule', 'list.svg'],
        ['./cal.php', 'calendar', 'calendar.svg'],
        ['https://duke.zoom.us/j/95585011098?pwd=a3hEdDhQdWJxQWFlRngrWVpiOGJaUT09', 'Brian\'s Zoom', 'B.png'],
        ['https://duke.zoom.us/j/91964419709', 'Niny\'s Zoom', 'N.png'],
        ['https://gradescope.com', 'gradescope', 'gradescope.png'],
        ['https://gitlab.oit.duke.edu/bdf10/course-policies/-/raw/master/218s23/218s23-policies.pdf', 'policies', 'scroll.svg'],
    ],
    'msg' => "<p>Welcome! You have found the homepage of the Spring 2023 manifestation of Math 218.</p>"
];
?>
