<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Null Spaces</h2>
<p>To check if a vector <code>v</code> is in the null space of a
   matrix <code>A</code>, we need only check that <code>A*v</code> is equal to
   the zero vector.
<div class="compute">
<script type="text/x-sage">
A = matrix([(-2, -7, 16), (-3, -11, 25), (-2, -13, 28), (-1, -9, 19)])
v = vector([1, 2, 1])

print(f'A = \n{A}\n')
print(f'v = {v}\n')
print(f'A*v = {A*v}')
</script>
</div>

<p>More efficiently, we can directly verify if <code>v</code> is in the null
   space of <code>A</code> with the syntax <code>A*v ==
   zero_vector(A.nrows())</code>.
<div class="compute">
<script type="text/x-sage">
A = matrix([(-2, -7, 16), (-3, -11, 25), (-2, -13, 28), (-1, -9, 19)])
v = vector([1, 2, 1])

print(f'A = \n{A}\n')
print(f'v = {v}\n')
print(f'Is v in Null(A)? {A*v == zero_vector(A.nrows())}')
</script>
</div>

<p>To check if <code>v</code> is an eigenvector of <code>A</code> corresponding
   to an eigenvalue <code>l</code>, we need to check if <code>v</code> is in the
   null space of <code>l*identity_matrix(A.nrows())-A</code>.
<div class="compute">
<script type="text/x-sage">
A = matrix([(41, 36, 108, -36), (-12, -55, 12, 204), (-12, 0, -43, -36), (0, -12, 12, 53)])
v = vector([3, 3, -2, 1])
l = -7

print(f'A = \n{A}\n')
print(f'v = {v}\n')
print(f'l = {l}\n')
print(f'(l*I-A)*v = {(l*identity_matrix(A.nrows())-A)*v}')
</script>
</div>

<p>More efficiently, we can check if <code>A*v == l*v</code>.
<div class="compute">
<script type="text/x-sage">
A = matrix([(41, 36, 108, -36), (-12, -55, 12, 204), (-12, 0, -43, -36), (0, -12, 12, 53)])
v = vector([3, 3, -2, 1])
l = -7

print(f'A = \n{A}\n')
print(f'v = {v}\n')
print(f'l = {l}\n')
print(f'A*v == l*v? {A*v == l*v}')
</script>
</div>

<p>For a 2x2 matrix, we can visualize the "input" vector <code>v</code> and the
   "output" vector <code>A*v</code>. The vector <code>v</code> is an eigenvector
   of <code>A</code> if input is <em>parallel</em> to the output.
<div class="compute">
<script type="text/x-sage">
A = matrix([(1, 2), (-1, 4)])
v1 = vector([1, 1])
v2 = vector([2, 1])
v3 = vector([-3, 1])

v1.plot()+(A*v1).plot()+v2.plot(color='green')+(A*v2).plot(color='green')+v3.plot(color='red')+(A*v3).plot(color='red')
</script>
</div>
