<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Definiteness</h2>
<p>Sage is capable of constructing the quadratic form defined by any
   real-symmetric matrix <code>S</code>.
<div class="compute">
<script type="text/x-sage">
S = matrix([(-6, -5, 1), (-5, -10, -3), (1, -3, 2)])

q = QuadraticForm(2*S).polynomial(names=[f'x{i}' for i in range(1, S.nrows()+1)])
print(f'S =\n{S}\n')
print(f'q(x) = <x, Sx> = {q}')
</script>
</div>

<p>The following code will calculate the real-symmetric matrix representing a
   defined quadratic form.
<div class="compute">
<script type="text/x-sage">
var('x1 x2 x3')
q = 3*x1**2 - 7*x1*x2 + 11*x2**2 - 4*x1*x3 + 2*x2*x3 - 19*x3**2

S = q.hessian() / 2
print(f'q(x) = <x, Sx> = {q}\n')
print(f'S =\n{S}')
</script>
</div>
