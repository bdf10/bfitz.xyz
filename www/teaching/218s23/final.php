<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead() ?>

<h2>Final Exam</h2>

<code>PLEASE FILL OUT YOUR COURSE EVALUATIONS</code>

<p> The Final Exam is scheduled to take place on <code>Tuesday 2-May</code> from
    <code>7:00 PM to 10:00 PM</code> in <code>Gross Hall 107</code>.</p>

<p> The exam will consist of mostly free-response problems with a few fill in
    the blank and multiple choice questions. You should be prepared to solve
    problems that you have not seen before.</p>

<p> The exam is <code>cumulative</code>, so you should be prepared for every
    topic covered throughout the semester to be represented on the exam.</p>

<p> The exam will be "closed" in the sense that no outside aid (like books or
    calculators) will be allowed. The only thing you need to bring is a writing
    utensil.</p>

<p> The schedule of office hours leading up to the final will be:</p>
<ul>
    <li>Brian's Office Hours</li>
    <ul>
        <li>Thursday 27-April 8:30am - 10:30am (Zoom)</li>
        <li>Friday 28-April 8:30am - 10:30am (Zoom)</li>
        <li>Sunday 30-April 2:30pm - 4:30pm (Zoom)</li>
        <li>Monday 1-May 1:15pm - 3:15pm (Zoom)</li>
    </ul>
    <li>Niny's Office Hours</li>
    <ul>
        <li>Friday 28-April 4:00pm - 6:00pm (Zoom)</li>
        <li>Monday 1-May 10:00am - 12:00pm (Zoom)</li>
    </ul>
</ul>

<p> To prepare for the exam, you should be able to:</p>

<ul>
    <li>Correctly articulate all definitions and theorems from lecture.</li>
    <li>Clearly and coherently solve all examples from lecture.</li>
    <li>Write correct and legible solutions to all problems from the midterm
        exams, <a href="./218s23-psets.pdf">problem sets</a>, and discussion worksheets.</li>
    <li>Clearly explain how to solve each comprehension quiz problem.</li>
</ul>

<p> If you feel confident that you can accomplish everything described above and
    would like some additional practice, you can review the extra material
    suggested for studying for <a href="./exam01.php">Exam I</a>,
    <a href="./exam02.php">Exam II</a>, and <a href="./exam03.php">Exam III</a>.
    Additionally, here are some <code>optional</code> problems
    from Gilbert Strang's textbook. Solutions to these problems will not be
    provided, but I will happily discuss them in office hours.</p>

<ul>
    <li>Section 7.2 Exercises 2, 4, 5, 7, 15, 16, 17</li>
</ul>
