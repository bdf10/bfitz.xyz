<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Matrix Exponentials</h2>
<p>Sage can calculate matrix exponentials with the syntax <code>exp(A)</code>.</p>
<?php
sagecell(<<<'EOF'
A = matrix([(20, 54), (-9, -25)])

print(f'A=\n{A}')
print(f'exp(A)=\n{expand(exp(A))}')
EOF)
?>

<p>By declaring a variable <code>t</code> and an initial vector <code>u0</code>,
    we can find the unique solution to u'=Au with initial condition u(0)=u0 with <code>exp(A*t)*u0</code>.</p>
<?php
sagecell(<<<'EOF'
var('t')
u0 = vector([0, 2])
A = matrix([(1, -1), (1, 1)])

print(f'A=\n{A}\n')
print(f'u(t)={expand(exp(A*t)*u0)}')
EOF)
?>
