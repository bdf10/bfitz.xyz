<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead() ?>

<h2>Schedule</h2>

<p>Quizzes and problem sets are distributed through gradescope.</p>

<?php
$fdoc = mktime(hour: 6, day: 6, month: 1, year: 2025);
include($_SERVER['DOCUMENT_ROOT'].'/scripts/topics.php');
$schedule = [
    ['debut' => strtotime("-4 week", $fdoc),'col1' => 'Week 0'] +
    $welcome_781,
    [],
    ['debut' => strtotime("-4 week", $fdoc),'col1' => 'Week 1'] +
    $matvec,
    $adjectives + ['comment' => '<strong style="color:red"> ⟵ complete at home!</strong>'],
    $lincomb,
    [],
    ['debut' => strtotime("+1 week", $fdoc),'col1' => 'Week 2'] +
    $digraphs,
    $geometry,
    $matmult + ['comment' => '<strong style="color:red"> ⟵ covered in week 3</strong>'],
    [],
    ['debut' => strtotime("+2 week", $fdoc),'col1' => 'Week 3'] +
    $rref,
    $axb,
    [],
    ['debut' => strtotime("+3 week", $fdoc),'col1' => 'Week 4'] +
    $gj,
    [],
    ['debut' => strtotime("+4 week", $fdoc),'col1' => 'Week 5'] +
    $nonsing,
    $ear,
    $palu,
    [],
    ['debut' => strtotime("+5 week", $fdoc),'col1' => 'Week 6'] +
    $evals,
    $null,
    [],
    ['debut' => strtotime("+6 week", $fdoc),'col1' => 'Week 7'] +
    $col,
    $fundsub,
    $linind,
    [],
    ['debut' => strtotime("+7 week", $fdoc),'col1' => 'Week 8'] +
    $bases,
    $dim,
    $orthog,
    [],
    ['debut' => strtotime("+8 week", $fdoc),'col1' => 'Week 9'] +
    $proj,
    $xhat,
    [],
    ['debut' => strtotime("+10 week", $fdoc),'col1' => 'Week 10'] +
    $aqr,
    [],
    ['debut' => strtotime("+11 week", $fdoc),'col1' => 'Week 11'] +
    $gs + ['comment' => '<strong style="color:red"> ⟵ originally week 10  (deadline: 4-April)</strong>'],
    $det1,
    $det2,
    [],
    ['debut' => strtotime("+12 week", $fdoc),'col1' => 'Week 12'] +
    $clx + ['comment' => '<strong style="color:red"> ⟵ originally week 11  (deadline: 11-April)</strong>'],
    $poly,
    $chi,
    [],
    ['debut' => strtotime("+12 week", $fdoc),'col1' => 'Week 13'] +
    $diag + ['comment' => '<strong style="color:red"> ⟵ originally week 12  (deadline: 18-April)</strong>'],
    $spectral,
    $posdef,
    [],
    ['debut' => strtotime("+14 week", $fdoc),'col1' => 'Week 14'] +
    $svd,
];
if (basename($_SERVER["SCRIPT_FILENAME"], '.php') == 'index') {
    mktbl($schedule);
} else {
    mktbl($schedule, rolling: false);
}
?>
