<?php
return [
    'h1' => 'Math 781: Matrices and Data',
    'icons' => [
        ['./', 'schedule', 'list.svg'],
        ['./cal.php', 'calendar', 'calendar.svg'],
        ['https://duke.zoom.us/j/95497205367?pwd=ioXE0aqh7Tu9InLLHoFEkSxOYmitxN.1', 'Brian\'s Zoom', 'zoom.svg'],
        ['https://gradescope.com', 'gradescope', 'gradescope.png'],
        ['https://gitlab.oit.duke.edu/bdf10/course-policies/-/raw/master/781s25/781s25-policies.pdf', 'policies', 'scroll.svg'],
    ],
    'msg' => "<p>Welcome! You have found the homepage of the Spring 2025 manifestation of Math 781.</p>"
];
?>
