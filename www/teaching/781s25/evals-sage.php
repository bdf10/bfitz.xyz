<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Eigenvalues</h2>
<p>We can check if <code>l</code> is an eigenvalue of <code>A</code> by
   studying <code>(l*identity_matrix(A.nrows())-A).rref()</code>.
<div class="compute">
<script type="text/x-sage">
A = matrix([(-41, 200, -150), (-15, 69, -45), (-5, 20, -6)])
l = 9

print(f'A=\n{A}\n')
print(f'rref({l}I-A)=\n{(l*identity_matrix(A.nrows())-A).rref()}')
</script>
</div>

<p>More efficiently, we can directly check if <code>l</code> is an eigenvalue
   of <code>A</code>
   with <code>(l*identity_matrix(A.nrows())-A).is_singular()</code>.
<div class="compute">
<script type="text/x-sage">
A = matrix([(-41, 200, -150), (-15, 69, -45), (-5, 20, -6)])
l = 9

print(f'A=\n{A}\n')
print(f'Is l={l} an eigenvalue? {(l*identity_matrix(A.nrows())-A).is_singular()}')
</script>
</div>

<p>The command <code>(l*identity_matrix(A.nrows())-A).right_nullity()</code>
   calculates the geometric multiplicity of an eigenvalue.
<div class="compute">
<script type="text/x-sage">
A = matrix([(-41, 200, -150), (-15, 69, -45), (-5, 20, -6)])
l = 9

print(f'A=\n{A}\n')
print(f'gm_A({l})={(l*identity_matrix(A.nrows())-A).right_nullity()}')
</script>
</div>
