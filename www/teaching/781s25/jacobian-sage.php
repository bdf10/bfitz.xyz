<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Linear Approximations</h2>
<p>The syntax <code>jacobian(f, VARIABLES)</code> calls the Jacobian derivative of a function <code>f</code>, where <code>VARIABLES</code> is the list of relevant variables.</p>

<?php
sagecell(<<<'EOF'
var('x y z')
f = vector([x**2*y*cos(x*z), exp(x*y)*sin(y*z), sin(x*y*z**2)])
VARIABLES=(x, y, z)

print(f'f={f}')
print(f'Df=\n{jacobian(f, VARIABLES)}')
EOF)
?>

