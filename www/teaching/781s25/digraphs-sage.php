<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Digraphs</h2>

<p>Sage handles digraphs! Consider the following digraph.</p>

<div style="text-align: center">
  <img src="digraph.png" alt="digraph" class="center" width="400">
</div>

<p>Let's study this digraph!

<div class="compute">
<script type="text/x-sage">
G = DiGraph()
G.allow_multiple_edges(True)
G.add_vertices(['v1', 'v2', 'v3'])
G.add_edges([('v1', 'v2'), ('v1', 'v2'), ('v2', 'v3'), ('v3', 'v2'), ('v1', 'v3')])
show(G)
</script>
</div>

<p>The syntax <code>G.incidence_matrix()</code> produces the incidence matrix.

<div class="compute">
<script type="text/x-sage">
G = DiGraph()
G.allow_multiple_edges(True)
G.add_vertices(['v1', 'v2', 'v3'])
G.add_edges([('v1', 'v2'), ('v1', 'v2'), ('v2', 'v3'), ('v3', 'v2'), ('v1', 'v3')])
A = G.incidence_matrix()

print(f'A=\n{A}')
</script>
</div>

<p>Now, suppose we add weights to our digraph.

<div style="text-align: center">
  <img src="weighted-digraph.png" alt="digraph" class="center" width="400">
</div>

<p>We can calculate the net flow through the nodes with a matrix-vector product!

<div class="compute">
<script type="text/x-sage">
G = DiGraph()
G.allow_multiple_edges(True)
G.add_vertices(['v1', 'v2', 'v3'])
G.add_edges([('v1', 'v2'), ('v1', 'v2'), ('v2', 'v3'), ('v3', 'v2'), ('v1', 'v3')])
A = G.incidence_matrix()
w = vector([331, -470, -674, 101, 17])

print(f'Aw = {A*w}')
</script>
</div>
