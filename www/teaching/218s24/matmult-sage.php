<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Matrix Multiplication</h2>
<p>The syntax for matrix multiplication is <code>A * B</code>.
<div class="compute">
<script type="text/x-sage">
A = matrix([(1, -2, 3, 8), (4, -9, 15, 3)])
B = matrix([(1, 3, 2), (-9, 8, 7), (-4, 1, 0), (0, 2, 12)])

print(f'A=\n{A}\n')
print(f'B=\n{B}\n')
print(f'AB=\n{A*B}\n')
</script>
</div>
