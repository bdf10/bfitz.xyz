<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead() ?>

<h2>Final Exam</h2>

<p> The Final Exam is scheduled to take place on <code>Friday 3-May</code> from
    <code>9:00 AM to 12:00 PM</code> in <code>White Lecture Hall</code> (our
    usual lecture room).</p>

<p> The exam will consist of mostly free-response problems with a few fill in
    the blank and multiple choice questions. You should be prepared to solve
    problems that you have not seen before.</p>

<p> The exam is <code>cumulative</code>, so you should be prepared for every
    topic covered throughout the semester to be represented on the exam.</p>

<p> The exam will be "closed" in the sense that no outside aid (like books or
    calculators) will be allowed. The only thing you need to bring is a writing
    utensil.</p>

<p> The schedule of office hours leading up to the final will be:</p>
<ul>
    <li>Brian's Office Hours (all on <a href="https://duke.zoom.us/j/91312814296?pwd=Umh3aFNYb0JWbG9FZFhmZ0xuc3VLQT09">zoom</a>)</li>
    <ul>
        <li>Mon 29-April 8:30am-9:30am & 2pm-3pm</li>
        <li>Tue 30-April 8:30am-9:30am & 11:00am-12:00pm</li>
        <li>Wed 01-May 8:30am-9:30am & 2pm-3pm</li>
        <li>Thu 02-May 8:30am-9:30am & 11:00am-12:00pm</li>
    </ul>
    <li>Kai's Office Hours (all in the kitchen on the 3rd floor of Gross Hall)</li>
    <ul>
        <li>Mon 04-April 12pm-1pm</li>
        <li>Tue 05-April 12pm-1pm</li>
        <li>Wed 06-April 1pm-2pm</li>
    </ul>
    <li>Jing's Office Hours (all in Physics 246)</li>
    <ul>
        <li>Mon 04-April 5pm-6pm</li>
        <li>Wed 06-April 4pm-6pm</li>
    </ul>
    <li>Blake's Office Hours</li>
    <ul>
        <li>Mon 04-April 8pm-10pm (Physics 274J)</li>
        <li>Thu 07-April 6pm-8pm (<a href="https://duke.zoom.us/j/2495874202">zoom</a>)</li>
    </ul>
    <li>Ben's Office Hours</li>
    <ul>
        <li>Tue 05-April 8pm-10pm (Physics 274J)</li>
    </ul>
</ul>

<p> To prepare for the exam, you should be able to:</p>

<ul>
    <li>Correctly articulate all definitions and theorems from lecture.</li>
    <li>Clearly and coherently solve all examples from lecture.</li>
    <li>Write correct and legible solutions to all problems from the midterm
        exams, problem sets, and discussion worksheets.</li>
    <li>Clearly explain how to solve each comprehension quiz problem.</li>
</ul>
