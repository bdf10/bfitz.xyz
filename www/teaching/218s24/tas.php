<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<!-- Created by htmlize-1.57 in css mode. -->
<html>
    <head>
        <title>tas.org</title>
        <style type="text/css">
         <!--
         body {
             color: #839496;
             background-color: #002b36;
         }
         .hl-line {
             /* hl-line */
             background-color: #073642;
         }
         .org-document-info-keyword {
             /* org-document-info-keyword */
             color: #586e75;
         }
         .org-document-title {
             /* org-document-title */
             color: #93a1a1;
             font-weight: bold;
         }
         .org-hide {
             /* org-hide */
             color: #002b36;
         }
         .org-level-1 {
             /* org-level-1 */
             color: #cb4b16;
             background-color: #002b36;
         }
         .org-level-2 {
             /* org-level-2 */
             color: #859900;
             background-color: #002b36;
         }
         .org-level-3 {
             /* org-level-3 */
             color: #268bd2;
             background-color: #002b36;
         }
         .org-link {
             /* org-link */
             color: #b58900;
             font-weight: bold;
             text-decoration: underline;
         }

         a {
             color: inherit;
             background-color: inherit;
             font: inherit;
             text-decoration: inherit;
         }
         a:hover {
             text-decoration: underline;
         }
         -->
        </style>
    </head>
    <body>
        <pre>
            <span class="org-document-info-keyword">#+title:</span><span class="org-document-title">Fall 2022 TA Meeting
            </span>
            <span class="org-level-1">* Course Introduction</span>
            <span class="org-hide">*</span><span class="org-level-2">* url: </span><span class="org-level-2"><span class="org-link"><a href="https://bfitz.xyz/teaching/218f22/">https://bfitz.xyz/teaching/218f22</a><a href="https://bfitz.xyz/teaching/218f22/">/</a></span></span><span class="org-level-2"> (append "admin.php" for full schedule)</span>
            <span class="org-hide">*</span><span class="org-level-2">* Material released Mondays</span>
            <span class="org-hide">**</span><span class="org-level-3">* Slides and Resources on webpage</span>
            <span class="org-hide">**</span><span class="org-level-3">* Problem Sets and Quizzes on Gradescope (all written by me)</span>
            <span class="org-hide">**</span><span class="org-level-3">* SageMath</span>
            <span class="org-hide">*</span><span class="org-level-2">* Assignment deadline the following Tuesday (through gradescope)</span>
            <span class="org-hide">*</span><span class="org-level-2">* Three midterm exams</span>
            <span class="org-hide">*</span><span class="org-level-2">* Final exam</span>
            <span class="org-hide">*</span><span class="org-level-2">* Discussions</span>
            <span class="org-hide">*</span><span class="org-level-2">* GitLab pages (everything is being worked on)</span>
            <span class="org-hide">**</span><span class="org-level-3">* 218-assignments</span>
            <span class="org-hide">**</span><span class="org-level-3">* 218-discussions</span>
            <span class="org-hide">**</span><span class="org-level-3">* 218-exams</span>
            <span class="org-hide">**</span><span class="org-level-3">* 218-lessons</span>
            <span class="org-hide">*</span><span class="org-level-2">* Textbook (Strang, not very important)</span>
            <span class="org-level-1">* Responsibilities</span>
            <span class="org-hide">*</span><span class="org-level-2">* Run Discussion Sections (75 minutes each Thursday)</span>
            <span class="org-hide">**</span><span class="org-level-3">* Print discussion worksheet from GitLab under construction</span>
            <span class="org-hide">**</span><span class="org-level-3">* Students spend at least some time working alone and some time collaborating</span>
            <span class="org-hide">**</span><span class="org-level-3">* Whole worksheet should be debriefed by the end of discussion</span>
            <span class="org-hide">**</span><span class="org-level-3">* Collect worksheets, scan with department copier, upload to gradescope (type the names of students)</span>
            <span class="org-hide">**</span><span class="org-level-3">* Hand worksheets back next discussion</span>
            <span class="org-hide">**</span><span class="org-level-3">* Thursdays before Exams will (probably) be review</span>
            <span class="org-hide">**</span><span class="org-level-3">* Absent students: email me, cc the student. Brian will handle the absence.</span>
            <span class="org-hide">*</span><span class="org-level-2">* Grade midterm exams (gradescope)</span>
            <span class="org-hide">*</span><span class="org-level-2">* Three office hours per week (zoom or in person)</span>
            <span class="org-hide">**</span><span class="org-level-3">* Yixin: 9am-12pm Wednesday (zoom)</span>
            <span class="org-hide">**</span><span class="org-level-3">* Hwai-Ray: Tu 10:35-11:50 &amp; Th 11:30-1:15 (location tba)</span>
            <span class="org-hide">**</span><span class="org-level-3">* Yuqing: ?</span>
            <span class="org-hide">**</span><span class="org-level-3">* Yupei: ?</span>
            <span class="org-hide">**</span><span class="org-level-3">* Yupeng: ?</span>
            <span class="org-hide">**</span><span class="org-level-3">* Bowen: ?</span>
            <span class="org-hide">*</span><span class="org-level-2">* Submit three exam problems each Friday</span>
            <span class="org-hide">**</span><span class="org-level-3">* DukeBox link already sent</span>
            <span class="org-hide">*</span><span class="org-level-2">* Help me edit exams</span>
            <span class="org-level-1">* Misc</span>
            <span class="org-hide">*</span><span class="org-level-2">* Zoom Rooms</span>
            <span class="org-hide">**</span><span class="org-level-3">* Yixin: </span><span class="org-level-3"><span class="org-link"><a href="https://duke.zoom.us/j/96653764077">https://duke.zoom.us/j/96653</a><a href="https://duke.zoom.us/j/96653764077">76407</a><a href="https://duke.zoom.us/j/96653764077">7</a></span></span>
            <span class="org-hide">**</span><span class="org-level-3">* Hwai-Ray: </span><span class="org-level-3"><span class="org-link"><a href="https://duke.zoom.us/j/97308350791?pwd=eUJ1QTl5N3d5bGxTQ25Ddnpia2dUdz09">https://duke.zoom.us/j/97308350791?pwd=eUJ1QTl5N3d5bGxTQ25Ddnpia2dUdz0</a><a href="https://duke.zoom.us/j/97308350791?pwd=eUJ1QTl5N3d5bGxTQ25Ddnpia2dUdz09">9</a></span></span>
            <span class="org-hide">**</span><span class="org-level-3">* Yuqing: ?</span>
            <span class="org-hide">**</span><span class="org-level-3">* Yupei: ?</span>
            <span class="org-hide">**</span><span class="org-level-3">* Yupeng: </span><span class="org-level-3"><span class="org-link"><a href="https://duke.zoom.us/j/93806074652?pwd=b1hRTWdUSlBrdklUL3E5ZWtnVDNDUT09">https://duke.zoom.us/j/93806074652?pwd=b1hRTWdUSlBrdklUL3E5ZWtnVDNDUT0</a><a href="https://duke.zoom.us/j/93806074652?pwd=b1hRTWdUSlBrdklUL3E5ZWtnVDNDUT09">9</a></span></span>
            <span class="org-hide">**</span><span class="org-level-3">* Bowen: ?</span>
            <span class="org-hide">*</span><span class="org-level-2">* Communication</span>
            <span class="org-hide">**</span><span class="org-level-3">* Sakai Announcements (I will add you soon)</span>
        </pre>
    </body>
</html>
