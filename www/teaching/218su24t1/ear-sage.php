<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>EA=R Factorizations</h2>
<p>The following code will calculate <code>EA=R</code> using the Gauss-Jordan algorithm as articulated in class.</p>

<?php
sagecell(<<<'EOF'
A = matrix([(1, 2), (4, 9), (0, 1)])


elem = elementary_matrix

def row_swap(i, i0, n):
    E = elem(n, row1=i, row2=i0)
    return E


def row_scale(i, c, n):
    E = elem(n, row1=i, scale=c)
    return E


def row_add(d, p, n):
    E = prod([elem(n, row1=i, row2=p, scale=-m) for i, m in d.items() if i != p])
    return E


def ear(A):
    i = j = 0
    m, n = A.dimensions()
    E = identity_matrix(m)
    while i <= m-1 and j <= n-1:
        if A[i, j] == 0:
            try:
                i0 = min(filter(lambda x: x > i, A.T[j].dict()))
                Ek = row_swap(i, i0, m)
                A = Ek*A
                E = Ek*E
            except ValueError:
                j += 1
        elif A[i, j] != 1:
            Ek = row_scale(i, 1/A[i, j], m)
            A = Ek*A
            E = Ek*E
        else:
            Ek = row_add(A.T[j].dict(), i, m)
            A = Ek*A
            E = Ek*E
            i += 1
            j += 1
    return E

print(f'A=\n{A}')
print(f'E=\n{ear(A)}')
print(f'R=\n{A.rref()}')
EOF)
?>
