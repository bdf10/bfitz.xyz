<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead() ?>

<h2>Exam V</h2>

<p> Exam V is a <code>50 minute</code> exam scheduled to take place at the
    beginning of class on <code>Wed 21-June</code> and will cover all
    topics from <code>Week 5</code>.</p>

<p> The exam will be four single-sided pages and consist of mostly free-response
    problems with a few fill in the blank and multiple choice questions. Some of
    the problems will be sourced from the Week 5 problem sets, but you should
    also be prepared to solve problems that you have not seen before.</p>

<p> The exam will be "closed" in the sense that no outside aid (like books or
    calculators) will be allowed. The only thing you need to bring is a writing
    utensil.</p>

<p> To prepare for the exam, you should be able to:</p>

<ul>
    <li>Correctly articulate all definitions and theorems from lecture.</li>
    <li>Clearly and coherently solve all examples from lecture.</li>
    <li>Write correct and legible solutions to all problems from the problem
        sets and comprehension quizzes.</li>
</ul>

<p> If you feel confident that you can accomplish everything described above and
    would like some additional practice, you are free to browse my exams from
    <a href="./exams.php">previous semesters</a> (focus on the relevant problems
    from previous Exam III's).</p>
