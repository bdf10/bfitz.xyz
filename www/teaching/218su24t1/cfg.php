<?php
return [
    'h1' => 'Math 218: Matrices and Vectors',
    'icons' => [
        ['./', 'schedule', 'list.svg'],
        ['./cal.php', 'calendar', 'calendar.svg'],
        ['https://gradescope.com', 'gradescope', 'gradescope.png'],
        ['https://gitlab.oit.duke.edu/bdf10/course-policies/-/raw/master/218su24t1/218su24t1-policies.pdf', 'policies', 'scroll.svg'],
        ['./exams.php', 'exams', 'folder.svg'],
    ],
    'msg' => "<p>Welcome! You have found the homepage of the Summer 2024 manifestation of Math 218.</p>"
];
?>
