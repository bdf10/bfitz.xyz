<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead() ?>

<h2>Schedule</h2>

<p>Quizzes are distributed through gradescope.</p>

<?php
$fdoc = mktime(hour: 6, day: 13, month: 5, year: 2024);
include($_SERVER['DOCUMENT_ROOT'].'/scripts/topics.php');
$schedule = [
    ['debut' => strtotime("-4 week", $fdoc),'col1' => 'Week 0'] +
    $welcome_218_su,
    [],
    ['debut' => strtotime("-4 week", $fdoc),'col1' => 'Week 1'] +
    $matvec_su,
    $adjectives_su + ['comment' => '<strong style="color:red"> ⟵ complete at home!</strong>'],
    $lincomb_su,
    $digraphs_su,
    [],
    ['debut' => strtotime("+1 week", $fdoc),'col1' => 'Week 2'] +
    $geometry_su,
    $matmult_su,
    $rref_su,
    $axb_su,
    $gj_su,
    $nonsing_su,
    $ear_su + ['comment' => '<strong style="color:red"> ⟵ not on Exam I!</strong>'],
    [],
    [
        'debut' => strtotime("+1 week", $fdoc),
        'col1' => date("D d-M", mktime(hour: 0, day: 30, month: 5, year: 2024)),
        'lecname' => 'Exam I',
        'comment' => '<strong style="color:red"> ⟵ covers weeks 1&2 material</strong>',
        'leclink' => './218su24t1-exam01.pdf',
        'icons' => [
            'info' => './exam01.php',
            'key' => './218su24t1-exam01-solutions.pdf'
        ],
    ],
    [],
    ['debut' => strtotime("+2 week", $fdoc),'col1' => 'Week 3'] +
    $palu_su,
    $evals_su,
    $null_su,
    [],
    ['debut' => strtotime("+3 week", $fdoc),'col1' => 'Week 4'] +
    $col_su,
    $fundsub_su,
    $linind_su,
    $bases_su,
    $dim_su,
    $orthog_su,
    $proj_su,
    [],
    ['debut' => strtotime("+4 week", $fdoc),'col1' => 'Week 5'] +
    $xhat_su,
    $aqr_su,
    $gs_su,
    $det1_su,
    $det2_su,
    $clx_su,
    $poly_su,
    $chi_su,
    [],
    [
        'debut' => strtotime("+4 week", $fdoc),
        'col1' => date("D d-M", mktime(hour: 0, day: 20, month: 6, year: 2024)),
        'lecname' => 'Exam II',
        'comment' => '<strong style="color:red"> ⟵ covers weeks 3-5 & EA=R</strong>',
        'leclink' => './218su24t1-exam01.pdf',
        /* 'leclink' => './exam02.php', */
        'icons' => [
            'info' => './exam02.php',
            'key' => './218su24t1-exam02-solutions.pdf'
        ],
    ],
    [],
    ['debut' => strtotime("+5 week", $fdoc),'col1' => 'Week 6'] +
    $diag_su,
    $exp_su,
    $spectral_su,
    $posdef_su,
    [],
    ['debut' => strtotime("+5 week", $fdoc),'col1' => 'Week 7'] +
    $svd_su,
    [],
    [
        'debut' => strtotime("+5 week", $fdoc),
        'col1' => date("D d-M", mktime(hour: 0, day: 26, month: 6, year: 2024)),
        'lecname' => 'Final Exam',
        'leclink' => './final.php',
        'comment' => '<strong style="color:red"> ⟵ click for info!</strong>',
    ],
];
if (basename($_SERVER["SCRIPT_FILENAME"], '.php') == 'index') {
    mktbl($schedule);
} else {
    mktbl($schedule, rolling: false);
}
?>
