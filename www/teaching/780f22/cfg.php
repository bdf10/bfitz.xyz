<?php
return [
    'h1' => 'Math 780: Calculus and Probability',
    'icons' => [
        ['./', 'schedule', 'list.svg'],
        ['./cal.php', 'calendar', 'calendar.svg'],
        ['https://duke.zoom.us/j/94999327121?pwd=SzNxc09VZm95Z093SUpvMFU4R2JQZz09', 'zoom', 'zoom.svg'],
        ['https://gradescope.com', 'gradescope', 'gradescope.png'],
        ['https://gitlab.oit.duke.edu/bdf10/course-policies/-/raw/master/780f21/780f21-policies.pdf', 'policies', 'scroll.svg'],
    ],
    'msg' => '<p>Welcome! You have found the homepage of the Fall 2022 manifestation of Math 780.</p>'
];
?>
