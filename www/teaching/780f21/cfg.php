<?php
return [
    'h1' => 'Math 780: Calculus and Probability',
    'icons' => [
        ['./', 'schedule', 'list.svg'],
        ['./cal.php', 'calendar', 'calendar.svg'],
        ['https://gradescope.com', 'gradescope', 'gradescope.png'],
        ['https://sakai.duke.edu', 'sakai', 'edstem.png'],
        ['https://gitlab.oit.duke.edu/bdf10/course-policies/-/raw/master/780f21/780f21-policies.pdf?inline=true', 'policies', 'scroll.svg'],
    ],
    'msg' => '<p>Welcome! You have found the homepage of the Fall 2021 manifestation of Math 780.</p>'
];
?>
