<?php
return [
    'h1' => 'Math 781: Matrices and Data',
    'icons' => [
        ['./', 'schedule', 'list.svg'],
        ['./cal.php', 'calendar', 'calendar.svg'],
        ['https://duke.zoom.us/j/93819300076?pwd=b1ZvbHRqQklxYVJEYmVCOElxTHpXQT09', 'passcode: linear', 'zoom.svg'],
        ['https://gradescope.com', 'gradescope', 'gradescope.png'],
        ['https://sakai.duke.edu', 'ed discussion', 'edstem.png'],
        ['https://gitlab.oit.duke.edu/bdf10/course-policies/-/raw/master/781s22/781s22-policies.pdf?inline=true', 'policies', 'scroll.svg'],
    ],
    'msg' => '<p>Welcome! You have found the homepage of the Spring 2022 manifestation of Math 781.</p>'
];
?>
