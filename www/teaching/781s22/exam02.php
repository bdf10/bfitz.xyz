<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead() ?>

<h2>Exam II</h2>

<p> Exam II is scheduled to take place in class on <code>Fri 22-Oct</code> and will cover all
    topics from <code>Weeks 5-8</code>.</p>

<p> The exam will be four single-sided pages and consist of mostly free-response
    problems with a few fill in the blank and multiple choice questions. You
    should be prepared to solve problems that you have not seen before.</p>

<p> The exam will be "closed" in the sense that no outside aid (like books or
    calculators) will be allowed. The only thing you need to bring is a writing
    utensil.</p>

<p> To prepare for the exam, you should be able to:</p>

<ul>
    <li>Correctly articulate all definitions and theorems from lecture.</li>
    <li>Clearly and coherently solve all examples from lecture.</li>
    <li>Write correct and legible solutions to all problems from the problem
        sets and <a href="./218f21-e02--study.pdf">discussion worksheets</a>.</li>
    <li>Clearly explain how to solve each comprehension quiz problem.</li>
</ul>

<p> If you feel confident that you can accomplish everything described above and
    would like some additional practice, here are some <code>optional</code> problems
    from Gilbert Strang's textbook. Solutions to these problems will not be
    provided, but I will happily discuss them in office hours.</p>

<ul>
    <li>Section 3.1 Exercises 9(a), 19, 20, 22, 28, 29</li>
    <li>Section 6.1 Exercises 8, 9, 13</li>
    <li>Section 3.2 Exercises 15-19, 22-23, 25</li>
    <li>Section 3.3 Exercises 7, 31</li>
    <li>Section 3.4 Exercises 1, 2</li>
    <li>Section 3.5 Exercises 2, 3, 4(a), 4(c)</li>
    <li>Section 4.1 Exercises 9, 17, 19, 21, 25, 26, 29</li>
    <li>Section 4.2 Exercises 1, 11, 13, , 19, 20</li>
    <li>Section 4.3 Exercises 1</li>
    <li>Section 4.4 Exercises 3, 5, 7, 21, 22</li>
</ul>
