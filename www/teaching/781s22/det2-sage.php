<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Determinants II</h2>
<p>We can calculate the adjugate of a matrix with the
   syntax <code>A.adjugate()</code>. The cofactor matrix is then given
   by <code>A.adjugate().T</code>.
<div class="compute">
<script type="text/x-sage">
A = matrix([(3, -7, 9, 4), (12, 14, -20, 5), (22, -22, 14, 22), (-11, 22, 15, 5)])

print(f'A=\n{A}\n')
print(f'C=\n{A.adjugate().T}\n')
print(f'adj(A)=\n{A.adjugate()}\n')
</script>
</div>
