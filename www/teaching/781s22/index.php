<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead() ?>

<h2>Schedule</h2>

<p>Quizzes can be found on gradescope.</p>

<?php
$fdoc = mktime(hour: 6, day: 3, month: 1, year: 2022);
$schedule = [
    [
        'debut' => strtotime("-4 week", $fdoc),
        'col1' => 'Week 0',
        'lecname' => 'Welcome!',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/course-policies/-/raw/master/781s22/welcome.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtu.be/-dEmbZsbvkI',
        ],
    ],
    [],
    [
        'debut' => strtotime("-4 week", $fdoc),
        'col1' => 'Week 1',
        'lecname' => 'Matrices and Vectors',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/vocab/vocab.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDVGsMw5PXJh0ZiFOs7gKOK',
            'sage' => './vocab-sage.php',
        ],
    ],
    [],
    [
        'debut' => strtotime("+1 week", $fdoc),
        'col1' => 'Week 2',
        'lecname' => 'Digraphs',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/digraphs/digraphs.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCCpUdsW05EcJkWyoMk1P1w2',
            'sage' => './digraphs-sage.php',
        ],
    ],
    [
        'lecname' => 'Vector Geometry',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/geometry/geometry.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDuuBiIIX9_jeyQX4Owkcra',
            'sage' => './geometry-sage.php',
        ],
    ],
    [],
    [
        'debut' => strtotime("+2 week", $fdoc),
        'col1' => 'Week 3',
        'lecname' => 'Matrix Multiplication',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/matmult/matmult.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDVZyAyLGsiIiBfgUjkNHbG',
            'sage' => './matmult-sage.php',
        ],
    ],
    [],
    [
        'debut' => strtotime("+3 week", $fdoc),
        'col1' => 'Week 4',
        'lecname' => 'Row Echelon Forms',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/rref/rref.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDNISmN_JY_iDB1093clEzZ',
            'sage' => './rref-sage.php',
        ],
    ],
    [
        'lecname' => 'Linear Systems',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/axb/axb.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDwlEY1XvpLt9piayYi-KsV',
            'sage' => './axb-sage.php',
            'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/axb/axb-ex.pdf?inline=true',
        ],
    ],
    [
        'lecname' => 'Gauss-Jordan Elimination',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/gj/gj.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCAQPajFz6SBVoGfJi7uDwN1',
            'sage' => './gj-sage.php',
            'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/gj/gj-examples.pdf?inline=true',
        ],
    ],
    [],
    [
        'debut' => strtotime("+4 week", $fdoc),
        'col1' => 'Week 5',
        'lecname' => 'Nonsingular Matrices',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/nonsing/nonsing.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCBEJAdI1OjxOYgMPr42W2aK',
            'sage' => './nonsing-sage.php',
        ],
    ],
    [
        'lecname' => 'EA=R Factorizations',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/ear/ear.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCBTvNlWm5TBy_662VwCjrd-',
            'sage' => './ear-sage.php',
        ],
    ],
    [
        'lecname' => 'PA=LU Factorizations',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/palu/palu.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCC8fkdt06abb3PoPsGFZGE_',
            'sage' => './palu-sage.php',
        ],
    ],
    [],
    [
        'debut' => strtotime("+5 week", $fdoc),
        'col1' => 'Week 6',
        'lecname' => 'Eigenvalues',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/evals/evals.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDRFYw5usU7O0CNxmxbMkKz',
            'sage' => './evals-sage.php',
        ],
    ],
    [
        'lecname' => 'Null Spaces',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/null/null.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDtTAi5KpGXH-eeUgRipRlU',
            'sage' => './null-sage.php',
        ],
    ],
    [

        'lecname' => 'Column Spaces',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/col/col.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCCEYLykkkV9fnt9cL3wemzI',
            'sage' => './col-sage.php',
        ],
    ],
    [],
    [
        'debut' => strtotime("+6 week", $fdoc),
        'col1' => 'Week 7',
        'lecname' => 'The Four Fundamental Subspaces',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/fundsub/fundsub.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCBLO0R2e0Ss2J9eDYp_VO_-',
            'sage' => './fundsub-sage.php',
        ],
    ],
    [
        'lecname' => 'Linear Independence',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/linind/linind.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDq3mdRoSQZkeZxTNKJO4vO',
            'sage' => './linind-sage.php',
        ],
    ],
    [
        'lecname' => 'Bases',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/bases/bases.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCBPCxI_yBV9yQ56aqLg7Tbg',
            'sage' => './bases-sage.php',
        ],
    ],
    [],
    [
        'debut' => strtotime("+7 week", $fdoc),
        'col1' => 'Week 8',
        'lecname' => 'Dimension',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/dim/dim.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDv25ifWyybsRNPI4uS0Phj',
            'sage' => './dim-sage.php',
        ],
    ],
    [
        'lecname' => 'Orthogonality',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/orthog/orthog.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCD7Lhh9l271DtnbiSQIDsXk',
            'sage' => './orthog-sage.php',
        ],
    ],
    [
        'lecname' => 'Projections',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/proj/proj.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCAgtrr5g8d6oR9U8fEphFgI',
            'sage' => './proj-sage.php',
        ],
    ],
    [],
    [
        'debut' => strtotime("+8 week", $fdoc),
        'col1' => 'Week 9',
        'lecname' => 'Least Squares',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/xhat/xhat.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCCbbYdcEFhOnazvDCBkU3Mr',
            'sage' => './xhat-sage.php',
        ],
    ],
    [
        'lecname' => 'A=QR Factorizations',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/aqr/aqr.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCD6Gpj_xfFSJ0FeJgvNAeZ9',
            'sage' => './aqr-sage.php',
        ],
    ],
    [
        'lecname' => 'The Gram-Schmidt Algorithm',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/gs/gs.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCCgVnzmc7GpajCxyYyqhRFa',
            'sage' => './gs-sage.php',
        ],
    ],
    [], /* SPRING BREAK */
    [
        'debut' => strtotime("+10 week", $fdoc),
        'col1' => 'Week 10',
        'lecname' => 'Determinants I',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/det1/det1.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDNySC8M3lT8qhXYIgEq1de',
            'sage' => './det1-sage.php',
        ],
    ],
    [
        'lecname' => 'Determinants II',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/det2/det2.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCCYDXbKfr21FC2rfDcs5ylV',
            'sage' => './det2-sage.php',
        ],
    ],
    [],
    [
        'debut' => strtotime("+11 week", $fdoc),
        'col1' => 'Week 11',
        'lecname' => 'Complex Numbers',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/clx/clx.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCASNHStI6lJFrnsrFHyJ0oF',
            'sage' => './clx-sage.php',
        ],
    ],
    [
        'lecname' => 'Polynomial Algebra',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/poly/poly.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCC2F9MY2i0IYSbB8_mIqWrW',
            'sage' => './poly-sage.php',
        ],
    ],
    [
        'lecname' => 'The Characteristic Polynomial',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/chi/chi.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDsS8d6cKcDXeEOwXZ-DSXf',
            'sage' => './chi-sage.php',
        ],
    ],
    [],
    [
        'debut' => strtotime("+12 week", $fdoc),
        'col1' => 'Week 12',
        'lecname' => 'Diagonalization',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/diag/diag.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCCJ3Ym-gfpi7jS66_grk2Bb',
            'sage' => './diag-sage.php',
        ],
    ],
    [
        'lecname' => 'The Spectral Theorem',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/spectral/spectral.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCC0g6fs8EAsE4RMyJ_L-Vmc',
            'sage' => './spectral-sage.php',
        ],
    ],
    [],
    [
        'debut' => strtotime("+13 week", $fdoc),
        'col1' => 'Week 13',
        'lecname' => 'Definiteness',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/posdef/posdef.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCAY-1mnoguKeJF-xe7vUP_e',
            'sage' => './posdef-sage.php',
            'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/posdef/posdef-ex.pdf?inline=true',
        ],
    ],
    [
        'lecname' => 'Singular Value Decomposition',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/svd/svd.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCBGtVOACD7lW9ejEMY8Tq8B',
            'sage' => './svd-sage.php',
            'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/svd/svd-ex.pdf?inline=true',
        ],
    ],
    [],
    [
        'debut' => strtotime("+14 week", $fdoc),
        'col1' => 'Week 14',
        'lecname' => 'Partial Derivatives',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/partial/partial.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCAHo2FLv4X5VFejOThbjJuf',
            'sage' => './partial-sage.php',
        ],
    ],
    [
        'lecname' => 'Linear Approximations',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/jacobian/jacobian.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCAncXe-cm8_Da5MtglnsLoQ',
            'sage' => './jacobian-sage.php',
        ],
    ],
    [],
    [
        'debut' => strtotime("+14 week", $fdoc),
        'col1' => 'Extra',
        'lecname' => 'Cholesky Factorizations',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/cholesky/cholesky.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCApkxXB-XIDhEDso4nDcO_f',
            'sage' => './cholesky-sage.php',
        ],
    ],
    [
        'lecname' => 'The Hessian',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/hessian/hessian.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDvM1pjbv0O5FPs2RAVMHPm',
            'sage' => './hessian-sage.php',
        ],
    ],
];
if (basename($_SERVER["SCRIPT_FILENAME"], '.php') == 'index') {
    mktbl($schedule);
} else {
    mktbl($schedule, rolling: false);
}
?>
