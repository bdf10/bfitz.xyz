<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead() ?>

<h2>Schedule</h2>

<p>Quizzes and problem sets are distributed through gradescope.</p>

<?php
$fdoc = mktime(hour: 6, day: 15, month: 5, year: 2023);
include($_SERVER['DOCUMENT_ROOT'].'/scripts/topics.php');
$schedule = [
    ['debut' => strtotime("-4 week", $fdoc),'col1' => 'Week 0'] +
    $welcome_218_su,
    [],
    ['debut' => strtotime("-4 week", $fdoc),'col1' => 'Week 1'] +
    $matvec_su,
    $adjectives_su + ['comment' => '<strong style="color:red"> ⟵ complete at home!</strong>'],
    $lincomb_su,
    $digraphs_su,
    $geometry_su,
    [],
    [
        'debut' => strtotime("-4 week", $fdoc),
        'col1' => date("D d-M", mktime(hour: 0, day: 24, month: 5, year: 2023)),
        'lecname' => 'Exam I',
        'comment' => '<strong style="color:red"> ⟵ covers week 1 material</strong>',
        'leclink' => './218su23-exam01.pdf',
        'icons' => [
            'info' => './exam01.php',
            'key' => './218su23-exam01-solutions.pdf'
        ],
    ],
    [],
    ['debut' => strtotime("+1 week", $fdoc),'col1' => 'Week 2'] +
    $matmult_su,
    $rref_su,
    $axb_su,
    $gj_su,
    $nonsing_su,
    $ear_su,
    [],
    [
        'debut' => strtotime("+1 week", $fdoc),
        'col1' => date("D d-M", mktime(hour: 0, day: 31, month: 5, year: 2023)),
        'lecname' => 'Exam II',
        'comment' => '<strong style="color:red"> ⟵ covers week 2 material</strong>',
        'leclink' => './218su23-exam02.pdf',
        'icons' => [
            'info' => './exam02.php',
            'key' => './218su23-exam02-solutions.pdf'
        ],
    ],
    [],
    ['debut' => strtotime("+2 week", $fdoc),'col1' => 'Week 3'] +
    $palu_su,
    $evals_su,
    $null_su,
    $col_su,
    $fundsub_su,
    $linind_su,
    [],
    [
        'debut' => strtotime("+2 week", $fdoc),
        'col1' => date("D d-M", mktime(hour: 0, day: 7, month: 6, year: 2023)),
        'lecname' => 'Exam III',
        'comment' => '<strong style="color:red"> ⟵ covers week 3 material</strong>',
        'leclink' => './218su23-exam03.pdf',
        'icons' => [
            'info' => './exam03.php',
            'key' => './218su23-exam03-solutions.pdf'
        ],
    ],
    [],
    ['debut' => strtotime("+3 week", $fdoc),'col1' => 'Week 4'] +
    $bases_su,
    $dim_su,
    $orthog_su,
    $proj_su,
    $xhat_su,
    $aqr_su,
    [],
    [
        'debut' => strtotime("+3 week", $fdoc),
        'col1' => date("D d-M", mktime(hour: 0, day: 14, month: 6, year: 2023)),
        'lecname' => 'Exam IV',
        'comment' => '<strong style="color:red"> ⟵ covers week 4 material</strong>',
        'leclink' => './218su23-exam04.pdf',
        'icons' => [
            'info' => './exam04.php',
            'key' => './218su23-exam04-solutions.pdf'
        ],
    ],
    [],
    ['debut' => strtotime("+4 week", $fdoc),'col1' => 'Week 5'] +
    $gs_su,
    $det1_su,
    $det2_su,
    $clx_su,
    $poly_su,
    $chi_su,
    [],
    [
        'debut' => strtotime("+4 week", $fdoc),
        'col1' => date("D d-M", mktime(hour: 0, day: 21, month: 6, year: 2023)),
        'lecname' => 'Exam V',
        'comment' => '<strong style="color:red"> ⟵ covers week 5 material</strong>',
        'leclink' => './218su23-exam05.pdf',
        'icons' => [
            'info' => './exam05.php',
            'key' => './218su23-exam05-solutions.pdf'
        ],
    ],
    [],
    ['debut' => strtotime("+4 week", $fdoc),'col1' => 'Week 6'] +
    $diag_su,
    $exp_su,
    $spectral_su,
    $posdef_su,
    $svd_su,
    /* $partial,
     * $jacobian,
     * $cholesky,
     * $hessian, */
    [],
    [
        'debut' => strtotime("+5 week", $fdoc),
        'col1' => date("D d-M", mktime(hour: 0, day: 2, month: 5, year: 2023)),
        'lecname' => 'Final Exam',
        'leclink' => './final.php',
        'comment' => '<strong style="color:red"> ⟵ click for info!</strong>',
    ],
];
if (basename($_SERVER["SCRIPT_FILENAME"], '.php') == 'index') {
    mktbl($schedule);
} else {
    mktbl($schedule, rolling: false);
}
?>
