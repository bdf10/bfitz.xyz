<?php
return [
    'h1' => 'Math 218: Matrices and Vectors',
    'icons' => [
        ['./', 'schedule', 'list.svg'],
        ['./cal.php', 'calendar', 'calendar.svg'],
        ['https://duke.zoom.us/j/95585011098?pwd=a3hEdDhQdWJxQWFlRngrWVpiOGJaUT09', 'Brian\'s Zoom', 'B.png'],
        ['https://urldefense.com/v3/__https://duke.zoom.us/j/2313347646__;!!OToaGQ!oOhm_HHfYVGQsgibdGQQ09jsC2ghQexR6Re9cCaemj9WxRjDHYsCvDnxisp-SvYuYHOk9kRMM03HYxfXKtH43URBdc-dAQgE$', 'Michael\'s Zoom', 'M.png'],
        ['https://urldefense.com/v3/__https://duke.zoom.us/j/7556276857__;!!OToaGQ!sNb9s3JKJN2fT11ZKqk4U8KZ2F1y6d5Jt70AhusuKpf6_TLlOeFwZp51Y94CP-rbDJ_U7oebfP0jlI9jSHZaAQs5H-AgtdJggjY$', 'Rodrigo\'s Zoom', 'R.png'],
        ['https://gradescope.com', 'gradescope', 'gradescope.png'],
        ['https://gitlab.oit.duke.edu/bdf10/course-policies/-/raw/master/218su23/218su23-policies.pdf', 'policies', 'scroll.svg'],
        ['./exams.php', 'exams', 'folder.svg'],
    ],
    'msg' => "<p>Welcome! You have found the homepage of the Summer 2023 manifestation of Math 218.</p>"
];
?>
