<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Projections</h2>
<p>The following code will calculate the projection matrix onto a
   space <code>V</code> after the user inputs a basis of <code>V</code>.
<div class="compute">
<script type="text/x-sage">
basis_of_V = [(1, -1, -1, 1), (1, 0, -2, 3), (-1, 7, -2, 2)]

X = matrix.column(basis_of_V)
P = X*(X.T*X).inverse()*X.T

print(f'V has basis given by the columns of:\n{X}\n')
print(f'The projection matrix onto V is P_V=\n{P}')
</script>
</div>

<p>To project a vector <code>v</code> onto <code>V</code>, we use the
   matrix-vector product <code>P*v</code>.
<div class="compute">
<script type="text/x-sage">
basis_of_V = [(1, -1, -1, 1), (1, 0, -2, 3), (-1, 7, -2, 2)]
v = vector([4, 5, 14, 1])

X = matrix.column(basis_of_V)
P = X*(X.T*X).inverse()*X.T

print(f'V has basis given by the columns of:\n{X}\n')
print(f'The projection matrix onto V is P_V=\n{P}\n')
print(f'The projection of v={v} onto V is P_V*v={P*v}')
</script>
</div>

<p>The following code will take a matrix <code>A</code> and print data
   describing its four fundamental subspaces, including the projection matrices
   onto each subspace.
<div class="compute">
<script type="text/x-sage">
A = matrix([(1, -5, 3, -5), (0, 0, 1, -2), (-5, 25, -16, 27)])

print(f'A=\n{A}\n')
print(f'rref(A)=\n{A.rref()}\n')
print(f'rref(A^T)=\n{A.T.rref()}\n')

print(f'Col(A)')
print(f'------')
print(f'pivot columns of A: {[v for i, v in enumerate(A.T) if i in A.pivots()]}')
print(f'nonzero rows of rref(A^T): {list(filter(bool, A.T.rref()))}')
print(f'dim Col(A): {A.rank()}')
X = matrix.column([v for i, v in enumerate(A.T) if i in A.pivots()])
P = X*(X.T*X).inverse()*X.T if X else zero_matrix(A.nrows())
print(f'projection matrix onto Col(A):\n{P}\n')

print(f'Col(A^T)')
print(f'--------')
print(f'pivot columns of A^T: {[v for i, v in enumerate(A) if i in A.T.pivots()]}')
print(f'nonzero rows of rref(A): {list(filter(bool, A.rref()))}')
print(f'dim Col(A^T): {A.rank()}')
X = matrix.column([v for i, v in enumerate(A) if i in A.T.pivots()])
P = X*(X.T*X).inverse()*X.T if X else zero_matrix(A.ncols())
print(f'projection matrix onto Col(A^T):\n{P}\n')


print(f'Null(A)')
print(f'-------')
print(f"pivot solutions to Av=0: {list(A.change_ring(QQ).right_kernel(basis='pivot').basis())}")
print(f'dim Null(A): {A.right_nullity()}')
X = A.right_kernel().basis_matrix().T
P = X*(X.T*X).inverse()*X.T if X else zero_matrix(A.ncols())
print(f'projection matrix onto Null(A):\n{P}\n')

print(f'Null(A^T)')
print(f'---------')
print(f"pivot solutions to A^Tv=0: {list(A.T.change_ring(QQ).right_kernel(basis='pivot').basis())}")
print(f'dim Null(A^T): {A.left_nullity()}')
X = A.left_kernel().basis_matrix().T
P = X*(X.T*X).inverse()*X.T if X else zero_matrix(A.nrows())
print(f'projection matrix onto Null(A^T):\n{P}')
</script>
</div>
