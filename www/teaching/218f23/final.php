<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead() ?>

<h2>Final Exam</h2>

<p> The Final Exam is scheduled to take place on <code>Thursday 14-December</code> from
    <code>9:00 AM to 12:00 PM</code>.</p>

<p> You will either take the final exam in <code>Gross Hall 107</code> or <code>Gross Hall 103</code>,
    depending on which discussion section you are enrolled in. Please verify your discussion number on DukeHub.</p>

<ul>
    <li><code>Gross Hall 107</code></li>
    <ul>
        <li>Math 218D-2-01D (Yupeng Li)</li>
        <li>Math 218D-2-02D (Yupeng Li)</li>
        <li>Math 218D-2-03D (Yuqing Dai)</li>
        <li>Math 218D-2-04D (Yuqing Dai)</li>
        <li>Math 218D-2-05D (George Daccache)</li>
        <li>Math 218D-2-06D (George Daccache)</li>
        <li>Math 218D-2-09D (Shijun Zhang)</li>
        <li>Math 218D-2-10D (Shijun Zhang)</li>
    </ul>
    <li><code>Gross Hall 103</code></li>
    <ul>
        <li>MATH 218D-2.07D (Benjamin Arora)</li>
        <li>MATH 218D-2.08D (Benjamin Arora)</li>
    </ul>
</ul>

<p> So, if Benjamin Arora is your discussion instructor, then you will take your
    final exam in Gross Hall 103. Everyone else will take their final exam in Gross
    Hall 107.</p>

<p> The exam will consist of mostly free-response problems with a few fill in
    the blank and multiple choice questions. You should be prepared to solve
    problems that you have not seen before.</p>

<p> The exam is <code>cumulative</code>, so you should be prepared for every
    topic covered throughout the semester to be represented on the exam.</p>

<p> The exam will be "closed" in the sense that no outside aid (like books or
    calculators) will be allowed. The only thing you need to bring is a writing
    utensil.</p>

<p> The schedule of office hours leading up to the final will be:</p>
<ul>
    <li>Brian's Office Hours</li>
    <ul>
        <li>Monday 11-Dec 09:15am-11:15am (Zoom)</li>
        <li>Tuesday 12-Dec 09:15am-11:15am (Zoom)</li>
        <li>Wed 13-Dec 09:15am-11:15am (Zoom)</li>
        <li>Wed 13-Dec 02:00pm-04:00pm (Zoom)</li>
    </ul>
    <li>Yupeng's Office Hours</li>
    <ul>
        <li>Monday 11-Dec 05:30pm-08:30pm (Gross Hall 352)</li>
    </ul>
    <li>Yuqing's Office Hours</li>
    <ul>
        <li>Sunday 10-Dec 10:00am-01:00pm (Gross Hall 352)</li>
    </ul>
    <li>George's Office Hours</li>
    <ul>
        <li>Tuesday 12-Dec 10:30am-01:30pm (Gross Hall 352)</li>
    </ul>
    <li>Benjamin's Office Hours</li>
    <ul>
        <li>Tuesday 12-Dec 05:00pm-08:00pm (Physics 274J)</li>
    </ul>
    <li>Shijun's Office Hours</li>
    <ul>
        <li>Friday 08-Dec 05:00pm-08:00pm (Physics 034)</li>
    </ul>
    <li>Blake's Office Hours</li>
    <ul>
        <li>Monday 11-Dec 07:00pm-10:00pm (Physics 274J)</li>
    </ul>
</ul>

<p> To prepare for the exam, you should be able to:</p>

<ul>
    <li>Correctly articulate all definitions and theorems from lecture.</li>
    <li>Clearly and coherently solve all examples from lecture.</li>
    <li>Write correct and legible solutions to all problems from the midterm
        exams, problem sets, and discussion worksheets.</li>
    <li>Clearly explain how to solve each comprehension quiz problem.</li>
</ul>
