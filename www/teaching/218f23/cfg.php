<?php
return [
    'h1' => 'Math 218: Matrices and Vectors',
    'icons' => [
        ['./', 'schedule', 'list.svg'],
        ['./cal.php', 'calendar', 'calendar.svg'],
        ['https://duke.zoom.us/j/91312814296?pwd=Umh3aFNYb0JWbG9FZFhmZ0xuc3VLQT09', 'Brian\'s Zoom', 'zoom.svg'],
        ['https://gradescope.com', 'gradescope', 'gradescope.png'],
        ['https://gitlab.oit.duke.edu/bdf10/course-policies/-/raw/master/218f23/218f23-policies.pdf', 'policies', 'scroll.svg'],
        ['./exams.php', 'exams', 'folder.svg'],
    ],
    'msg' => "<p>Welcome! You have found the homepage of the Fall 2023 manifestation of Math 218.</p>"
];
?>
