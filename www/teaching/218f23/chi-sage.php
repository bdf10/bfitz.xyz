<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>The Characteristic Polynomial</h2>
<p>We can calculate the characteristic polynomial of a matrix with the
   syntax <code>A.characteristic_polynomial('t')</code>.
<div class="compute">
<script type="text/x-sage">
A = matrix([(1, -2, 3, 8), (4, -9, 15, 3), (3, 0, -5, 4), (1, 9, 2, -7)])

print(f'A = \n{A}\n')
print(f"chi_A(t) = {A.characteristic_polynomial('t')}")
</script>
</div>

<p>If the eigenvalues of <code>A</code> are nice enough, then we can factor the
   characteristic polynomial with the
   syntax <code>factor(A.characteristic_polynomial('t'))</code>.
<div class="compute">
<script type="text/x-sage">
A = matrix([(-7, -29, -9, -16, -32, 0, 71, -44, 149), (-32, -13, -14, -48, 0, 16, 290, -24, 102), (-8, -52, -30, 0, -64, -32, -28, 44, 282), (-68, 24, -71, -71, 48, -32, 460, 276, 134), (20, 35, -25, 48, 25, -64, -269, 236, -75), (0, 0, 0, 0, 0, -7, 0, 0, 0), (-12, -3, -9, -16, 0, 0, 94, 12, 47), (0, -13, 0, 0, -16, 0, -9, -23, 51), (-4, 0, -13, 0, 0, -16, 4, 60, 35)])

print(f'A = \n{A}\n')
print(f"chi_A(t) = {factor(A.characteristic_polynomial('t'))}")
</script>
</div>
