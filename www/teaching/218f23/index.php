<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead() ?>

<h2>Schedule</h2>

<p>Quizzes and problem sets are distributed through gradescope.</p>

<?php
$fdoc = mktime(hour: 6, day: 28, month: 8, year: 2023);
include($_SERVER['DOCUMENT_ROOT'].'/scripts/topics.php');
$schedule = [
    ['debut' => strtotime("-4 week", $fdoc),'col1' => 'Week 0'] +
    $welcome_218,
    [],
    ['debut' => strtotime("-4 week", $fdoc),'col1' => 'Week 1'] +
    $matvec,
    $adjectives + ['comment' => '<strong style="color:red"> ⟵ complete at home! (w/quiz but no pset)</strong>'],
    $lincomb,
    $digraphs,
    [],
    ['debut' => strtotime("+1 week 1 days", $fdoc),'col1' => 'Week 2'] +
    $geometry,
    $matmult,
    [],
    ['debut' => strtotime("+2 week", $fdoc),'col1' => 'Week 3'] +
    $rref,
    $axb,
    $gj,
    [],
    ['debut' => strtotime("+3 week", $fdoc),'col1' => 'Week 4'] +
    $nonsing,
    $ear,
    $palu,
    [],
    [
        'debut' => strtotime("+3 week", $fdoc),
        'col1' => date("D d-M", mktime(hour: 0, day: 29, month: 9, year: 2023)),
        'lecname' => 'Exam I',
        'comment' => '<strong style="color:red"> ⟵ covers weeks 1-4</strong>',
        'leclink' => './218f23-exam01.pdf',
        /* 'leclink' => './exam01.php', */
        'icons' => [
            'info' => './exam01.php',
            'key' => './218f23-exam01-solutions.pdf'
        ],
    ],
    [],
    ['debut' => strtotime("+4 week", $fdoc),'col1' => 'Week 5'] +
    $evals,
    $null,
    [],
    ['debut' => strtotime("+5 week", $fdoc),'col1' => 'Week 6'] +
    $col,
    $fundsub,
    $linind,
    [],
    ['debut' => strtotime("+6 week", $fdoc),'col1' => 'Week 7'] +
    $bases,
    $dim,
    $orthog,
    [],
    ['debut' => strtotime("+7 week", $fdoc),'col1' => 'Week 8'] +
    $proj,
    $xhat,
    [],
    [
        'debut' => strtotime("+7 week", $fdoc),
        'col1' => date("D d-M", mktime(hour: 0, day: 27, month: 10, year: 2023)),
        'lecname' => 'Exam II',
        'comment' => '<strong style="color:red"> ⟵ covers weeks 5-8</strong>',
        /* 'leclink' => './exam02.php', */
        'leclink' => './218f23-exam02.pdf',
        'icons' => [
            'info' => './exam02.php',
            'key' => './218f23-exam02-solutions.pdf'
        ],
    ],
    [],
    ['debut' => strtotime("+8 week", $fdoc),'col1' => 'Week 9'] +
    $aqr,
    $gs,
    [],
    ['debut' => strtotime("+9 week", $fdoc),'col1' => 'Week 10'] +
    $det1,
    $det2,
    $clx,
    [],
    ['debut' => strtotime("+10 week", $fdoc),'col1' => 'Week 11'] +
    $poly,
    $chi,
    $diag,
    [],
    ['debut' => strtotime("+11 week", $fdoc),'col1' => 'Week 12'] +
    $exp,
    $spectral,
    $posdef,
    [],
    ['debut' => strtotime("+12 week", $fdoc),'col1' => 'Week 13'] +
    $partial,
    [],
    [
        'debut' => strtotime("+12 week", $fdoc),
        'col1' => date("D d-M", mktime(hour: 0, day: 1, month: 12, year: 2023)),
        'lecname' => 'Exam III',
        'comment' => '<strong style="color:red"> ⟵ covers weeks 9-13</strong>',
        'leclink' => './218f23-exam03.pdf',
        /* 'leclink' => './exam03.php', */
        'icons' => [
            'info' => './exam03.php',
            'key' => './218f23-exam03-solutions.pdf',
        ],
    ],
    [],
    ['debut' => strtotime("+13 week", $fdoc),'col1' => 'Week 14'] +
    $svd,
    [],
    ['debut' => strtotime("+14 week", $fdoc),'col1' => 'Week 15'] +
    $jacobian,
    $cholesky,
    $hessian,
    [],
    [
        'debut' => strtotime("+14 week", $fdoc),
        'col1' => date("D d-M", mktime(hour: 0, day: 14, month: 12, year: 2023)),
        'lecname' => 'Final Exam',
        'leclink' => './final.php',
        'comment' => '<strong style="color:red"> ⟵ click for info!</strong>',
    ],
];
if (basename($_SERVER["SCRIPT_FILENAME"], '.php') == 'index') {
    mktbl($schedule);
} else {
    mktbl($schedule, rolling: false);
}
?>
