<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>PA=LU Factorizations</h2>
<p>The following code will show the steps in calculating <code>PA=LU</code> using the algorithm from
    class. At each step, the current data is displayed in the form <code>[U|L|P]</code>.</p>

<?php
sagecell(<<<'EOF'
A = matrix([(2, -7, 8, -9, -17), (-6, 21, -24, 27, 59), (10, -24, 5, -29, -34), (12, 2, -92, 15, 87)])


elem = elementary_matrix

def row_swap(i, i0, n):
    st = f'r{i+1} <-> r{i0+1}'
    E = elem(n, row1=i, row2=i0)
    return st, E


def row_add(d, p, n):
    def pretty_int(x):
        if x == 1:
            return '+'
        if x == -1:
            return '-'
        if x < 0:
            return f'- {abs(x)} *'
        return f'+ {x} *'
    st = '\n'.join([f'r{i+1} {pretty_int(-m/d[p])} r{p+1} -> r{i+1}' for i, m in sorted(d.items()) if i > p])
    E = prod([elem(n, row1=i, row2=p, scale=-m/d[p]) for i, m in d.items() if i > p])
    return st, E


def print_step(st, A, L, P):
    print('\n'.join([st, A.augment(L, subdivide=True).augment(P, subdivide=True).__repr__()]))
    print('\n')


def palu(A):
    i = j = 0
    m, n = A.dimensions()
    P = identity_matrix(m)
    L = zero_matrix(m)
    print(A)
    print('\n')
    while i < m-1 and j < n-1:
        if A[i, j] == 0:
            try:
                i0 = min(filter(lambda x: x > i, A.T[j].dict()))
                st, E = row_swap(i, i0, m)
                A = E*A
                L = E*L
                P = E*P
                print_step(st, A, L, P)
            except ValueError:
                j += 1
        else:
            st, E = row_add(A.T[j].dict(), i, m)
            if E != 1:
                A = E*A
                L += E.inverse()-identity_matrix(L.nrows())
                print_step(st, A, L, P)
            i += 1
            j += 1

palu(A)
EOF)
?>
