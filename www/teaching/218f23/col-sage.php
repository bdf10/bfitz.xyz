<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead(sage: true) ?>

<h2>Column Spaces</h2>
<p>To check if <code>v</code> is in the column space of a matrix <code>A</code>,
   we can calculate <code>A.augment(v, subdivide=True).rref()</code> and look
   for a pivot in the augmented column.
<div class="compute">
<script type="text/x-sage">
A = matrix([(1, -2, -2, -9, -7), (-1, 1, 0, 3, 2), (-5, -5, -9, -23, -18), (0, 2, 3, 10, 8)])
v = vector([4, -3, 1, 2])

system = A.augment(v, subdivide=True)

print(f'[A | v] =\n{system}\n')
print(f'rref[A | v] =\n{system.rref()}\n')
</script>
</div>

<p>More efficiently, we can use the Rouché–Capelli Theorem and directly
   check <code>A.rank() == A.augment(v).rank()</code>.
<div class="compute">
<script type="text/x-sage">
A = matrix([(1, -2, -2, -9, -7), (-1, 1, 0, 3, 2), (-5, -5, -9, -23, -18), (0, 2, 3, 10, 8)])
v = vector([4, -3, 1, 2])

print(f'A =\n{A}\n')
print(f'v = {v}\n')
print(f'Is v in Col(A)? {A.rank() == A.augment(v).rank()}')
</script>
</div>
