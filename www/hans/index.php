<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead('Hans and Erin\'s Wedding') ?>

<h2>Welcome and Intro (Brian, 5m)</h2>
<p> As you all know, we are here today to celebrate the marriage of Hans and
    Erin. I'm not typically one for sentimentality, but it's hard to stand here by
    this lake and not feel something tug at my emotions. It's not just the scenery
    though--it's the love we all have for these individuals, who to you are
    family members and to me are two of my best friends.</p>

<p> As you all also know, we're doing this in Minnesota. But this relationship very
    much roots itself in Durham. In fact, in the early days of the courting process
    it felt like they had a whole city rooting for them. I was lucky enough to have
    a front-row seat. I feel even luckier now to have a front row seat to this
    ceremony. I guess we all should feel lucky, since we all have front row seats
    haha.</p>

<p> It's hard to say exactly, but I probably met Hans and Erin around the same time,
    about ten years ago. Erin was an employee at a neighborhood spot that Hans and I
    frequented. Even back then I'm pretty sure Hans had some romantic aspirations
    directed toward Erin. I think there's even an old story of Hans confiding this
    to John Conway, who responded "yeah man, she's got it going on."</p>

<p> Well, I not only feel comfortable speaking for all of you today but also the
    entire city of Durham when I say that they've both got it going on today.</p>

<p> This is such an exciting moment in Hans' and Erin's lives. Back home they have a
    new house, and a new career to look forward to building together. Right now
    though, I think we should all savor this more fleeting moment as they affirm
    their love for one another.</p>

<p> Now, Erin would like to begin the ceremony by saying a few words to the Arneson
    family.</p>

<h2>Erin Addresses Arnesons (Erin, 5m)</h2>

<code>Erin says stuff.</code>

<p>Next, Hans would like to address the Tuckman Family.</p>

<h2>Hans Addresses Tuckmans (Hans, 5m)</h2>

<code>Hans says stuff.</code>

<p> I'd like to say something now.</p>

<h2>Brian Story (Brian, 5m)</h2>

<p> It was seven or eight years ago that I really started to bond with Hans on a
    very regular basis. When we first started hanging out, I would spend much of
    my week looking forward to meeting up with him on Friday night and picking
    his brain about nearly any topic. A few years went by and eventually Hans
    says "Hey man, you remember Erin that girl from the back bar at Bull
    Mccabbes? She's been on this epic trip and coming back to Durham." I think
    he was excited just to be in her presence again.</p>

<p> Of course, as Hans and Erin began to hang out, Erin and I found ourselves
    hanging out. In fact, I pretty quickly realized that in a lot of ways I have
    more in common with Erin than I do with Hans, so it's cool that I get to be
    friends with both.</p>

<p> Anyway, four summers ago my wife got pregnant. The thought of having a baby
    really terrified me, but both Hans and Erin were really there to psych me up for
    fatherhood. I was actually excited driving my wife to the hospital for her first
    ultrasound. I'll never forget it. The ultrasound lady put that big thing on
    Sarah's stomach and immediately said "there's two!". This was such a shocking
    reveal to both of us. All the confidence Hans and Erin built into me more or
    less evaporated in the span of a few seconds.</p>

<p> I went through the rest of that day in a haze. After I got off work, I
    headed to the bar where I knew Hans and Erin would be working later. Erin showed
    up before Hans. When I told Erin that Sarah was pregnant with twins her reaction
    was one of "empathetic excitement." "You haven't told Hans yet, have you?" No.</p>

<p> As soon as Hans shows up, Erin says "Brian has something to tell you." Huh?
    "Had the first ultrasound today. It's twins." He immediately smiles and gives me
    a big hug. "It's going to be ok man. We'll get through this. You're gonna be a
    great dad." This was what I needed. Friends to comfort me in a moment of panic.</p>

<p> This was all before I even talked with anyone in my own family. Other than
    my wife, Hans and Erin have seen me at my worst more than any other person. Yet,
    they continue to reach out in their own ways to listen and laugh with me. I'm
    more than lucky to have them as friends. I was too busy thinking about how to
    become mayor of Grand Marais last night and forgot to ask Melissa for an
    eloquent way to say that.</p>

<p> Maybe just this once I'll cave in and say I feel blessed. After hanging out
    with all of you last night, I know everyone here feels the same.</p>

<p> Ok, so I've been vulnerable enough for one day. Now, Erin would like to
    express her vows to Hans.</p>

<h2>Erin Vows (Erin, 5m)</h2>

<code>Erin says stuff.</code>

<p> Next, Hans would like to express his vows to Erin.</p>

<h2>Hans Vows (Hans, 5m)</h2>

<code>Hans says stuff.</code>

<h2>Exchange Rings/Kiss (2m)</h2>

<p> Leif and Solveig, please bring the rings.</p>

<p> Erin, please take Hans' ring. Erin: with this ring, do you take Hans to be your husband?</p>

<p> Hans, please take Erin's ring. Hans: with this ring, do you take Erin to be your wife?</p>

<p> You may now kiss the bride.</p>

<p> I now pronounce you husband and wife.</p>
