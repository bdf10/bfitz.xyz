<?php
$welcome_218 = [
    'lecname' => 'Welcome!',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/welcome/terms/s25/218s25-welcome.pdf',
    'icons' => [
        'youtube' => 'https://youtu.be/Xf3RZAySBIo',
        'ios' => 'https://apps.apple.com/us/app/gradescope/id1563280912',
        'android' => 'https://play.google.com/store/search?q=gradescope&c=apps&hl=en_US&gl=US',
    ],
];
$welcome_781 = [
    'lecname' => 'Welcome!',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/course-policies/-/raw/master/781s25/welcome.pdf',
];
$matvec = [
    'lecname' => 'Matrices and Vectors',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/matvec/matvec.pdf',
    'icons' => [
        'youtube' => 'https://youtu.be/3uNbT8X7zGo',
        'sage' => './matvec-sage.php',
    ],
];
$adjectives = [
    'lecname' => 'Adjectives',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/adjectives/adjectives.pdf',
    'icons' => [
        'youtube' => 'https://youtu.be/ywohncL9Qm0',
        'sage' => './adjectives-sage.php',
    ],
];
$lincomb = [
    'lecname' => 'Matrix-Vector Products',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/lincomb/lincomb.pdf',
    'icons' => [
        'youtube' => 'https://youtu.be/-095IM1GU5U',
        'sage' => './lincomb-sage.php',
    ],
];
$digraphs = [
    'lecname' => 'Digraphs',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/digraphs/digraphs.pdf',
    'icons' => [
        'youtube' => 'https://youtu.be/rWbcj17JuWA',
        'sage' => './digraphs-sage.php',
    ],
];
$geometry = [
    'lecname' => 'Vector Geometry',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/geometry/geometry.pdf',
    'icons' => [
        'youtube' => 'https://youtu.be/HAgVUqbnBWM',
        'sage' => './geometry-sage.php',
    ],
];
$matmult = [
    'lecname' => 'Matrix Multiplication',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/matmult/matmult.pdf',
    'icons' => [
        'youtube' => 'https://youtu.be/4_3z7DE4580',
        'sage' => './matmult-sage.php',
    ],
];
$rref = [
    'lecname' => 'Row Echelon Forms',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/rref/rref.pdf',
    'icons' => [
        'youtube' => 'https://youtu.be/SouZy61TGIY',
        'sage' => './rref-sage.php',
    ],
];
$axb = [
    'lecname' => 'Linear Systems',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/axb/axb.pdf',
    'icons' => [
        'youtube' => 'https://youtu.be/yIUmtIwh37Y',
        'sage' => './axb-sage.php',
        'notes' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/axb/axb-ex.pdf',
    ],
];
$gj = [
    'lecname' => 'Gauss-Jordan Elimination',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/gj/gj.pdf',
    'icons' => [
        'youtube' => 'https://youtu.be/LUQHT6OniTY',
        'sage' => './gj-sage.php',
        'notes' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/gj/gj-examples.pdf',
    ],
];
$nonsing = [
    'lecname' => 'Nonsingular Matrices',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/nonsing/nonsing.pdf',
    'icons' => [
        'youtube' => 'https://youtu.be/GCc2_G8TK0s',
        'sage' => './nonsing-sage.php',
    ],
];
$ear = [
    'lecname' => 'EA=R Factorizations',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/ear/ear.pdf',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCBTvNlWm5TBy_662VwCjrd-',
        'sage' => './ear-sage.php',
    ],
];
$palu = [
    'lecname' => 'PA=LU Factorizations',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/palu/palu.pdf',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCC8fkdt06abb3PoPsGFZGE_',
        'sage' => './palu-sage.php',
    ],
];
$evals = [
    'lecname' => 'Eigenvalues',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/evals/evals.pdf',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDRFYw5usU7O0CNxmxbMkKz',
        'sage' => './evals-sage.php',
    ],
];
$null = [

    'lecname' => 'Null Spaces',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/null/null.pdf',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDtTAi5KpGXH-eeUgRipRlU',
        'sage' => './null-sage.php',
    ],
];
$col = [
    'lecname' => 'Column Spaces',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/col/col.pdf',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCCEYLykkkV9fnt9cL3wemzI',
        'sage' => './col-sage.php',
    ],
];
$fundsub = [
    'lecname' => 'The Four Fundamental Subspaces',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/fundsub/fundsub.pdf',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCBLO0R2e0Ss2J9eDYp_VO_-',
        'sage' => './fundsub-sage.php',
    ],
];
$linind = [
    'lecname' => 'Linear Independence',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/linind/linind.pdf',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDq3mdRoSQZkeZxTNKJO4vO',
        'sage' => './linind-sage.php',
    ],
];
$bases = [
    'lecname' => 'Bases',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/bases/bases.pdf',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCBPCxI_yBV9yQ56aqLg7Tbg',
        'sage' => './bases-sage.php',
    ],
];
$dim = [

    'lecname' => 'Dimension',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/dim/dim.pdf',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDv25ifWyybsRNPI4uS0Phj',
        'sage' => './dim-sage.php',
    ],
];
$orthog = [
    'lecname' => 'Orthogonality',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/orthog/orthog.pdf',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCD7Lhh9l271DtnbiSQIDsXk',
        'sage' => './orthog-sage.php',
    ],
];
$proj = [
    'lecname' => 'Projections',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/proj/proj.pdf',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCAgtrr5g8d6oR9U8fEphFgI',
        'sage' => './proj-sage.php',
    ],
];
$xhat = [
    'lecname' => 'Least Squares',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/xhat/xhat.pdf',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCCbbYdcEFhOnazvDCBkU3Mr',
        'sage' => './xhat-sage.php',
    ],
];
$aqr = [
    'lecname' => 'A=QR Factorizations',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/aqr/aqr.pdf',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCD6Gpj_xfFSJ0FeJgvNAeZ9',
        'sage' => './aqr-sage.php',
    ],
];
$gs = [
    'lecname' => 'The Gram-Schmidt Algorithm',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/gs/gs.pdf',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCCgVnzmc7GpajCxyYyqhRFa',
        'sage' => './gs-sage.php',
    ],
];
$det1 = [
    'lecname' => 'Determinants I',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/det1/det1.pdf',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDNySC8M3lT8qhXYIgEq1de',
        'sage' => './det1-sage.php',
    ],
];
$det2 = [
    'lecname' => 'Determinants II',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/det2/det2.pdf',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCCYDXbKfr21FC2rfDcs5ylV',
        'sage' => './det2-sage.php',
    ],
];
$clx = [
    'lecname' => 'Complex Numbers',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/clx/clx.pdf',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCASNHStI6lJFrnsrFHyJ0oF',
        'sage' => './clx-sage.php',
    ],
];
$poly = [
    'lecname' => 'Polynomial Algebra',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/poly/poly.pdf',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCC2F9MY2i0IYSbB8_mIqWrW',
        'sage' => './poly-sage.php',
    ],
];
$chi = [
    'lecname' => 'The Characteristic Polynomial',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/chi/chi.pdf',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDsS8d6cKcDXeEOwXZ-DSXf',
        'sage' => './chi-sage.php',
    ],
];
$diag = [
    'lecname' => 'Diagonalization',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/diag/diag.pdf',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCCJ3Ym-gfpi7jS66_grk2Bb',
        'sage' => './diag-sage.php',
    ],
];
$exp = [
    'lecname' => 'Matrix Exponentials',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/exp/exp.pdf',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDIB1xn7WUZNEaefgxM0Jby',
        'sage' => './exp-sage.php',
        'notes' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/exp/exp-ex.pdf',
    ],
];
$spectral = [
    'lecname' => 'The Spectral Theorem',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/spectral/spectral.pdf',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCC0g6fs8EAsE4RMyJ_L-Vmc',
        'sage' => './spectral-sage.php',
    ],
];
$posdef = [
    'lecname' => 'Definiteness',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/posdef/posdef.pdf',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCAY-1mnoguKeJF-xe7vUP_e',
        'sage' => './posdef-sage.php',
        'notes' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/posdef/posdef-ex.pdf',
    ],
];
$svd = [
    'lecname' => 'Singular Value Decomposition',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/svd/svd.pdf',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCBGtVOACD7lW9ejEMY8Tq8B',
        'sage' => './svd-sage.php',
        'notes' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/svd/svd-ex.pdf',
    ],
];
$partial = [
    'lecname' => 'Partial Derivatives',
    'comment' => '<strong style="color:red"> ⟵ (w/quiz but no pset)</strong>',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/partial/partial.pdf',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCAHo2FLv4X5VFejOThbjJuf',
        'sage' => './partial-sage.php',
    ],
];
$jacobian = [
    'lecname' => 'Linear Approximations',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/jacobian/jacobian.pdf',
    'comment' => '<strong style="color:red"> ⟵ pset not collected!</strong>',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCAncXe-cm8_Da5MtglnsLoQ',
        'sage' => './jacobian-sage.php',
        'memo' => './jacobian-hw.pdf',
    ],
];
$cholesky = [
    'lecname' => 'Cholesky Factorizations',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/cholesky/cholesky.pdf',
    'comment' => '<strong style="color:red"> ⟵ pset not collected!</strong>',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCApkxXB-XIDhEDso4nDcO_f',
        'sage' => './cholesky-sage.php',
        'memo' => './cholesky-hw.pdf',
    ],
];
$hessian = [
    'lecname' => 'The Hessian',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/hessian/hessian.pdf',
    'comment' => '<strong style="color:red"> ⟵ pset not collected!</strong>',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDvM1pjbv0O5FPs2RAVMHPm',
        'sage' => './hessian-sage.php',
        'memo' => './hessian-hw.pdf',
    ],
];
$sb25 = [
    'lecname' => 'Spring Break!',
    'leclink' => 'https://math.duke.edu',
];
$welcome_218_su = [
    'lecname' => 'Welcome!',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/welcome/terms/su24t2/218su24t2-welcome.pdf',
    /* 'icons' => [
     *     'youtube' => 'https://youtu.be/VI2ASiw4GTw',
     * ], */
];
$matvec_su = [
    'lecname' => 'Matrices and Vectors',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/matvec/matvec.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/matvec-hw.pdf',
        'youtube' => 'https://youtu.be/3uNbT8X7zGo',
        'sage' => './matvec-sage.php',
    ],
];
$adjectives_su = [
    'lecname' => 'Adjectives',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/adjectives/adjectives.pdf',
    'icons' => [
        'youtube' => 'https://youtu.be/ywohncL9Qm0',
        'sage' => './adjectives-sage.php',
    ],
];
$lincomb_su = [
    'lecname' => 'Matrix-Vector Products',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/lincomb/lincomb.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/lincomb-hw.pdf',
        'youtube' => 'https://youtu.be/-095IM1GU5U',
        'sage' => './lincomb-sage.php',
    ],
];
$digraphs_su = [
    'lecname' => 'Digraphs',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/digraphs/digraphs.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/digraphs-hw.pdf',
        'youtube' => 'https://youtu.be/rWbcj17JuWA',
        'sage' => './digraphs-sage.php',
    ],
];
$geometry_su = [
    'lecname' => 'Vector Geometry',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/geometry/geometry.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/geometry-hw.pdf',
        'youtube' => 'https://youtu.be/HAgVUqbnBWM',
        'sage' => './geometry-sage.php',
    ],
];
$matmult_su = [
    'lecname' => 'Matrix Multiplication',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/matmult/matmult.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/matmult-hw.pdf',
        'youtube' => 'https://youtu.be/4_3z7DE4580',
        'sage' => './matmult-sage.php',
    ],
];
$rref_su = [
    'lecname' => 'Row Echelon Forms',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/rref/rref.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/rref-hw.pdf',
        'youtube' => 'https://youtu.be/SouZy61TGIY',
        'sage' => './rref-sage.php',
    ],
];
$axb_su = [
    'lecname' => 'Linear Systems',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/axb/axb.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/axb-hw.pdf',
        'youtube' => 'https://youtu.be/yIUmtIwh37Y',
        'sage' => './axb-sage.php',
        'notes' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/axb/axb-ex.pdf',
    ],
];
$gj_su = [
    'lecname' => 'Gauss-Jordan Elimination',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/gj/gj.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/gj-hw.pdf',
        'youtube' => 'https://youtu.be/LUQHT6OniTY',
        'sage' => './gj-sage.php',
        'notes' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/gj/gj-examples.pdf',
    ],
];
$nonsing_su = [
    'lecname' => 'Nonsingular Matrices',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/nonsing/nonsing.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/nonsing-hw.pdf',
        'youtube' => 'https://youtu.be/GCc2_G8TK0s',
        'sage' => './nonsing-sage.php',
    ],
];
$ear_su = [
    'lecname' => 'EA=R Factorizations',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/ear/ear.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/ear-hw.pdf',
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCBTvNlWm5TBy_662VwCjrd-',
        'sage' => './ear-sage.php',
    ],
];
$palu_su = [
    'lecname' => 'PA=LU Factorizations',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/palu/palu.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/palu-hw.pdf',
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCC8fkdt06abb3PoPsGFZGE_',
        'sage' => './palu-sage.php',
    ],
];
$evals_su = [
    'lecname' => 'Eigenvalues',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/evals/evals.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/evals-hw.pdf',
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDRFYw5usU7O0CNxmxbMkKz',
        'sage' => './evals-sage.php',
    ],
];
$null_su = [

    'lecname' => 'Null Spaces',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/null/null.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/null-hw.pdf',
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDtTAi5KpGXH-eeUgRipRlU',
        'sage' => './null-sage.php',
    ],
];
$col_su = [
    'lecname' => 'Column Spaces',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/col/col.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/col-hw.pdf',
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCCEYLykkkV9fnt9cL3wemzI',
        'sage' => './col-sage.php',
    ],
];
$fundsub_su = [
    'lecname' => 'The Four Fundamental Subspaces',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/fundsub/fundsub.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/fundsub-hw.pdf',
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCBLO0R2e0Ss2J9eDYp_VO_-',
        'sage' => './fundsub-sage.php',
    ],
];
$linind_su = [
    'lecname' => 'Linear Independence',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/linind/linind.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/linind-hw.pdf',
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDq3mdRoSQZkeZxTNKJO4vO',
        'sage' => './linind-sage.php',
    ],
];
$bases_su = [
    'lecname' => 'Bases',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/bases/bases.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/bases-hw.pdf',
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCBPCxI_yBV9yQ56aqLg7Tbg',
        'sage' => './bases-sage.php',
    ],
];
$dim_su = [

    'lecname' => 'Dimension',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/dim/dim.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/dim-hw.pdf',
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDv25ifWyybsRNPI4uS0Phj',
        'sage' => './dim-sage.php',
    ],
];
$orthog_su = [
    'lecname' => 'Orthogonality',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/orthog/orthog.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/orthog-hw.pdf',
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCD7Lhh9l271DtnbiSQIDsXk',
        'sage' => './orthog-sage.php',
    ],
];
$proj_su = [
    'lecname' => 'Projections',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/proj/proj.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/proj-hw.pdf',
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCAgtrr5g8d6oR9U8fEphFgI',
        'sage' => './proj-sage.php',
    ],
];
$xhat_su = [
    'lecname' => 'Least Squares',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/xhat/xhat.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/xhat-hw.pdf',
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCCbbYdcEFhOnazvDCBkU3Mr',
        'sage' => './xhat-sage.php',
    ],
];
$aqr_su = [
    'lecname' => 'A=QR Factorizations',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/aqr/aqr.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/aqr-hw.pdf',
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCD6Gpj_xfFSJ0FeJgvNAeZ9',
        'sage' => './aqr-sage.php',
    ],
];
$gs_su = [
    'lecname' => 'The Gram-Schmidt Algorithm',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/gs/gs.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/gs-hw.pdf',
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCCgVnzmc7GpajCxyYyqhRFa',
        'sage' => './gs-sage.php',
    ],
];
$det1_su = [
    'lecname' => 'Determinants I',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/det1/det1.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/det1-hw.pdf',
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDNySC8M3lT8qhXYIgEq1de',
        'sage' => './det1-sage.php',
    ],
];
$det2_su = [
    'lecname' => 'Determinants II',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/det2/det2.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/det2-hw.pdf',
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCCYDXbKfr21FC2rfDcs5ylV',
        'sage' => './det2-sage.php',
    ],
];
$clx_su = [
    'lecname' => 'Complex Numbers',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/clx/clx.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/clx-hw.pdf',
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCASNHStI6lJFrnsrFHyJ0oF',
        'sage' => './clx-sage.php',
    ],
];
$poly_su = [
    'lecname' => 'Polynomial Algebra',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/poly/poly.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/poly-hw.pdf',
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCC2F9MY2i0IYSbB8_mIqWrW',
        'sage' => './poly-sage.php',
    ],
];
$chi_su = [
    'lecname' => 'The Characteristic Polynomial',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/chi/chi.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/chi-hw.pdf',
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDsS8d6cKcDXeEOwXZ-DSXf',
        'sage' => './chi-sage.php',
    ],
];
$diag_su = [
    'lecname' => 'Diagonalization',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/diag/diag.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/diag-hw.pdf',
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCCJ3Ym-gfpi7jS66_grk2Bb',
        'sage' => './diag-sage.php',
    ],
];
$exp_su = [
    'lecname' => 'Matrix Exponentials',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/exp/exp.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/exp-hw.pdf',
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDIB1xn7WUZNEaefgxM0Jby',
        'sage' => './exp-sage.php',
        'notes' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/exp/exp-ex.pdf',
    ],
];
$spectral_su = [
    'lecname' => 'The Spectral Theorem',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/spectral/spectral.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/spectral-hw.pdf',
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCC0g6fs8EAsE4RMyJ_L-Vmc',
        'sage' => './spectral-sage.php',
    ],
];
$posdef_su = [
    'lecname' => 'Definiteness',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/posdef/posdef.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/posdef-hw.pdf',
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCAY-1mnoguKeJF-xe7vUP_e',
        'sage' => './posdef-sage.php',
        'notes' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/posdef/posdef-ex.pdf',
    ],
];
$svd_su = [
    'lecname' => 'Singular Value Decomposition',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/svd/svd.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/svd-hw.pdf',
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCBGtVOACD7lW9ejEMY8Tq8B',
        'sage' => './svd-sage.php',
        'notes' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/svd/svd-ex.pdf',
    ],
];
$partial_su = [
    'lecname' => 'Partial Derivatives',
    'comment' => '<strong style="color:red"> ⟵ (w/quiz but no pset)</strong>',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/partial/partial.pdf',
    'icons' => [
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCAHo2FLv4X5VFejOThbjJuf',
        'sage' => './partial-sage.php',
    ],
];
$jacobian_su = [
    'lecname' => 'Linear Approximations',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/jacobian/jacobian.pdf',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/jacobian-hw.pdf',
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCAncXe-cm8_Da5MtglnsLoQ',
        'sage' => './jacobian-sage.php',
        'notes' => './jacobian-hw.pdf',
    ],
];
$cholesky_su = [
    'lecname' => 'Cholesky Factorizations',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/cholesky/cholesky.pdf',
    'comment' => '<strong style="color:red"> ⟵ pset not collected!</strong>',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/cholesky-hw.pdf',
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCApkxXB-XIDhEDso4nDcO_f',
        'sage' => './cholesky-sage.php',
        'notes' => './cholesky-hw.pdf',
    ],
];
$hessian_su = [
    'lecname' => 'The Hessian',
    'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/hessian/hessian.pdf',
    'comment' => '<strong style="color:red"> ⟵ pset not collected!</strong>',
    'icons' => [
        'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-psets/-/raw/main/psets/hessian-hw.pdf',
        'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDvM1pjbv0O5FPs2RAVMHPm',
        'sage' => './hessian-sage.php',
        'notes' => './hessian-hw.pdf',
    ],
];
?>
