<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead('Christmas Dinner 2021') ?>

<h2>Standing Rib Roast</h2>
<ul>
    <li>rub with oil and 2t salt (+ pepper) per bone</li>
    <li>bring to room temp (one hour)</li>
    <li>start in cold oven, roast at 250F until 118F internal (could be 3hrs)</li>
    <li>cover in foil and rest</li>
    <li>brown at 500F or 550F for 10-15mins</li>
    <li>carve and serve</li>
</ul>
<h2>Yorkshire Pudding</h2>
<ul>
    <li>280g flour</li>
    <li>473ml whole milk (room temperature)</li>
    <li>200g eggs (at room temperature)</li>
    <li>1.5t kosher salt</li>
    <li>4T of roast drippings (divided in half)</li>
    <li>Bake at 400F for 30mins.</li>
</ul>
<h2>Mushroom Sauce</h2>
<ul>
    + sautee shallots in butter
    + add mushrooms and salt and cook down
    + add garlic
    + add vinegar
    + add wine & thyme
    + reduce
    + add beef stock
    + thicken with butter or even cornstarch, if necessary
</ul>
<h2>Chantilly Mashed Potatoes</h2>
<ul>
    <li>Bake at 475F for 20 minutes.</li>
    <li>Broil? Top with chives.</li>
</ul>
<h2>Carrots</h2>
<ul>
    <li>Combine carrots, butter, oil, salt, sugar, water and cook covered 3 minutes</li>
    <li>Add shallots and pepper and cook, covered, for about 2 minutes.</li>
    <li>Add the chives and cook for about 1 minute, uncovered, until the pan is dry,
        tossing the carrots occasionally.</li>
</ul>
<h2>Vin Chaud</h2>
<ul>
    <li>Combine wine, 1/4 cup sugar, 5 cloves, 4 cinnamon sticks, 2 cardamon pods,
        orange zest over very low heat.</li>
    <li>Heat mixture until it nearly reaches a simmer, stirring to dissolve
        sugar. Do not allow wine to come to a boil, or alcohol will evaporate and
        the flavor of the vin chaud will be affected.</li>
    <li>The wine will be at its optimal temperature when the sugar has dissolved and
        steam rises when mixing spoon is lifted from wine.</li>
    <li>Maintain this temperature to keep wine warm and let wine steep with spices
        to develop flavor. The longer it steeps, the stronger the flavor.</li>
    <li>Strain wine through a fine-mesh sieve or a cheesecloth-lined colander into a
        clean saucepan. Discard spices.</li>
    <li>Cover pan and let vin chaud stand for 5 to 10 minutes before serving.</li>
    <li>Add 1 to 2 teaspoons of cognac to 4 individual warmed mugs or heatproof
        glasses before ladling in the mulled wine.</li>
</ul>
<h2>Salad</h2>
