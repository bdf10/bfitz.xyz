<!doctype html>
<?php $TITLE='Math 222: Advanced Multivariable Calculus'; ?>

<title><?php echo $TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="/css/style.php">

<?php include($_SERVER['DOCUMENT_ROOT'].'/menu.php') ?>
<?php include('./menu.php') ?>

<h1><?php echo $TITLE; ?></h1>

<p> Math 222 covers partial differentiation, multiple integrals, and topics in
    differential and integral vector calculus, including Green's theorem,
    Stokes's theorem, and Gauss's theorem for students with a background in
    linear algebra.

  <h2>Lessons</h2>

<p>Exercises are generally taken from Marsden and Tromba's <em>Vector
    Calculus</em>.

  <table>
    <tr>
      <th style="text-align: left">Topic</th>
      <th style="text-align: left">Exercises</th>
    </tr>

    <tr>
      <td><a href="">Basic Vector Arithmetic</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

    <tr>
      <td><a href="">Level Sets</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

    <tr>
      <td><a href="">Limits and Continuity</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

    <tr>
      <td><a href="">Differentiation</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/222-lectures/-/raw/master/s18/paths.pdf?inline=true">Differentiable Paths</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/222-lectures/-/raw/master/s18/jacobian.pdf?inline=true">Differentiation Rules</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/222-lectures/-/raw/master/s18/tangent-spaces.pdf?inline=true">Gradients, Directional Derivatives, and Tangent Spaces</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/222-lectures/-/raw/master/s18/hessian.pdf?inline=true">Iterated Partial Derivatives</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/222-lectures/-/raw/master/s18/taylor.pdf?inline=true">Taylor's Theorem</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/222-lectures/-/raw/master/s18/optimization.pdf?inline=true">Extrema of Real-Valued Functions</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/222-lectures/-/raw/master/s18/lagrange.pdf?inline=true">Constrained Extrema and Lagrange Multipliers</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/222-lectures/-/raw/master/s18/implicit-function-theorem.pdf?inline=true">The Implicit Function Theorem</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

    <tr>
      <td><a href="">The Inverse Function Theorem</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/222-lectures/-/raw/master/s18/double-integrals.pdf?inline=true">Introduction to Double Integrals</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/222-lectures/-/raw/master/s18/general-regions.pdf?inline=true">Double Integrals Over General Regions</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

    <tr>
      <td><a href="">Changing the Order of Integration</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/222-lectures/-/raw/master/s18/triple-integrals.pdf?inline=true">Triple Integrals</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/222-lectures/-/raw/master/s18/change-of-variables.pdf?inline=true">Change of Variables</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/222-lectures/-/raw/master/s18/integrals-named-coords.pdf?inline=true">Polar, Cylindrical, and Spherical Coordinates</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/222-lectures/-/raw/master/s18/improper-integrals.pdf?inline=true">Improper Integrals</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/222-lectures/-/raw/master/s18/vector-fields.pdf?inline=true">Vector Fields</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/222-lectures/-/raw/master/s18/div-curl.pdf?inline=true">The grad-div-curl Exact Sequence</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/222-lectures/-/raw/master/s18/path-integrals.pdf?inline=true">Path Integrals</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/222-lectures/-/raw/master/s18/line-integrals.pdf?inline=true">Line Integrals</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/222-lectures/-/raw/master/s18/parametric-surfaces.pdf?inline=true">Parameterized Surfaces</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/222-lectures/-/raw/master/s18/vector-surface-integrals.pdf?inline=true">Surface Integrals</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/222-lectures/-/raw/master/s18/surface-integrals.pdf?inline=true">Surface Integrals of Vector Fields</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/222-lectures/-/raw/master/s18/greens-theorem.pdf?inline=true">Green's Theorem</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/222-lectures/-/raw/master/s18/divergence-theorem.pdf?inline=true">Stokes' Theorem and the Divergence Theorem</a></td>
      <td><a href="https://www.amazon.com/Vector-Calculus-Jerrold-Marsden/dp/1429215089/ref=sr_1_1?dchild=1&keywords=marsden+and+tromba&qid=1616088815&sr=8-1"><img src="/pix/icons/book.svg"></a></td>
    </tr>

  </table>
