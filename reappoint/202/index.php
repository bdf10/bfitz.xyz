<!doctype html>
<?php $TITLE='Math 202: Multivariable Calculus for Economists'; ?>

<title><?php echo $TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="/css/style.php">

<?php include($_SERVER['DOCUMENT_ROOT'].'/menu.php') ?>
<?php include('./menu.php') ?>

<h1><?php echo $TITLE; ?></h1>

<p> Math 202 covers Gaussian elimination, matrix algebra, determinants, linear
    independence. Calculus of several variables, chain rule, implicit
    differentiation. Optimization, first order conditions, Lagrange
    multipliers. Integration of functions of several variables. Not open to
    students who have taken Mathematics 212 or 222.

  <h2>Lessons</h2>

<p>Exercises are generally taken from Simon and Blume
  and <a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf">Whitman
    Calculus</a>. The other exercises listed below are written by me.

<p>The source code for the lecture slides below is publicly hosted on my
  <a href="https://gitlab.oit.duke.edu/teaching/202-lectures/">GitLab page</a>.

  <table>
    <tr>
      <th style="text-align: left">Topic</th>
      <th style="text-align: left">Exercises</th>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/points-vectors/points-vectors.pdf?inline=true">Points and Vectors in R^n</a> <- click to access the lecture!</td>
      <!-- <td>[SB 204] 1, 3, 4; <a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf#page=302">[W 302]</a> 1, 6, 13; <a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf#page=306">[W 306]</a> 1-11</td> -->
      <td><a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf"><img src="/pix/icons/book.svg"></a></td>
    </tr>
    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/dot-product/dot-product.pdf?inline=true">The Dot Product</a></td>
      <!-- <td>[SB 220] 10-13, 15, 20, 22; <a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf#page=312">[W 312]</a> 1-5, 6-10, 16-19, 23</td> -->
      <td><a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf"><img src="/pix/icons/book.svg"></a></td>
    </tr>
    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/cross-product/cross-product.pdf?inline=true">The Cross Product</a></td>
      <!-- <td>[SB 221] 23(f); <a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf#page=316">[W 316]</a> 1-8</td> -->
      <td><a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf"><img src="/pix/icons/book.svg"></a></td>
    </tr>
    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/lines/lines.pdf?inline=true">Lines in R^n</a></td>
      <!-- <td>[SB 225] 29, 31; <a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf#page=321">[W 321]</a> 7, 10, 12-15, 17, 21, 22</td> -->
      <td><a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf"><img src="/pix/icons/book.svg"></a></td>
    </tr>
    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/planes/planes.pdf?inline=true">Planes in R^n</a></td>
      <!-- <td>[SB 230] 32, 34 (c, d), 35 (c, d), 36, 38-40; <a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf#page=321">[W 321]</a> 1-6, 8-9, 16</td> -->
      <td><a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf"><img src="/pix/icons/book.svg"></a></td>
    </tr>
    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/gauss-jordan/gauss-jordan.pdf?inline=true">Systems of Linear Equations</a></td>
      <!-- <td>[SB 128] 2, 3, 7; [SB 133] 10, 12; [SB 140] 15-18; [SB 149] 20, 21, 23</td> -->
      <td><a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf"><img src="/pix/icons/book.svg"></a></td>
    </tr>
    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/endo-exo/endo-exo.pdf?inline=true">The Theory of Endogenous and Exogenous Variables</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/hw/202s18-endo-exo-hw.pdf"><img src="/pix/icons/memo.svg"></a></td>
      <!-- <td>[S 151] 25, 30; <a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/hw/202s18-endo-exo-hw.pdf">[AP]</a></td> -->
    </tr>
    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/matrix-algebra/matrix-algebra.pdf?inline=true">Matrix Algebra</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/hw/202s18-matrix-algebra-hw.pdf"><img src="/pix/icons/memo.svg"></a></td>
      <!-- <td>[SB 159] 1-5; [SB 161] 6, 7; <a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/hw/202s18-matrix-algebra-hw.pdf">[AP]</a></td> -->
    </tr>
    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/square-matrices/square-matrices.pdf?inline=true">The Algebra of Square Matrices</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/hw/202s18-square-matrices-hw.pdf"><img src="/pix/icons/memo.svg"></a></td>
      <!-- <td>[SB 172] 15, 18, 19, 20, 22, 25(c, d), 27(c), 28; [SB 193] 6, 7, 9; [SB 196] 11-14; <a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/hw/202s18-square-matrices-hw.pdf">[AP]</a></td> -->
    </tr>
    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/linear-independence/linear-independence.pdf?inline=true">Linear Independence</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/hw/202s18-linear-independence-hw.pdf"><img src="/pix/icons/memo.svg"></a></td>
      <!-- <td>[SB 243] 2, 3, 6; <a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/hw/202s18-linear-independence-hw.pdf">[AP]</a></td> -->
    </tr>
    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/islm/islm.pdf?inline=true">IS-LM Analysis</a></td>
      <!-- <td>[SB 198] 15, 17</td> -->
      <td><a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf"><img src="/pix/icons/book.svg"></a></td>
    </tr>
    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/linear-subspaces/linear-subspaces.pdf?inline=true">Linear Subspaces of R^n</a></td>
      <!-- <td>[SB 246] 9, 10; [SB 755] 1-4</td> -->
      <td><a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf"><img src="/pix/icons/book.svg"></a></td>
    </tr>
    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/basis-dimension/basis-dimension.pdf?inline=true">Bases and Dimension of Linear Subspaces</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/hw/202s18-bases-dimension-hw.pdf"><img src="/pix/icons/memo.svg"></a></td>
      <!-- <td>[SB 249] 12, 14; [SB 771] 12; [SB 764] 10; <a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/hw/202s18-bases-dimension-hw.pdf">[AP]</a></td> -->
    </tr>
    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/multivariable-functions/multivariable-functions.pdf?inline=true">Functions Between Euclidean Spaces</a></td>
      <!-- <td>[SB 286] 2, 9(a, b, c), 10</td> -->
      <td><a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf"><img src="/pix/icons/book.svg"></a></td>
    </tr>
    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/special-functions/special-functions.pdf?inline=true">Special Kinds of Functions</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/hw/202s18-special-functions-hw.pdf"><img src="/pix/icons/memo.svg"></a></td>
      <!-- <td>[SB 292] 11, 12, 15; [SB 299] 23, 24; <a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/hw/202s18-special-functions-hw.pdf">[AP]</a></td> -->
    </tr>
    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/partial-derivatives/partial-derivatives.pdf?inline=true">Partial Derivatives</a></td>
      <!-- <td>[SB 302] 1, 2; [SB 322] 18-20; <a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf#page=363">[W 363]</a> 1-7, 16, 17; <a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf#page=370">[W 370]</a> 1-6, 9, 10, 21(a, b); <a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf#page=372">[W 372]</a> 1-7, 10-12</td> -->
      <td><a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf"><img src="/pix/icons/book.svg"></a></td>
    </tr>
    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/linear-approximations/linear-approximations.pdf?inline=true">Linear Approximations</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/hw/202s18-linear-approximations-hw.pdf"><img src="/pix/icons/memo.svg"></a></td>
      <!-- <td>[SB 307] 4(a, b); [SB 312] 6(a, b, c), 7-10; [SB 319] 15, 16; <a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf#page=338">[W 338]</a> 4, 5, 10-13; <a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/hw/202s18-linear-approximations-hw.pdf">[AP]</a></td> -->
    </tr>
    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/chain-rule/chain-rule.pdf?inline=true">The Multivariable Chain Rule</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/hw/202s18-chain-rule-hw.pdf"><img src="/pix/icons/memo.svg"></a></td>
      <!-- <td>[SB 318] 11, 13, 14; [SB 327] 21, 22; <a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf#page=366">[W 366]</a> 1-4; <a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/hw/202s18-chain-rule-hw.pdf">[AP]</a></td> -->
    </tr>
    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/implicit-functions/implicit-functions.pdf?inline=true">Implicit Functions</a></td>
      <!-- <td>[SB 342] 1, 3-9</td> -->
      <td><a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf"><img src="/pix/icons/book.svg"></a></td>
    </tr>
    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/implicit-systems/implicit-systems.pdf?inline=true">Systems of Implicit Functions</a></td>
      <!-- <td>[SB 358] 16-24</td> -->
      <td><a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf"><img src="/pix/icons/book.svg"></a></td>
    </tr>
    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/definiteness/definiteness.pdf?inline=true">Definiteness of Quadratic Forms</a></td>
      <!-- <td>[SB 386] 1, 2; [SB 392] 6</td> -->
      <td><a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf"><img src="/pix/icons/book.svg"></a></td>
    </tr>
    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/unconstrained-optimization/unconstrained-optimization.pdf?inline=true">Unconstrained Optimization</a></td>
      <!-- <td>[SB 402] 1, 2</td> -->
      <td><a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf"><img src="/pix/icons/book.svg"></a></td>
    </tr>
    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/constrained-optimization/constrained-optimization.pdf?inline=true">Constrained Optimization</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/hw/202s18-constrained-optimization-hw.pdf"><img src="/pix/icons/memo.svg"></a></td>
      <!-- <td>[SB 423] 2, 3; <a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf#page=382">[W 382]</a> 5, 8, 10, 13-16; <a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/hw/202s18-constrained-optimization-hw.pdf">[AP]</a></td> -->
    </tr>
    <tr>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/double-integrals/double-integrals.pdf?inline=true">Double Integrals</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/hw/202s18-double-integrals-hw.pdf"><img src="/pix/icons/memo.svg"></a></a></td>
<!-- <td><a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf#page=393">[W 393]</a> 1, 2, 5, 6; <a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/hw/202s18-double-integrals-hw.pdf">[AP]</a></td> -->
</tr>
<tr>
  <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/double-integrals-examples/double-integrals-examples.pdf?inline=true">Examples of Double Integrals</a></td>
  <!-- <td><a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf#page=393">[W 393]</a> 3, 4, 9-13, 32(a, b, d, e)</td> -->
  <td><a href="https://www.whitman.edu/mathematics/multivariable/multivariable.pdf"><img src="/pix/icons/book.svg"></a></td>
</tr>

</table>

<p>

  <h2>Discussion Assignments</h2>

<p>Explain these.

<p>The source code for the assignments below is publicly hosted on my
  <a href="https://gitlab.oit.duke.edu/teaching/202-lectures/">GitLab page</a>.

  <table>
    <tr>
      <th style="text-align: left">Semester</th>
      <th style="text-align: left">Assignment</th>
    </tr>

    <tr>
      <td>Fall 2017</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/discussion-assignments/202-discussion01.pdf?inline=true">Discussion Assignment 1</a></td>
    </tr>
    <tr>
      <td>Fall 2017</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/discussion-assignments/202-discussion02.pdf?inline=true">Discussion Assignment 2</a></td>
    </tr>
    <tr>
      <td>Fall 2017</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/discussion-assignments/202-discussion03.pdf?inline=true">Discussion Assignment 3</a></td>
    </tr>
    <tr>
      <td>Fall 2017</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/discussion-assignments/202-discussion04.pdf?inline=true">Discussion Assignment 4</a></td>
    </tr>
    <tr>
      <td>Fall 2017</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/discussion-assignments/202-discussion05.pdf?inline=true">Discussion Assignment 5</a></td>
    </tr>
    <tr>
      <td>Fall 2017</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/discussion-assignments/202-discussion06.pdf?inline=true">Discussion Assignment 6</a></td>
    </tr>
    <tr>
      <td>Fall 2017</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/discussion-assignments/202-discussion07.pdf?inline=true">Discussion Assignment 7</a></td>
    </tr>
    <tr>
      <td>Fall 2017</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/discussion-assignments/202-discussion08.pdf?inline=true">Discussion Assignment 8</a></td>
    </tr>
    <tr>
      <td>Fall 2017</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/discussion-assignments/202-discussion09.pdf?inline=true">Discussion Assignment 9</a></td>
    </tr>
    <tr>
      <td>Fall 2017</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/discussion-assignments/202-discussion10.pdf?inline=true">Discussion Assignment 10</a></td>
    </tr>

  </table>

<p>

  <h2>Quizzes</h2>

<p>The source code for the quizzes below is publicly hosted on my
  <a href="https://gitlab.oit.duke.edu/teaching/202-lectures/">GitLab page</a>.

  <table>
    <tr>
      <th style="text-align: left">Semester</th>
      <th style="text-align: left">Quiz</th>
      <th style="text-align: left">Solutions</th>
    </tr>

    <tr>
      <td>Fall 2017</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f17-quiz01.pdf?inline=true">Quiz #1</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f17-quiz01-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Fall 2017</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f17-quiz02.pdf?inline=true">Quiz #2</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f17-quiz02-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Fall 2017</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f17-quiz03.pdf?inline=true">Quiz #3</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f17-quiz03-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Fall 2017</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f17-quiz04.pdf?inline=true">Quiz #4</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f17-quiz04-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Fall 2017</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f17-quiz05.pdf?inline=true">Quiz #5</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f17-quiz05-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Fall 2017</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f17-quiz06.pdf?inline=true">Quiz #6</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f17-quiz06-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Fall 2017</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f17-quiz07.pdf?inline=true">Quiz #7</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f17-quiz07-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Fall 2017</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f17-quiz08.pdf?inline=true">Quiz #8</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f17-quiz08-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Fall 2017</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f17-quiz09.pdf?inline=true">Quiz #9</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f17-quiz09-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Fall 2017</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f17-quiz10.pdf?inline=true">Quiz #10</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f17-quiz10-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Spring 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202s18-quiz01.pdf?inline=true">Quiz #1</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202s18-quiz01-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Spring 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202s18-quiz02.pdf?inline=true">Quiz #2</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202s18-quiz02-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Spring 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202s18-quiz03.pdf?inline=true">Quiz #3</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202s18-quiz03-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Spring 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202s18-quiz04.pdf?inline=true">Quiz #4</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202s18-quiz04-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Spring 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202s18-quiz05.pdf?inline=true">Quiz #5</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202s18-quiz05-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Spring 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202s18-quiz06.pdf?inline=true">Quiz #6</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202s18-quiz06-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Spring 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202s18-quiz07.pdf?inline=true">Quiz #7</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202s18-quiz07-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Spring 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202s18-quiz08.pdf?inline=true">Quiz #8</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202s18-quiz08-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Spring 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202s18-quiz09.pdf?inline=true">Quiz #9</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202s18-quiz09-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Spring 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202s18-quiz10.pdf?inline=true">Quiz #10</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202s18-quiz10-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Spring 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202s18-quiz11.pdf?inline=true">Quiz #11</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202s18-quiz11-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Spring 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202s18-quiz12.pdf?inline=true">Quiz #12</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202s18-quiz12-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Spring 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202s18-quiz13.pdf?inline=true">Quiz #13</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202s18-quiz13-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Fall 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f18-quiz01.pdf?inline=true">Quiz #1</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f18-quiz01-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Fall 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f18-quiz02.pdf?inline=true">Quiz #2</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f18-quiz02-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Fall 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f18-quiz03.pdf?inline=true">Quiz #3</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f18-quiz03-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Fall 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f18-quiz04.pdf?inline=true">Quiz #4</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f18-quiz04-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Fall 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f18-quiz05.pdf?inline=true">Quiz #5</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f18-quiz05-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Fall 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f18-quiz06.pdf?inline=true">Quiz #6</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f18-quiz06-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Fall 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f18-quiz07.pdf?inline=true">Quiz #7</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f18-quiz07-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Fall 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f18-quiz08.pdf?inline=true">Quiz #8</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f18-quiz08-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Fall 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f18-quiz09.pdf?inline=true">Quiz #9</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f18-quiz09-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Fall 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f18-quiz10.pdf?inline=true">Quiz #10</a></td>
      <td><a href="https://gitlab.oit.duke.edu/teaching/202-lectures/-/raw/master/quizzes/202f18-quiz10-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>


  </table>
