#+title: Syllabi of Courses
#+latex_header: \usepackage{fullpage}
#+latex_header: \usepackage{longtable}

#+LATEX: \newpage
* Math 218: Matrices and Vector Spaces

This is an introductory course on linear algebra that focuses on concepts,
methods and applications. Gaussian elimination is presented as the fundamental
process for solving systems of linear equations. Deeper understanding is
developed by examination of matrix factorizations, orthogonality, and associated
vector subspaces. Least squares problems, projection problems, eigenvalue
problems, the singular value decomposition and principal component analysis will
also be studied as fundamental tools for solving data-driven problems.

The style of this course will be more applied and computational than Math 221
(for students focusing on becoming math majors), which goes into much more depth
on theory and develops skills in writing rigorous mathematical proofs. Math 218
is also significantly different from Math 216 – differential equations will be
treated with less sophistication, but we will do more advanced topics from
linear algebra.

| Topic                          |
|--------------------------------|
| Matrices and Vectors           |
| Digraphs                       |
| Vector Geometry                |
| Matrix Multiplication          |
| Row Echelon Forms              |
| Linear Systems                 |
| Gauß-Jordan Elimination        |
| Nonsingular Matrices           |
| EA=R Factorizations            |
| PA=LU Factorizations           |
| Eigenvalues                    |
| Null Spaces                    |
| Column Spaces                  |
| The Four Fundamental Subspaces |
| Linear Independence            |
| Bases                          |
| Dimension                      |
| Orthogonality                  |
| Projections                    |
| Least Squares                  |
| A=QR Factorizations            |
| The Gram-Schmidt Algorithm     |
| Determinants I                 |
| Determinants II                |
| Determinants III               |
| Complex Numbers                |
| Polynomial Algebra             |
| The Characteristic Polynomial  |
| Diagonalization                |
| Matrix Exponentials            |
| The Spectral Theorem           |
| Definiteness                   |
| Cholesky Factorizations        |
| Jordan Canonical Form          |
| Singular Value Decomposition   |

#+LATEX: \newpage
* Math 790-92: Topics in Foundational Mathematics

These 5-week courses are aimed exclusively at graduate students and have numbers
790-92 with different section numbers depending on the topic. They are designed
to help students acquire basic mathematical skills so that they might take
additional courses in the mathematical sciences as well as be more successful in
self-study. Students should check the schedule for a given term to see the
offerings.

** Introduction to Matrices

| Topic                          |
|--------------------------------|
| Matrices and Vectors           |
| Digraphs                       |
| Vector Geometry                |
| Matrix Multiplication          |
| Row Echelon Forms              |
| Linear Systems                 |
| Gauß-Jordan Elimination        |
| Nonsingular Matrices           |
| EA=R Factorizations            |
| PA=LU Factorizations           |
| Eigenvalues                    |
| Null Spaces                    |
| Column Spaces                  |
| The Four Fundamental Subspaces |

** Projections and Least Squares Methods

| Topic                      |
|----------------------------|
| Linear Independence        |
| Bases                      |
| Dimension                  |
| Orthogonality              |
| Projections                |
| Least Squares              |
| A=QR Factorizations        |
| The Gram-Schmidt Algorithm |
| Determinants I             |
| Determinants II            |
| Determinants III           |

** Eigenvalues and Singular Value Decomposition

| Topic                         |
|-------------------------------|
| Complex Numbers               |
| Polynomial Algebra            |
| The Characteristic Polynomial |
| Diagonalization               |
| Matrix Exponentials           |
| The Spectral Theorem          |
| Definiteness                  |
| Cholesky Factorizations       |
| Jordan Canonical Form         |
| Singular Value Decomposition  |

** Matrix Decompositions and Data

| Topic                        |
|------------------------------|
| PA=LU Factorizations         |
| A=QR Factorizations          |
| Determinants                 |
| Eigenvalues and Eigenvectors |
| Diagonalization              |
| The Spectral Theorem         |
| Definiteness                 |
| Singular Value Decomposition |

** Discrete Probability

| Topic                           |
|---------------------------------|
| Sets and Counting               |
| Probability Spaces              |
| Bayes Rule and Independence     |
| Random Variables                |
| Expected Value                  |
| Variance and Standard Deviation |
| Covariance and Correlation      |

** Differential Calculus

| Topic                 |
|-----------------------|
| Limits                |
| Continuity            |
| Derivatives           |
| Derivative Rules      |
| Linear Approximations |
| Related Rates         |
| Optimization          |

** Integral Calculus

| Topic                               |
|-------------------------------------|
| Riemann Sums                        |
| Definite Integrals                  |
| The Fundamental Theorem of Calculus |
| Indefinite Integrals                |
| Substitution                        |
| Integration by Parts                |
| Probability Distributions           |

** Multivariable Calculus

| Topic                              |
|------------------------------------|
| Vectors and Matrices               |
| Functions Between Euclidean Spaces |
| Partial Derivatives                |
| Linear Approximations              |
| Implicit Functions                 |
| Implicit Systems                   |
| Definiteness                       |
| Unconstrained Optimization         |
| Constrained Optimization           |
| Double Integrals                   |

#+LATEX: \newpage
* Math 202: Multivariable Calculus for Economists

Math 202 covers Gaussian elimination, matrix algebra, determinants, linear
independence. Calculus of several variables, chain rule, implicit
differentiation. Optimization, first order conditions, Lagrange
multipliers. Integration of functions of several variables. Not open to students
who have taken Mathematics 212 or 222.

Exercises are generally taken from Simon and Blume and Whitman Calculus. The
other exercises are written by me.

| Topic                                            |
|--------------------------------------------------|
| Points and Vectors in R^n                        |
| The Dot Product                                  |
| The Cross Product                                |
| Lines in R^n                                     |
| Planes in R^n                                    |
| Systems of Linear Equations                      |
| The Theory of Endogenous and Exogenous Variables |
| Matrix Algebra                                   |
| The Algebra of Square Matrices                   |
| Linear Independence                              |
| IS-LM Analysis                                   |
| Linear Subspaces of R^n                          |
| Bases and Dimension of Linear Subspaces          |
| Functions Between Euclidean Spaces               |
| Special Kinds of Functions                       |
| Partial Derivatives                              |
| Linear Approximations                            |
| The Multivariable Chain Rule                     |
| Implicit Functions                               |
| Systems of Implicit Functions                    |
| Definiteness of Quadratic Forms                  |
| Unconstrained Optimization                       |
| Constrained Optimization                         |
| Double Integrals                                 |
| Examples of Double Integrals                     |

#+LATEX: \newpage
* Math 771: Teaching College Mathematics

This course is designed for first year mathematics graduate students as
preparation for teaching as graduate students at Duke and as professors, once
they graduate. Topics include lesson planning, overview of the content in
calculus courses, current issues in undergraduate mathematics education, writing
and grading tests, evaluating teaching and practice teaching. Consent of
instructor required.

| Day    | Topic                                                      |
|--------+------------------------------------------------------------|
| Day 1  | Qualities of Good and Bad Teaching                         |
| Day 2  | Writing Lecture Notes                                      |
| Day 3  | Writing Examples                                           |
| Day 4  | Presentation Assignments and Preparation                   |
| Day 5  | Workshop Presentations                                     |
| Day 6  | Classroom Observations                                     |
| Day 7  | Presentations                                              |
| Day 8  | Presentations                                              |
| Day 9  | Presentation Feedback                                      |
| Day 10 | Presentation Feedback                                      |
| Day 11 | Exam Writing                                               |
| Day 12 | Exam Grading                                               |
| Day 13 | Exam Grading                                               |
| Day 14 | Gradescope Tutorial                                        |
| Day 15 | Gradescope Tutorial                                        |
| Day 16 | Active Learning Session With Office of Learning Innovation |
| Day 17 | Final Presentation Preparation                             |
| Day 18 | Student Anxiety Session with ARC                           |
| Day 19 | Final Presentations and Homework Problems                  |
| Day 20 | Final Presentations and Homework Problems                  |
| Day 21 | Final Presentations and Homework Problems                  |
| Day 22 | Final Presentations and Homework Problems                  |
| Day 23 | Challenging Scenerios                                      |
| Day 23 | Graduate Student Panel                                     |
| Day 23 | Final Presentation Feedback                                |

#+LATEX: \newpage
* Math 212: Multivariable Calculus

This is a modified version of the standard Math 212 schedule designed for the
pilot program for first-year engineering students in Spring 2020. These students
successfully completed Math 218 "Matrices and Vector Spaces" in Fall 2019 and
thus had acheived a degree of mathematical maturity not held by a typical Math
212 student.

Vector fields are introduced on the first day of class and woven into the
schedule rather than introduced concurrently with the vector calculus theorems
at the end of the course. The language of matrices is used throughout the
course. For example, we approach the topic of unconstrained optimization by
discussing the definiteness of the quadratic form corresponding to the Hessian
matrix of the objective function. This language is traditionally unavailable to
students who have not had a course in linear algebra that covers the
definiteness of Hermitian matrices.

We used Susan Colley's "Vector Calculus", which is beautifully written and
embraces the language of matrices throughout the whole text.

| Topic                              |
|------------------------------------|
| Vector Fields                      |
| Orientations and Cross Products    |
| Lines and Planes                   |
| Quadric Surfaces                   |
| Coordinate Systems                 |
| The Derivative                     |
| The Gradient                       |
| The Chain Rule                     |
| Parameterized Curves               |
| The Hessian                        |
| Line Integrals                     |
| grad, curl, and div                |
| Double Integrals                   |
| Triple Integrals                   |
| Applications of Integration        |
| Change of Variables                |
| Parameterized Surfaces             |
| Surface Integrals                  |
| Green's Theorem                    |
| Stokes' Theorem                    |
| Gauss' Divergence Theorem          |
| The Big Picture of Vector Calculus |

#+LATEX: \newpage
* Math 222: Advanced Multivariable Calculus

Math 222 covers partial differentiation, multiple integrals, and topics in
differential and integral vector calculus, including Green's theorem, Stokes's
theorem, and Gauss's theorem for students with a background in linear algebra.

| Topic                                                  |
|--------------------------------------------------------|
| Basic Vector Arithmetic                                |
| Level Sets                                             |
| Limits and Continuity                                  |
| Differentiation                                        |
| Differentiable Paths                                   |
| Differentiation Rules                                  |
| Gradients, Directional Derivatives, and Tangent Spaces |
| Iterated Partial Derivatives                           |
| Taylor's Theorem                                       |
| Extrema of Real-Valued Functions                       |
| Constrained Extrema and Lagrange Multipliers           |
| The Implicit Function Theorem                          |
| The Inverse Function Theorem                           |
| Introduction to Double Integrals                       |
| Double Integrals Over General Regions                  |
| Changing the Order of Integration                      |
| Triple Integrals                                       |
| Change of Variables                                    |
| Polar, Cylindrical, and Spherical Coordinates          |
| Improper Integrals                                     |
| Vector Fields                                          |
| The grad-div-curl Exact Sequence                       |
| Path Integrals                                         |
| Line Integrals                                         |
| Parameterized Surfaces                                 |
| Surface Integrals                                      |
| Surface Integrals of Vector Fields                     |
| Green's Theorem                                        |
| Stokes' Theorem and the Divergence Theorem             |

#+LATEX: \newpage
* MIDS: Master in Interdisciplinary Data Science

This Linear Algebra module covers ten topics divided into four units. Unit I
introduces the basic vocabulary and properties of matrix and vector
algebra. Unit II covers systems of linear equations and nonsingular matrices
through Gauss-Jordan elimination. Unit III discets the least squares problem and
introduces the concept of QR-factorizations, which make calculating least
squares solutions more efficient. Unit IV introduces the eigenvalue problem and
principal component analysis via the Spectral Theorem.

| Unit     | Topic                                                 |
|----------+-------------------------------------------------------|
| Unit I   | Scalars and Vectors                                   |
| Unit I   | Vector Arithmetic and Geometry                        |
| Unit I   | Introduction to Matrices                              |
| Unit II  | Augmented Matrices and Reduced Row Echelon Form       |
| Unit II  | Gauss-Jordan Elimination                              |
| Unit II  | Nonsingular Matrices                                  |
| Unit III | Linear Independence and Least Squares Approximations  |
| Unit III | Gram-Schmidt Orthogonalization and QR-Factorizations  |
| Unit IV  | Eigenvalues, Eigenvectors, and Diagonalization        |
| Unit IV  | The Spectral Theorem and Principal Component Analysis |
