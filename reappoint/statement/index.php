<!doctype html>
<?php $TITLE='Intellectual Statement'; ?>

<title><?php echo $TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="/css/style.php">

<?php include($_SERVER['DOCUMENT_ROOT'].'/menu.php') ?>
<?php include('./menu.php') ?>

<h1><?php echo $TITLE; ?></h1>

<p> This statement summarizes my contributions to the mathematics department and
    to Duke University since my appointment as a Lecturer in Fall 2017. A pdf of
    this statement can be found <a href="./statement.pdf">here</a>.

  <?php include 'statement.html';?>
