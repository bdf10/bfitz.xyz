<?php require($_SERVER['DOCUMENT_ROOT'].'/scripts/bannerinfo.php'); ?>
<nav class="top-nav">
  <div class="top-nav-title">
    <!-- <h1><a id="sitetitle" href="/">Brian Fitzpatrick</a></h1> -->
    <h1>Brian Fitzpatrick</h1>
  </div>
  <ul class="top-nav-links">
    <li><a href="/">Home</a></li>
    <li><a href="https://gitlab.oit.duke.edu/bdf10/cv/-/raw/master/cv.pdf?inline=true">CV</a></li>
    <li><a href="/statement">Statement</a></li>
    <li><a href="/exams">Exams</a></li>
    <li><a href="/218">218</a></li>
    <li><a href="/790">790</a></li>
    <li><a href="/202">202</a></li>
    <li><a href="/771">771</a></li>
    <li><a href="/212">212</a></li>
    <li><a href="/222">222</a></li>
    <li><a href="/mids">MIDS</a></li>
  </ul>
</nav>
<a href="<?php echo $BANNER_HTTP; ?>">
  <div class="banner" title="<?php echo $BANNER_TEXT; ?>"></div>
</a>
