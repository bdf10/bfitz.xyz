<!doctype html>
<?php $TITLE='Exam Repository'; ?>

<title><?php echo $TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="/css/style.php">

<?php include($_SERVER['DOCUMENT_ROOT'].'/menu.php') ?>
<?php include('./menu.php') ?>

<h1><?php echo $TITLE; ?></h1>

<p>Old exams written by me.

<p>The source code for these exams is publicly hosted on my
  <a href="https://gitlab.oit.duke.edu/bdf10/public-exams/">GitLab page</a>.

  <table>
    <tr>
      <th style="text-align: left">Course</th>
      <th style="text-align: left">Semester</th>
      <th style="text-align: left">Exam</th>
      <th style="text-align: left">Solutions</th>
    </tr>

    <!-- Math 222 -->
    <tr>
      <td>Math 222</td>
      <td>Spring 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/222/222s18/222s18-exam01/222s18-exam01.pdf?inline=true">Exam I</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/222/222s18/222s18-exam01/222s18-exam01-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 222</td>
      <td>Spring 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/222/222s18/222s18-exam02/222s18-exam02.pdf?inline=true">Exam II</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/222/222s18/222s18-exam02/222s18-exam02-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <!-- Math 218 -->
    <tr>
      <td>Math 218</td>
      <td>Spring 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218s18/218s18-exam01/218s18-exam01.pdf?inline=true">Exam I</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218s18/218s18-exam01/218s18-exam01-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>
    <tr>

    <tr>
      <td>Math 218</td>
      <td>Spring 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218s18/218s18-exam02/218s18-exam02.pdf?inline=true">Exam II</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218s18/218s18-exam02/218s18-exam02-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 218</td>
      <td>Spring 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218s18/218s18-exam03/218s18-exam03.pdf?inline=true">Exam III</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218s18/218s18-exam03/218s18-exam03-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 218</td>
      <td>Fall 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218f18/218f18-exam01/218f18-exam01.pdf?inline=true">Exam I</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218f18/218f18-exam01/218f18-exam01-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>
    <tr>

    <tr>
      <td>Math 218</td>
      <td>Fall 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218f18/218f18-exam02/218f18-exam02.pdf?inline=true">Exam II</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218f18/218f18-exam02/218f18-exam02-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 218</td>
      <td>Spring 2019</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218s19/218s19-exam01/218s19-exam01.pdf?inline=true">Exam I</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218s19/218s19-exam01/218s19-exam01-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>
    <tr>

    <tr>
      <td>Math 218</td>
      <td>Spring 2019</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218s19/218s19-exam02/218s19-exam02.pdf?inline=true">Exam II</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218s19/218s19-exam02/218s19-exam02-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 218</td>
      <td>Spring 2019</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218s19/218s19-exam03/218s19-exam03.pdf?inline=true">Exam III</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218s19/218s19-exam03/218s19-exam03-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 218</td>
      <td>Summer 2019</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218su19/218su19-exam01/218su19-exam01.pdf?inline=true">Exam I</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218su19/218su19-exam01/218su19-exam01-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>
    <tr>

    <tr>
      <td>Math 218</td>
      <td>Summer 2019</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218su19/218su19-exam02/218su19-exam02.pdf?inline=true">Exam II</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218su19/218su19-exam02/218su19-exam02-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 218</td>
      <td>Summer 2019</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218su19/218su19-exam03/218su19-exam03.pdf?inline=true">Exam III</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218su19/218su19-exam03/218su19-exam03-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 218</td>
      <td>Fall 2019</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218f19/218f19-exam01/218f19-exam01.pdf?inline=true">Exam I</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218f19/218f19-exam01/218f19-exam01-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>
    <tr>

    <tr>
      <td>Math 218</td>
      <td>Fall 2019</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218f19/218f19-exam02/218f19-exam02.pdf?inline=true">Exam II</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218f19/218f19-exam02/218f19-exam02-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 218</td>
      <td>Fall 2019</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218f19/218f19-exam03/218f19-exam03.pdf?inline=true">Exam III</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218f19/218f19-exam03/218f19-exam03-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 218</td>
      <td>Spring 2020</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218s20/218s20-exam01/218s20-exam01.pdf?inline=true">Exam I</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218s20/218s20-exam01/218s20-exam01-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>
    <tr>

    <tr>
      <td>Math 218</td>
      <td>Spring 2020</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218s20/218s20-exam02/218s20-exam02.pdf?inline=true">Exam II</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/218/218s20/218s20-exam02/218s20-exam02-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <!-- Math 216 -->
    <tr>
      <td>Math 216</td>
      <td>Summer 2012</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/216/216su12/216su12-exam01/216su12-exam01.pdf?inline=true">Exam I</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/216/216su12/216su12-exam01/216su12-exam01-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>
    <tr>

    <tr>
      <td>Math 216</td>
      <td>Summer 2012</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/216/216su12/216su12-exam02/216su12-exam02.pdf?inline=true">Exam II</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/216/216su12/216su12-exam02/216su12-exam02-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 216</td>
      <td>Summer 2012</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/216/216su12/216su12-exam03/216su12-exam03.pdf?inline=true">Exam III</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/216/216su12/216su12-exam03/216su12-exam03-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 216</td>
      <td>Summer 2016</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/216/216su16/216su16-exam01/216su16-exam01.pdf?inline=true">Exam I</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/216/216su16/216su16-exam01/216su16-exam01-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>
    <tr>

    <tr>
      <td>Math 216</td>
      <td>Summer 2016</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/216/216su16/216su16-exam02/216su16-exam02.pdf?inline=true">Exam II</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/216/216su16/216su16-exam02/216su16-exam02-solutions.pdf?inline=true"><img src="/pix/icons/cross.svg"></a></td>
    </tr>

    <tr>
      <td>Math 216</td>
      <td>Summer 2016</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/216/216su16/216su16-exam03/216su16-exam03.pdf?inline=true">Exam III</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/216/216su16/216su16-exam03/216su16-exam03-solutions.pdf?inline=true"><img src="/pix/icons/cross.svg"></a></td>
    </tr>

    <tr>
      <td>Math 216</td>
      <td>Summer 2017</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/216/216su17/216su17-exam01/216su17-exam01.pdf?inline=true">Exam I</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/216/216su17/216su17-exam01/216su17-exam01-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>
    <tr>

    <tr>
      <td>Math 216</td>
      <td>Summer 2017</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/216/216su17/216su17-exam02/216su17-exam02.pdf?inline=true">Exam II</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/216/216su17/216su17-exam02/216su17-exam02-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 216</td>
      <td>Summer 2017</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/216/216su17/216su17-exam03/216su17-exam03.pdf?inline=true">Exam III</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/216/216su17/216su17-exam03/216su17-exam03-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <!-- Math 212 -->
    <tr>
      <td>Math 212</td>
      <td>Fall 2015</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/212/212f15/212f15-exam01/212f15-exam01.pdf?inline=true">Exam I</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/212/212f15/212f15-exam01/212f15-exam01-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>
    <tr>

    <tr>
      <td>Math 212</td>
      <td>Fall 2015</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/212/212f15/212f15-exam02/212f15-exam02.pdf?inline=true">Exam II</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/212/212f15/212f15-exam02/212f15-exam02-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 212</td>
      <td>Spring 2016</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/212/212s16/212s16-exam01/212s16-exam01.pdf?inline=true">Exam I</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/212/212s16/212s16-exam01/212s16-exam01-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>
    <tr>

    <tr>
      <td>Math 212</td>
      <td>Spring 2016</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/212/212s16/212s16-exam02/212s16-exam02.pdf?inline=true">Exam II</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/212/212s16/212s16-exam02/212s16-exam02-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 212</td>
      <td>Fall 2016</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/212/212f16/212f16-exam01/212f16-exam01.pdf?inline=true">Exam I</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/212/212f16/212f16-exam01/212f16-exam01-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>
    <tr>

    <tr>
      <td>Math 212</td>
      <td>Fall 2016</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/212/212f16/212f16-exam02/212f16-exam02.pdf?inline=true">Exam II</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/212/212f16/212f16-exam02/212f16-exam02-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 212</td>
      <td>Fall 2016</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/212/212f16/212f16-exam03/212f16-exam03.pdf?inline=true">Exam III</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/212/212f16/212f16-exam03/212f16-exam03-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 212</td>
      <td>Spring 2020</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/212/212s20/212s20-exam01/212s20-exam01.pdf?inline=true">Exam I</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/212/212s20/212s20-exam01/212s20-exam01-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>
    <tr>

    <tr>
      <td>Math 212</td>
      <td>Spring 2020</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/212/212s20/212s20-exam02/212s20-exam02.pdf?inline=true">Exam II</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/212/212s20/212s20-exam02/212s20-exam02-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <!-- Math 202 -->
    <tr>
      <td>Math 202</td>
      <td>Spring 2017</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/202/202s17/202s17-exam01/202s17-exam01.pdf?inline=true">Exam I</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/202/202s17/202s17-exam01/202s17-exam01-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>
    <tr>

    <tr>
      <td>Math 202</td>
      <td>Spring 2017</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/202/202s17/202s17-exam02/202s17-exam02.pdf?inline=true">Exam II</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/202/202s17/202s17-exam02/202s17-exam02-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 202</td>
      <td>Spring 2017</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/202/202s17/202s17-exam03/202s17-exam03.pdf?inline=true">Exam III</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/202/202s17/202s17-exam03/202s17-exam03-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 202</td>
      <td>Fall 2017</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/202/202f17/202f17-exam01/202f17-exam01.pdf?inline=true">Exam I</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/202/202f17/202f17-exam01/202f17-exam01-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>
    <tr>

    <tr>
      <td>Math 202</td>
      <td>Fall 2017</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/202/202f17/202f17-exam02/202f17-exam02.pdf?inline=true">Exam II</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/202/202f17/202f17-exam02/202f17-exam02-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 202</td>
      <td>Spring 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/202/202s18/202s18-exam01/202s18-exam01.pdf?inline=true">Exam I</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/202/202s18/202s18-exam01/202s18-exam01-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>
    <tr>

    <tr>
      <td>Math 202</td>
      <td>Spring 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/202/202s18/202s18-exam02/202s18-exam02.pdf?inline=true">Exam II</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/202/202s18/202s18-exam02/202s18-exam02-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 202</td>
      <td>Spring 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/202/202s18/202s18-exam03/202s18-exam03.pdf?inline=true">Exam III</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/202/202s18/202s18-exam03/202s18-exam03-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 202</td>
      <td>Fall 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/202/202f18/202f18-exam01/202f18-exam01.pdf?inline=true">Exam I</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/202/202f18/202f18-exam01/202f18-exam01-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>
    <tr>

    <tr>
      <td>Math 202</td>
      <td>Fall 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/202/202f18/202f18-exam02/202f18-exam02.pdf?inline=true">Exam II</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/202/202f18/202f18-exam02/202f18-exam02-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 202</td>
      <td>Fall 2018</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/202/202f18/202f18-exam03/202f18-exam03.pdf?inline=true">Exam III</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/202/202f18/202f18-exam03/202f18-exam03-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <!-- Math 122L -->
    <tr>
      <td>Math 122</td>
      <td>Fall 2011</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/122/122f11/122f11-exam01/122f11-exam01.pdf?inline=true">Exam I</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/122/122f11/122f11-exam01/122f11-exam01-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>
    <tr>

    <tr>
      <td>Math 122</td>
      <td>Fall 2011</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/122/122f11/122f11-exam02/122f11-exam02.pdf?inline=true">Exam II</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/122/122f11/122f11-exam02/122f11-exam02-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 122</td>
      <td>Fall 2011</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/122/122f11/122f11-exam03/122f11-exam03.pdf?inline=true">Exam III</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/122/122f11/122f11-exam03/122f11-exam03-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 122</td>
      <td>Fall 2012</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/122/122f12/122f12-exam01/122f12-exam01.pdf?inline=true">Exam I</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/122/122f12/122f12-exam01/122f12-exam01-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>
    <tr>

    <tr>
      <td>Math 122</td>
      <td>Fall 2012</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/122/122f12/122f12-exam02/122f12-exam02.pdf?inline=true">Exam II</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/122/122f12/122f12-exam02/122f12-exam02-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 122</td>
      <td>Fall 2012</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/122/122f12/122f12-exam03/122f12-exam03.pdf?inline=true">Exam III</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/122/122f12/122f12-exam03/122f12-exam03-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 122</td>
      <td>Fall 2013</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/122/122f13/122f13-exam01/122f13-exam01.pdf?inline=true">Exam I</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/122/122f13/122f13-exam01/122f13-exam01-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>
    <tr>

    <tr>
      <td>Math 122</td>
      <td>Fall 2013</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/122/122f13/122f13-exam02/122f13-exam02.pdf?inline=true">Exam II</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/122/122f13/122f13-exam02/122f13-exam02-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 122</td>
      <td>Fall 2013</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/122/122f13/122f13-exam03/122f13-exam03.pdf?inline=true">Exam III</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/122/122f13/122f13-exam03/122f13-exam03-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 111</td>
      <td>Fall 2014</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/111/111f14/111f14-exam01/111f14-exam01.pdf?inline=true">Exam I</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/111/111f14/111f14-exam01/111f14-exam01-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>
    <tr>

    <tr>
      <td>Math 111</td>
      <td>Fall 2014</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/111/111f14/111f14-exam02/111f14-exam02.pdf?inline=true">Exam II</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/111/111f14/111f14-exam02/111f14-exam02-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

    <tr>
      <td>Math 111</td>
      <td>Fall 2014</td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/111/111f14/111f14-exam03/111f14-exam03.pdf?inline=true">Exam III</a></td>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/public-exams/-/raw/master/111/111f14/111f14-exam03/111f14-exam03-solutions.pdf?inline=true"><img src="/pix/icons/key.svg"></a></td>
    </tr>

  </table>
