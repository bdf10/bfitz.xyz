<!doctype html>
<?php $TITLE='Math 218: Matrices and Vector Spaces'; ?>

<title><?php echo $TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="/css/style.php">

<?php include($_SERVER['DOCUMENT_ROOT'].'/menu.php') ?>
<?php include('./menu.php') ?>

<h1><?php echo $TITLE; ?></h1>

<p> This is an introductory course on linear algebra that focuses on concepts,
  methods and applications. Gaussian elimination is presented as the
  fundamental process for solving systems of linear equations. Deeper
  understanding is developed by examination of matrix factorizations,
  orthogonality, and associated vector subspaces. Least squares problems,
  projection problems, eigenvalue problems, the singular value decomposition
  and principal component analysis will also be studied as fundamental tools
  for solving data-driven problems.

<p> The style of this course will be more applied and computational than Math
  221 (for students focusing on becoming math majors), which goes into much
  more depth on theory and develops skills in writing rigorous mathematical
  proofs. Math 218 is also significantly different from Math 216 –
  differential equations will be treated with less sophistication, but we will
  do more advanced topics from linear algebra.

  <h2>Lessons</h2>

<p>The source code for the lecture slides below is publicly hosted on my
  <a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/">GitLab page</a>.

  <table>
    <tr>
      <th style="text-align: left">Topic</th>
      <th style="text-align: left">Resources</th>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/vocab/vocab.pdf?inline=true">Matrices and Vectors</a> <- click to access the lecture!</td>
																       <td>
																	 <a href="https://youtube.com/playlist?list=PLwK75AcBxZCDVGsMw5PXJh0ZiFOs7gKOK"><img src="/pix/icons/youtube.png"></a>
																	 <a href="https://bfitz.xyz/teaching/218s21/vocab-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
																       </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/digraphs/digraphs.pdf?inline=true">Digraphs</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCCpUdsW05EcJkWyoMk1P1w2"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/digraphs-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/geometry/geometry.pdf?inline=true">Vector Geometry</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCDuuBiIIX9_jeyQX4Owkcra"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/geometry-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/matmult/matmult.pdf?inline=true">Matrix Multiplication</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCDVZyAyLGsiIiBfgUjkNHbG"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/matmult-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/rref/rref.pdf?inline=true">Row Echelon Forms</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCDNISmN_JY_iDB1093clEzZ"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/rref-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/axb/axb.pdf?inline=true">Linear Systems</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCDwlEY1XvpLt9piayYi-KsV"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/axb-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
	<a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/axb/axb-ex.pdf?inline=true"><img src="/pix/icons/memo.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/gj/gj.pdf?inline=true">Gauß-Jordan Elimination</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCAQPajFz6SBVoGfJi7uDwN1"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/gj-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
	<a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/gj/gj-examples.pdf?inline=true"><img src="/pix/icons/memo.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/nonsing/nonsing.pdf?inline=true">Nonsingular Matrices</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCBEJAdI1OjxOYgMPr42W2aK"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/nonsing-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/ear/ear.pdf?inline=true">EA=R Factorizations</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCBTvNlWm5TBy_662VwCjrd-"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/ear-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/palu/palu.pdf?inline=true">PA=LU Factorizations</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCC8fkdt06abb3PoPsGFZGE_"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/palu-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/evals/evals.pdf?inline=true">Eigenvalues</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCDRFYw5usU7O0CNxmxbMkKz"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/evals-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/null/null.pdf?inline=true">Null Spaces</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCDtTAi5KpGXH-eeUgRipRlU"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/null-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/col/col.pdf?inline=true">Column Spaces</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCCEYLykkkV9fnt9cL3wemzI"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/col-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/fundsub/fundsub.pdf?inline=true">The Four Fundamental Subspaces</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCBLO0R2e0Ss2J9eDYp_VO_-"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/fundsub-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/linind/linind.pdf?inline=true">Linear Independence</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCDq3mdRoSQZkeZxTNKJO4vO"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/linind-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/bases/bases.pdf?inline=true">Bases</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCBPCxI_yBV9yQ56aqLg7Tbg"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/bases-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/dim/dim.pdf?inline=true">Dimension</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCDv25ifWyybsRNPI4uS0Phj"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/dim-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/orthog/orthog.pdf?inline=true">Orthogonality</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCD7Lhh9l271DtnbiSQIDsXk"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/orthog-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/proj/proj.pdf?inline=true">Projections</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCAgtrr5g8d6oR9U8fEphFgI"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/proj-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/xhat/xhat.pdf?inline=true">Least Squares</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCCbbYdcEFhOnazvDCBkU3Mr"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/xhat-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/aqr/aqr.pdf?inline=true">A=QR Factorizations</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCD6Gpj_xfFSJ0FeJgvNAeZ9"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/aqr-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/gs/gs.pdf?inline=true">The Gram-Schmidt Algorithm</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCCgVnzmc7GpajCxyYyqhRFa"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/gs-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/det1/det1.pdf?inline=true">Determinants I</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCDNySC8M3lT8qhXYIgEq1de"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/det1-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/det2/det2.pdf?inline=true">Determinants II</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCCYDXbKfr21FC2rfDcs5ylV"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/det2-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/det3/det3.pdf?inline=true">Determinants III</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCDCRpDi86YpsJsj_1bJnL_B"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/det3-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/clx/clx.pdf?inline=true">Complex Numbers</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCASNHStI6lJFrnsrFHyJ0oF"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/clx-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/poly/poly.pdf?inline=true">Polynomial Algebra</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCC2F9MY2i0IYSbB8_mIqWrW"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/poly-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/chi/chi.pdf?inline=true">The Characteristic Polynomial</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCDsS8d6cKcDXeEOwXZ-DSXf"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/chi-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/diag/diag.pdf?inline=true">Diagonalization</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCCJ3Ym-gfpi7jS66_grk2Bb"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/diag-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/exp/exp.pdf?inline=true">Matrix Exponentials</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCDIB1xn7WUZNEaefgxM0Jby"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/exp-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/spectral/spectral.pdf?inline=true">The Spectral Theorem</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCC0g6fs8EAsE4RMyJ_L-Vmc"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/spectral-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/posdef/posdef.pdf?inline=true">Definiteness</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCAY-1mnoguKeJF-xe7vUP_e"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/posdef-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/cholesky/cholesky.pdf?inline=true">Cholesky Factorizations</a></td>
      <td>
	<a href="https://www.youtube.com"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/cholesky-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/jord/jord.pdf?inline=true">Jordan Canonical Form</a></td>
      <td>
	<a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/jord-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

    <tr>
      <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/svd/svd.pdf?inline=true">Singular Value Decomposition</a></td>
      <td>
	<a href="https://youtube.com/playlist?list=PLwK75AcBxZCBGtVOACD7lW9ejEMY8Tq8B"><img src="/pix/icons/youtube.png"></a>
	<a href="https://bfitz.xyz/teaching/218s21/svd-sage.php" target="_blank"><img src="/pix/icons/sage.svg"></a>
      </td>
    </tr>

  </table>
