<!doctype html>
<?php $TITLE='Home'; ?>

<title><?php echo $TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="/css/style.php">

<?php include($_SERVER['DOCUMENT_ROOT'].'/menu.php') ?>

<h1>
  <?php echo $TITLE; ?>
</h1>

<p> Since Fall 2017, I have served as a lecturer in
    the <a href="http://math.duke.edu/">mathematics department</a>
    at <a href="http://duke.edu">Duke University</a>. This website will serve as
    a repository of materials relevant to my reappointment as a lecturer.

<p> My intellectual statement and syllabi of courses can be found at the links
    at the top of this website. A pdf of my statement can be
    found <a href="./statement/statement.pdf">here</a> and a pdf of my syllabi
    of courses can be found <a href="./syllabi/syllabi.pdf">here</a>.

<p> This website is built upon free and open source software. You can contribute
    to the construction of this site through
    its <a href="https://gitlab.oit.duke.edu/bdf10/bfitz.xyz">GitLab
    repository</a>.

<p> The url of this site
    is <a href="http://reappoint.bfitz.xyz">reappoint.bfitz.xyz</a>. Information
    relevant to courses I am currently teaching can be found at my main
    webpage <a href="http://bfitz.xyz">bfitz.xyz</a>.
