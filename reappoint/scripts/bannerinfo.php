<?php
date_default_timezone_set('America/New_York');
function get(&$var, $default=null) {
    return isset($var) ? $var : $default;
}
$banners = [
    '02.01' => ['suspiria.png', 'Suspiria (1977)', 'https://en.wikipedia.org/wiki/Suspiria'],
    '03.14' => ['blood-and-black-lace.png', 'Blood and Black Lace (1964)', 'https://en.wikipedia.org/wiki/Blood_and_Black_Lace'],
    // '04.03' => ['2001.png', '2001: A Space Odyssey (1968)', 'https://en.wikipedia.org/wiki/2001:_A_Space_Odyssey_(film)'],
    // '04.26' => ['the-last-waltz.jpg', 'The Last Waltz (1978)', 'https://en.wikipedia.org/wiki/The_Last_Waltz'],
    '06.24' => ['sorcerer.jpg', 'Sorcerer (1977)', 'https://en.wikipedia.org/wiki/Sorcerer_(film)'],
    '06.25' => ['the-thing.jpg', 'The Thing (1982)', 'https://en.wikipedia.org/wiki/The_Thing_(1982_film)'],
    '10.28' => ['tenebrae.png', 'Tenebrae (1982)', 'https://en.wikipedia.org/wiki/Tenebrae_(film)'],
    // '05.18' => ['blood-and-black-lace.png', 'Blood and Black Lace (1964)'],
];
$BANNER_FILE = '/pix/banners/' . get($banners[date('m.d')][0], 'a-serious-man.jpg');
$BANNER_TEXT = get($banners[date('m.d')][1], 'A Serious Man (2009)');
$BANNER_HTTP = get($banners[date('m.d')][2], 'https://en.wikipedia.org/wiki/A_Serious_Man');
?>
