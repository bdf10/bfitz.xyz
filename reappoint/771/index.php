<!doctype html>
<?php $TITLE='Math 771S: Teaching College Mathematics'; ?>

<title><?php echo $TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="/css/style.php">

<?php include($_SERVER['DOCUMENT_ROOT'].'/menu.php') ?>
<?php include('./menu.php') ?>

<h1><?php echo $TITLE; ?></h1>

<p> This course is designed for first year mathematics graduate students as
    preparation for teaching as graduate students at Duke and as professors,
    once they graduate. Topics include lesson planning, overview of the content
    in calculus courses, current issues in undergraduate mathematics education,
    writing and grading tests, evaluating teaching and practice
    teaching. Consent of instructor required.

  <h2>Schedule</h2>

  <table>
    <tr>
      <th style="text-align: left">Day</th>
      <th style="text-align: left">Topic</th>
    </tr>

    <tr>
      <td>Day 1</td>
      <td>Qualities of Good and Bad Teaching</td>
    </tr>

    <tr>
      <td>Day 2</td>
      <td>Writing Lecture Notes</td>
    </tr>

    <tr>
      <td>Day 3</td>
      <td>Writing Examples</td>
    </tr>

    <tr>
      <td>Day 4</td>
      <td>Presentation Assignments and Preparation</td>
    </tr>

    <tr>
      <td>Day 5</td>
      <td>Workshop Presentations</td>
    </tr>

    <tr>
      <td>Day 6</td>
      <td>Classroom Observations</td>
    </tr>

    <tr>
      <td>Day 7</td>
      <td>Presentations</td>
    </tr>

    <tr>
      <td>Day 8</td>
      <td>Presentations</td>
    </tr>

    <tr>
      <td>Day 9</td>
      <td>Presentation Feedback</td>
    </tr>

    <tr>
      <td>Day 10</td>
      <td>Presentation Feedback</td>
    </tr>

    <tr>
      <td>Day 11</td>
      <td>Exam Writing</td>
    </tr>

    <tr>
      <td>Day 12</td>
      <td>Exam Grading</td>
    </tr>

    <tr>
      <td>Day 13</td>
      <td>Exam Grading</td>
    </tr>

    <tr>
      <td>Day 14</td>
      <td>Gradescope Tutorial</td>
    </tr>

    <tr>
      <td>Day 15</td>
      <td>Gradescope Tutorial</td>
    </tr>

    <tr>
      <td>Day 16</td>
      <td>Active Learning Session With Office of Learning Innovation</td>
    </tr>

    <tr>
      <td>Day 17</td>
      <td>Final Presentation Preparation</td>
    </tr>

    <tr>
      <td>Day 18</td>
      <td>Student Anxiety Session with ARC</td>
    </tr>

    <tr>
      <td>Day 19</td>
      <td>Final Presentations and Homework Problems</td>
    </tr>

    <tr>
      <td>Day 20</td>
      <td>Final Presentations and Homework Problems</td>
    </tr>

    <tr>
      <td>Day 21</td>
      <td>Final Presentations and Homework Problems</td>
    </tr>

    <tr>
      <td>Day 22</td>
      <td>Final Presentations and Homework Problems</td>
    </tr>

    <tr>
      <td>Day 23</td>
      <td>Challenging Scenerios</td>
    </tr>

    <tr>
      <td>Day 23</td>
      <td>Graduate Student Panel</td>
    </tr>

    <tr>
      <td>Day 23</td>
      <td>Final Presentation Feedback</td>
    </tr>

  </table>
