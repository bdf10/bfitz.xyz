<!doctype html>
<?php $TITLE='MIDS: Master in Interdisciplinary Data Science'; ?>

<title><?php echo $TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="/css/style.php">

<?php include($_SERVER['DOCUMENT_ROOT'].'/menu.php') ?>
<?php include('./menu.php') ?>

<h1><?php echo $TITLE; ?></h1>

<p> This Linear Algebra module covers ten topics divided into four units. Unit I
    introduces the basic vocabulary and properties of matrix and vector
    algebra. Unit II covers systems of linear equations and nonsingular matrices
    through Gauss-Jordan elimination. Unit III discets the least squares problem
    and introduces the concept of QR-factorizations, which make calculating
    least squares solutions more efficient. Unit IV introduces the eigenvalue
    problem and principal component analysis via the Spectral Theorem.

  <h2>Lessons</h2>

<p>The source code for the lessons below is publicly hosted on my
  <a href="https://gitlab.oit.duke.edu/mids/coursepacks/">GitLab page</a>.

  <table>
    <tr>
      <th style="text-align: left">Unit</th>
      <th style="text-align: left">Topic</th>
    </tr>

    <tr>
      <td>Unit I</td>
      <td><a href="https://gitlab.oit.duke.edu/mids/coursepacks/-/raw/master/unit01/unit01-section01.pdf?inline=true">Scalars and Vectors</a></td>
    </tr>

    <tr>
      <td>Unit I</td>
      <td><a href="https://gitlab.oit.duke.edu/mids/coursepacks/-/raw/master/unit01/unit01-section02.pdf?inline=true">Vector Arithmetic and Geometry</a></td>
    </tr>

    <tr>
      <td>Unit I</td>
      <td><a href="https://gitlab.oit.duke.edu/mids/coursepacks/-/raw/master/unit01/unit01-section03.pdf?inline=true">Introduction to Matrices</a></td>
    </tr>

    <tr>
      <td>Unit II</td>
      <td><a href="https://gitlab.oit.duke.edu/mids/coursepacks/-/raw/master/unit02/unit02-section01.pdf?inline=true">Augmented Matrices and Reduced Row Echelon Form</a></td>
    </tr>

    <tr>
      <td>Unit II</td>
      <td><a href="https://gitlab.oit.duke.edu/mids/coursepacks/-/raw/master/unit02/unit02-section02.pdf?inline=true">Gauss-Jordan Elimination</a></td>
    </tr>

    <tr>
      <td>Unit II</td>
      <td><a href="https://gitlab.oit.duke.edu/mids/coursepacks/-/raw/master/unit02/unit02-section03.pdf?inline=true">Nonsingular Matrices</a></td>
    </tr>

    <tr>
      <td>Unit III</td>
      <td><a href="https://gitlab.oit.duke.edu/mids/coursepacks/-/raw/master/unit03/unit03-section01.pdf?inline=true">Linear Independence and Least Squares Approximations</a></td>
    </tr>

    <tr>
      <td>Unit III</td>
      <td><a href="https://gitlab.oit.duke.edu/mids/coursepacks/-/raw/master/unit03/unit03-section02.pdf?inline=true">Gram-Schmidt Orthogonalization and QR-Factorizations</a></td>
    </tr>

    <tr>
      <td>Unit IV</td>
      <td><a href="https://gitlab.oit.duke.edu/mids/coursepacks/-/raw/master/unit04/unit04-section01.pdf?inline=true">Eigenvalues, Eigenvectors, and Diagonalization</a></td>
    </tr>

    <tr>
      <td>Unit IV</td>
      <td><a href="https://gitlab.oit.duke.edu/mids/coursepacks/-/raw/master/unit04/unit04-section02.pdf?inline=true">The Spectral Theorem and Principal Component Analysis</a></td>
    </tr>

  </table>
