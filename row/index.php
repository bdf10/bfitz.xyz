<?php include($_SERVER['DOCUMENT_ROOT'].'/scripts/head.php') ?>
<?php mkhead('Home') ?>

<p> In late 2022, I purchased a WaterRower Classic Rowing Machine with S4
    Monitor. I assembled the machine and put in my first session on January 9,
    2023. This website is an attempt for me to organize my rowing data.</p>

<p> This website is built upon free and open source software. You can contribute
    to the construction of this site through its
    <a href="https://gitlab.oit.duke.edu/bdf10/bfitz.xyz">GitLab
        repository</a>.</p>
