<?php
date_default_timezone_set('America/New_York');

function get(&$var, $default=null) {
    return isset($var) ? $var : $default;
}

function mkhead($h1=null, $icons=null, $banner_id=null) {

    $cfg = is_file('./cfg.php') ? include('./cfg.php') : [];
    $h1 = get($h1, get($cfg['h1'], 'title'));
    $icons = get($icons, get($cfg['icons'], []));
    $banner_id = get($banner_id, get($cfg['banner_id'], date('m.d')));

    $banners = [
        /* 'default' => ['exorcist-rowing.png', 'The Exorcist III (1990)', 'The_Exorcist_III'], */
        'default' => ['foo.png', 'The Exorcist III (1990)', 'The_Exorcist_III'],
    ];
    $banner = get($banners[$banner_id], $banners['default']);

    echo <<<EOF
    <!doctype html>

    <title>$h1</title>
    <link rel="stylesheet" type="text/css" href="/css/style.css">

    EOF;

    echo <<<EOF
    <nav class="top-nav">
      <div class="top-nav-title">
        <h1>Brian Fitzpatrick</h1>
      </div>
      <ul class="top-nav-links">
        <li><a href="/">Home</a></li>
        <li><a href="/data/path">Data</a></li>
      </ul>
    </nav>
    <a href="https://en.wikipedia.org/wiki/$banner[2]">
      <div style="border-radius: .3rem;
                  height: 28vh;
                  margin-left: -1em;
                  margin-right: -1em;
                  margin-bottom: 1em;
                  background: url('/pix/banners/$banner[0]') no-repeat center center;
                  background-size: cover"
           title="$banner[1]">
      </div>
    </a>

    <h1>
      $h1

    EOF;

    foreach ($icons as $icon) {
        echo <<<EOF
          <a href="$icon[0]" title="$icon[1]"><img src="/pix/icons/$icon[2]" style="height: 1.75em !important"></a>

        EOF;
    }
    echo "</h1>\n\n" . get($cfg['msg']) . "\n";
}

?>
