<?php
date_default_timezone_set('America/New_York');

function get(&$var, $default=null) {
  return isset($var) ? $var : $default;
}

function mkhead($h1=null, $sage=null, $icons=null, $banner_id=null) {

  $cfg = is_file('./cfg.php') ? include('./cfg.php') : [];
  $h1 = get($h1, get($cfg['h1'], 'title'));
  $icons = get($icons, get($cfg['icons'], []));
  $banner_id = get($banner_id, get($cfg['banner_id'], date('m.d')));

  $banners = [
    '02.01' => ['suspiria.png', 'Suspiria (1977)', 'Suspiria'],
    /* '10.28' => ['tenebrae.jpg', 'Tenebrae (1982)', 'Tenebrae_(film)'], */
    /* 'default' => ['h3.jpg', 'Halloween III: Season of the Witch', 'Halloween_III:_Season_of_the_Witch'], */
    'default' => ['dracula.png', '', '']
  ];
  $banner = get($banners[$banner_id], $banners['default']);

  echo <<<EOF
    <!doctype html>

    <title>$h1</title>
    <link rel="stylesheet" type="text/css" href="/css/style.css">

EOF;

  echo <<<EOF
    <nav class="top-nav">
      <div class="top-nav-title">
        <h1>Halloween 2022</h1>
      </div>
      <ul class="top-nav-links">
        <li><a href="/">Home</a></li>
        <li><a href="/">About</a></li>
      </ul>
    </nav>
    <a href="https://en.wikipedia.org/wiki/$banner[2]">
      <div style="border-radius: .3rem;
                  height: 28vh;
                  margin-left: -1em;
                  margin-right: -1em;
                  margin-bottom: 1em;
                  background: url('/pix/banners/$banner[0]') no-repeat center center;
                  background-size: cover"
           title="$banner[1]">
      </div>
    </a>

    <h1>
      $h1

EOF;

  foreach ($icons as $icon) {
    echo <<<EOF
          <a href="$icon[0]" title="$icon[1]"><img src="/pix/icons/$icon[2]" style="height: 1.75em !important"></a>

EOF;
  }
  echo "</h1>\n\n" . get($cfg['msg']) . "\n";
}


function mktbl($l, $rolling=true, $moment=null) {
  echo <<<EOL
    <table>
    \t<tr>
    \t\t<th style="text-align: left">Timeframe</th>
    \t\t<th style="text-align: left">Topic</th>
    \t\t<th style="text-align: left">Resources</th>
    \t</tr>

EOL;
  $moment = get($moment, time());
  foreach($l as $line) {
    if ($rolling and !empty($line) and $moment < get($line['debut'], 0)) {
      break;
    }
    if (empty($line)){
      echo <<<EOL
            \t<tr class="blank_row"><td></td></tr>

EOL;
      continue;
    }
    $line['col1'] = get($line['col1'], '');
    echo <<<EOL
        \t<tr>
        \t\t<td>{$line['col1']}</td>
        \t\t<td><a href="{$line['leclink']}">{$line['lecname']}</a></td>
        \t\t<td>

EOL;
    foreach(get($line['icons'], []) as $icon_type => $icon_link) {
      foreach(scandir($_SERVER['DOCUMENT_ROOT'].'/pix/icons/') as $icon_file) {
        if (str_starts_with($icon_file, $icon_type)) {
          $icon = $icon_file;
          break;
        }
      }
      echo <<<EOL
            \t\t\t<a href="$icon_link"><img src="/pix/icons/$icon"></a>

EOL;
    }
    echo "\t\t</td>" . PHP_EOL;

  }
  echo "</table>";
}
?>
