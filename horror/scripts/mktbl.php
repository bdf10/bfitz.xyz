<?php
date_default_timezone_set('America/New_York');

/* function get(&$var, $default=null) {
 *     return isset($var) ? $var : $default;
 * } */

$l = [
    [
        'debut' => mktime(hour: 6, day: 23, month: 8, year: 2021),
        'lecname' => 'Welcome!',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/welcome/welcome.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtu.be/9rVbL30Xrtk',
        ],
    ],
    [
        'bump' => 'week',
        'debut' => mktime(hour: 6, day: 23, month: 8, year: 2021),
        'lecname' => 'Matrices and Vectors',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/vocab/vocab.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDVGsMw5PXJh0ZiFOs7gKOK',
            'sage' => './vocab-sage.php',
        ],
    ],
    [
        'lecname' => 'Digraphs',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/digraphs/digraphs.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCCpUdsW05EcJkWyoMk1P1w2',
            'sage' => './digraphs-sage.php',
        ],
    ],
    [
        'lecname' => 'Vector Geometry',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/geometry/geometry.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDuuBiIIX9_jeyQX4Owkcra',
            'sage' => './geometry-sage.php',
        ],
    ],
    [
        'bump' => 'week',
        'lecname' => 'Lines and Planes',
        'leclink' => '',
        'icons' => [
            'youtube' => '',
            'sage' => './flat-sage.php',
        ],
    ],
    [
        'lecname' => 'Matrix Multiplication',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/matmult/matmult.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDVZyAyLGsiIiBfgUjkNHbG',
            'sage' => './matmult-sage.php',
        ],
    ],
    [
        'lecname' => 'Row Echelon Forms',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/rref/rref.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDNISmN_JY_iDB1093clEzZ',
            'sage' => './rref-sage.php',
        ],
    ],
    [
        'bump' => 'week',
        'lecname' => 'Linear Systems',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/axb/axb.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://bfitz.xyz/teaching/218s21/axb-sage.php',
            'sage' => './axb-sage.php',
            'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/axb/axb-ex.pdf?inline=true',
        ],
    ],
    [
        'lecname' => 'Gauss-Jordan Elimination',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/gj/gj.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCAQPajFz6SBVoGfJi7uDwN1',
            'sage' => './gj-sage.php',
            'memo' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/gj/gj-examples.pdf?inline=true',
        ],
    ],
    [
        'lecname' => 'Nonsingular Matrices',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/nonsing/nonsing.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCBEJAdI1OjxOYgMPr42W2aK',
            'sage' => './nonsing-sage.php',
        ],
    ],
    [
        'bump' => 'week',
        'lecname' => 'EA=R Factorizations',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/ear/ear.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCBTvNlWm5TBy_662VwCjrd-',
            'sage' => './ear-sage.php',
        ],
    ],
    [
        'lecname' => 'PA=LU Factorizations',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/palu/palu.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCC8fkdt06abb3PoPsGFZGE_',
            'sage' => './palu-sage.php',
        ],
    ],
    [
        'lecname' => 'Eigenvalues',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/evals/evals.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDRFYw5usU7O0CNxmxbMkKz',
            'sage' => './evals-sage.php',
        ],
    ],
    [
        'bump' => 'exam',
        'lecname' => 'Exam I',
        'leclink' => '',
        'icons' => [
            'key' => '',
        ],
    ],
    [
        'bump' => 'week',
        'lecname' => 'Null Spaces',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/null/null.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDtTAi5KpGXH-eeUgRipRlU',
            'sage' => './null-sage.php',
        ],
    ],
    [
        'lecname' => 'Column Spaces',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/col/col.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCCEYLykkkV9fnt9cL3wemzI',
            'sage' => './col-sage.php',
        ],
    ],
    [
        'lecname' => 'The Four Fundamental Subspaces',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/fundsub/fundsub.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCBLO0R2e0Ss2J9eDYp_VO_-',
            'sage' => './fundsub-sage.php',
        ],
    ],
    [
        'bump' => 'week',
        'lecname' => 'Linear Independence',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/linind/linind.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDq3mdRoSQZkeZxTNKJO4vO',
            'sage' => './linind-sage.php',
        ],
    ],
    [
        'lecname' => 'Bases',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/bases/bases.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCBPCxI_yBV9yQ56aqLg7Tbg',
            'sage' => './bases-sage.php',
        ],
    ],
    [
        'lecname' => 'Dimension',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/dim/dim.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDv25ifWyybsRNPI4uS0Phj',
            'sage' => './dim-sage.php',
        ],
    ],
    [
        'bump' => 'week',
        'lecname' => 'Orthogonality',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/orthog/orthog.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCD7Lhh9l271DtnbiSQIDsXk',
            'sage' => './orthog-sage.php',
        ],
    ],
    [
        'lecname' => 'Projections',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/proj/proj.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCAgtrr5g8d6oR9U8fEphFgI',
            'sage' => './proj-sage.php',
        ],
    ],
    [
        'bump' => 'week',
        'lecname' => 'Least Squares',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/xhat/xhat.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCCbbYdcEFhOnazvDCBkU3Mr',
            'sage' => './xhat-sage.php',
        ],
    ],
    [
        'lecname' => 'A=QR Factorizations',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/aqr/aqr.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCD6Gpj_xfFSJ0FeJgvNAeZ9',
            'sage' => './aqr-sage.php',
        ],
    ],
    [
        'lecname' => 'The Gram-Schmidt Algorithm',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/gs/gs.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCCgVnzmc7GpajCxyYyqhRFa',
            'sage' => './gs-sage.php',
        ],
    ],
    [
        'bump' => 'exam',
        'lecname' => 'Exam II',
        'leclink' => '',
        'icons' => [
            'key' => '',
        ],
    ],
    [
        'bump' => 'week',
        'lecname' => 'Determinants I',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/det1/det1.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDNySC8M3lT8qhXYIgEq1de',
            'sage' => './det1-sage.php',
        ],
    ],
    [
        'lecname' => 'Determinants II',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/det2/det2.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCCYDXbKfr21FC2rfDcs5ylV',
            'sage' => './det2-sage.php',
        ],
    ],
    [
        'lecname' => 'Determinants III',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/det3/det3.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCCYDXbKfr21FC2rfDcs5ylV',
            'sage' => './det3-sage.php',
        ],
    ],
    [
        'bump' => 'week',
        'lecname' => 'Complex Numbers',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/clx/clx.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCASNHStI6lJFrnsrFHyJ0oF',
            'sage' => './clx-sage.php',
        ],
    ],
    [
        'lecname' => 'Polynomial Algebra',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/poly/poly.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCC2F9MY2i0IYSbB8_mIqWrW',
            'sage' => './poly-sage.php',
        ],
    ],
    [
        'lecname' => 'The Characteristic Polynomial',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/chi/chi.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCDsS8d6cKcDXeEOwXZ-DSXf',
            'sage' => './chi-sage.php',
        ],
    ],
    [
        'bump' => 'week',
        'lecname' => 'Diagonalization',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/diag/diag.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCCJ3Ym-gfpi7jS66_grk2Bb',
            'sage' => './diag-sage.php',
        ],
    ],
    [
        'lecname' => 'Matrix Exponentials',
        'leclink' => '',
        'icons' => [
            'youtube' => '',
            'sage' => './exp-sage.php',
        ],
    ],
    [
        'lecname' => 'Jordan Canonical Form',
        'leclink' => '',
        'icons' => [
            'youtube' => '',
            'sage' => './jord-sage.php',
        ],
    ],
    [
        'bump' => 'week',
        'lecname' => 'The Spectral Theorem',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/spectral/spectral.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCC0g6fs8EAsE4RMyJ_L-Vmc',
            'sage' => './spectral-sage.php',
        ],
    ],
    [
        'lecname' => 'Definiteness',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/posdef/posdef.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCAY-1mnoguKeJF-xe7vUP_e',
            'sage' => './posdef-sage.php',
        ],
    ],
    [
        'lecname' => 'Cholesky Factorizations',
        'leclink' => '',
        'icons' => [
            'youtube' => '',
            'sage' => './cholesky-sage.php',
        ],
    ],
    [
        'bump' => 'exam',
        'lecname' => 'Exam III',
        'leclink' => '',
        'icons' => [
            'key' => '',
        ],
    ],
    [
        'bump' => 'week',
        'lecname' => 'Singular Value Decomposition',
        'leclink' => 'https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/svd/svd.pdf?inline=true',
        'icons' => [
            'youtube' => 'https://youtube.com/playlist?list=PLwK75AcBxZCBGtVOACD7lW9ejEMY8Tq8B',
            'sage' => './svd-sage.php',
        ],
    ],
    [
        'lecname' => 'Pseudoinverses?/PCA?/Matrix Norms?',
        'leclink' => '',
        'icons' => [
            'youtube' => '',
            'sage' => './pseudo-sage.php',
        ],
    ],
    [
        'bump' => 'week',
        'lecname' => 'Partial Derivatives',
        'leclink' => '',
        'icons' => [
            'youtube' => '',
            'sage' => './dydx-sage.php',
        ],
    ],
    [
        'bump' => 'week',
        'lecname' => 'Linear Approximations',
        'leclink' => '',
        'icons' => [
            'youtube' => '',
            'sage' => './approx-sage.php',
        ],
    ],
    [
        'lecname' => 'The Hessian',
        'leclink' => '',
        'icons' => [
            'youtube' => '',
            'sage' => './hessian-sage.php',
        ],
    ],
    [
        'lecname' => 'Pseudoinverses?/PCA?/Matrix Norms?',
        'leclink' => '',
        'icons' => [
            'youtube' => '',
            'sage' => './hessian-sage.php',
        ],
    ],
];

function mktbl($l, $week=0, $exam=1) {
    echo <<<EOL
    <table>
    \t<tr>
    \t\t<th style="text-align: left">Timeframe</th>
    \t\t<th style="text-align: left">Topic</th>
    \t\t<th style="text-align: left">Resources</th>
    \t</tr>

    EOL;
    $debut_formatted = date("D d-M", $debut);
    $col1 = "Week $week ($debut_formatted)";
    foreach($l as $line) {
        $debut = get($line['debut'], $debut);
        if (isset($line['bump'])) {
            if ($line['bump'] == 'week') {
                $debut = get($line['debut'], strtotime("+7 day", $debut));
                $week++;
                $debut_formatted = date("D d-M", $debut);
                $col1 = "Week $week ($debut_formatted)";
            } elseif ($line['bump'] == 'exam') {
                $col1 = "Weeks $exam-$week";
                $exam = $week+1;
            }
            /* if (time() < $debut) { break; } */
            echo <<<EOL
            \t<tr class="blank_row"><td></td></tr>

            EOL;
        }
        echo <<<EOL
        \t<tr>
        \t\t<td>$col1</td>
        \t\t<td><a href="{$line['leclink']}">{$line['lecname']}</a></td>
        \t\t<td>

        EOL;
        foreach(get($line['icons'], []) as $icon_type => $icon_link) {
            foreach(scandir($_SERVER['DOCUMENT_ROOT'].'/pix/icons/') as $icon_file) {
                if (str_starts_with($icon_file, $icon_type)) {
                    $icon = $icon_file;
                    break;
                }
            }
            echo <<<EOL
            \t\t\t<a href="$icon_link"><img src="/pix/icons/$icon"></a>

            EOL;
        }
        echo "\t\t</td>" . PHP_EOL;

    }
    echo "</table>";
}

mktbl($l)
?>
