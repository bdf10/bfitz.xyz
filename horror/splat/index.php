<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<!-- Created by htmlize-1.57 in css mode. -->
<html>
    <head>
        <title>splatter.org</title>
        <style type="text/css">
         <!--
         body {
             color: #839496;
             background-color: #002b36;
         }
         .hl-line {
             /* hl-line */
             background-color: #073642;
         }
         .org-done {
             /* org-done */
             color: #859900;
             font-weight: bold;
         }
         .org-headline-done {
             /* org-headline-done */
             color: #859900;
         }
         .org-hide {
             /* org-hide */
             color: #002b36;
         }
         .org-level-1 {
             /* org-level-1 */
             color: #cb4b16;
             background-color: #002b36;
         }
         .org-level-2 {
             /* org-level-2 */
             color: #859900;
             background-color: #002b36;
         }
         .org-tag {
             /* org-tag */
             font-weight: bold;
         }
         .org-todo {
             /* org-todo */
             color: #2aa198;
             font-weight: bold;
         }

         a {
             color: inherit;
             background-color: inherit;
             font: inherit;
             text-decoration: inherit;
         }
         a:hover {
             text-decoration: underline;
         }
         -->
        </style>
    </head>
    <body>
        <pre>
            <span class="org-level-1"> * Friday, October 7th</span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-todo">TODO</span></span><span class="org-level-2"> 03:00 PM THE CRAZIES                                            </span><span class="org-level-2"><span class="org-tag">:one:</span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-todo">TODO</span></span><span class="org-level-2"> 03:10 PM KILLER KLOWNS FROM OUTER SPACE                         </span><span class="org-level-2"><span class="org-tag">:two:</span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-todo">TODO</span></span><span class="org-level-2"> 05:00 PM TREMORS                                                </span><span class="org-level-2"><span class="org-tag">:two:</span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-done">DONE</span></span><span class="org-level-2"> </span><span class="org-level-2"><span class="org-headline-done">05:10 PM VIDEODROME                                             </span></span><span class="org-level-2"><span class="org-tag"><span class="org-headline-done">:one:</span></span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-todo">TODO</span></span><span class="org-level-2"> 07:00 PM INTERVIEW WITH THE VAMPIRE                             </span><span class="org-level-2"><span class="org-tag">:one:</span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-done">DONE</span></span><span class="org-level-2"> </span><span class="org-level-2"><span class="org-headline-done">07:30 PM THE TINGLER/HOUSE ON HAUNTED HILL                   </span></span><span class="org-level-2"><span class="org-tag"><span class="org-headline-done">:fletch:</span></span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-todo">TODO</span></span><span class="org-level-2"> 09:30 PM A NIGHTMARE ON ELM STREET                              </span><span class="org-level-2"><span class="org-tag">:one:</span></span>
            <span class="org-level-1"> * Saturday, October 8th</span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-done">DONE</span></span><span class="org-level-2"> </span><span class="org-level-2"><span class="org-headline-done">11:00 AM KILLER KLOWNS FROM OUTER SPACE                         </span></span><span class="org-level-2"><span class="org-tag"><span class="org-headline-done">:one:</span></span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-todo">TODO</span></span><span class="org-level-2"> 11:10 AM ALTERED STATES                                         </span><span class="org-level-2"><span class="org-tag">:two:</span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-todo">TODO</span></span><span class="org-level-2"> 11:20 AM DUEL                                                </span><span class="org-level-2"><span class="org-tag">:fletch:</span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-done">DONE</span></span><span class="org-level-2"> </span><span class="org-level-2"><span class="org-headline-done">01:00 PM THE CRAZIES                                            </span></span><span class="org-level-2"><span class="org-tag"><span class="org-headline-done">:one:</span></span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-todo">TODO</span></span><span class="org-level-2"> 01:10 PM THE BURNING                                         </span><span class="org-level-2"><span class="org-tag">:fletch:</span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-todo">TODO</span></span><span class="org-level-2"> 01:20 PM NIGHT OF THE DEMON                                     </span><span class="org-level-2"><span class="org-tag">:two:</span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-todo">TODO</span></span><span class="org-level-2"> 03:10 PM THE &#8216;BURBS                                          </span><span class="org-level-2"><span class="org-tag">:fletch:</span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-done">DONE</span></span><span class="org-level-2"> </span><span class="org-level-2"><span class="org-headline-done">03:20 PM TREMORS                                                </span></span><span class="org-level-2"><span class="org-tag"><span class="org-headline-done">:one:</span></span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-todo">TODO</span></span><span class="org-level-2"> 03:30 PM ALLIGATOR                                              </span><span class="org-level-2"><span class="org-tag">:two:</span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-todo">TODO</span></span><span class="org-level-2"> 05:10 PM VIDEODROME                                          </span><span class="org-level-2"><span class="org-tag">:fletch:</span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-done">DONE</span></span><span class="org-level-2"> </span><span class="org-level-2"><span class="org-headline-done">05:20 PM A NIGHTMARE ON ELM STREET                              </span></span><span class="org-level-2"><span class="org-tag"><span class="org-headline-done">:one:</span></span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-todo">TODO</span></span><span class="org-level-2"> 05:30 PM THE DEADLY SPAWN                                       </span><span class="org-level-2"><span class="org-tag">:two:</span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-todo">TODO</span></span><span class="org-level-2"> 07:00 PM HOCUS POCUS                                         </span><span class="org-level-2"><span class="org-tag">:fletch:</span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-done">DONE</span></span><span class="org-level-2"> </span><span class="org-level-2"><span class="org-headline-done">07:10 PM FLESH FOR FRANKENSTEIN: Uncut Version                  </span></span><span class="org-level-2"><span class="org-tag"><span class="org-headline-done">:one:</span></span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-todo">TODO</span></span><span class="org-level-2"> 09:00 PM INTERVIEW WITH THE VAMPIRE                          </span><span class="org-level-2"><span class="org-tag">:fletch:</span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-done">DONE</span></span><span class="org-level-2"> </span><span class="org-level-2"><span class="org-headline-done">09:10 PM BLOOD FOR DRACULA: Uncut Version                       </span></span><span class="org-level-2"><span class="org-tag"><span class="org-headline-done">:one:</span></span></span>
            <span class="org-level-1"> * Sunday, October 9th</span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-todo">TODO</span></span><span class="org-level-2"> 02:00 PM HOCUS POCUS                                         </span><span class="org-level-2"><span class="org-tag">:fletch:</span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-todo">TODO</span></span><span class="org-level-2"> 02:10 PM DUEL                                                   </span><span class="org-level-2"><span class="org-tag">:one:</span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-done">DONE</span></span><span class="org-level-2"> </span><span class="org-level-2"><span class="org-headline-done">02:20 PM THE DEADLY SPAWN                                       </span></span><span class="org-level-2"><span class="org-tag"><span class="org-headline-done">:two:</span></span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-todo">TODO</span></span><span class="org-level-2"> 04:00 PM ALTERED STATES                                         </span><span class="org-level-2"><span class="org-tag">:one:</span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-todo">TODO</span></span><span class="org-level-2"> 04:10 PM THE &#8216;BURBS                                          </span><span class="org-level-2"><span class="org-tag">:fletch:</span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-done">DONE</span></span><span class="org-level-2"> </span><span class="org-level-2"><span class="org-headline-done">04:20 PM ALLIGATOR                                              </span></span><span class="org-level-2"><span class="org-tag"><span class="org-headline-done">:two:</span></span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-todo">TODO</span></span><span class="org-level-2"> 06:10 PM FLESH FOR FRANKENSTEIN: Uncut Version                  </span><span class="org-level-2"><span class="org-tag">:one:</span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-done">DONE</span></span><span class="org-level-2"> </span><span class="org-level-2"><span class="org-headline-done">06:20 PM THE BURNING                                         </span></span><span class="org-level-2"><span class="org-tag"><span class="org-headline-done">:fletch:</span></span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-todo">TODO</span></span><span class="org-level-2"> 08:00 PM BLOOD FOR DRACULA: Uncut Version                       </span><span class="org-level-2"><span class="org-tag">:one:</span></span>
            <span class="org-hide"> *</span><span class="org-level-2">* </span><span class="org-level-2"><span class="org-done">DONE</span></span><span class="org-level-2"> </span><span class="org-level-2"><span class="org-headline-done">08:10 PM NIGHT OF THE DEMON                                  </span></span><span class="org-level-2"><span class="org-tag"><span class="org-headline-done">:fletch:</span></span></span>
        </pre>
    </body>
</html>
